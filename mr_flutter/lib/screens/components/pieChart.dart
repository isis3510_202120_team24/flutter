import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:flutter/material.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';

class PieChartView extends StatelessWidget {
  static const Map<String, double> sampleData = {
    'romance': 0,
    'fantasy': 12,
    'horror': 1,
    'musical': 23,
    'comedy': 43,
    'war': 0,
    'musicalMystery': 12,
    'thriller': 0,
    'animation': 0,
    'adventure': 0,
    'warWestern': 13,
    'drama': 13,
    'mystery': 10,
    'children': 2,
    'filmNoir': 2,
    'action': 5,
    'imax': 0,
    'crime': 0,
    'western': 0,
    'noGenresListed': 0,
    'documentary': 0,
    'sciFi': 0
  };
  //var data = getLikes();

  PieChartView({data});

  final Map<String, double> jsonData = sampleData;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: PieChart(
      dataMap: jsonData,
      legendOptions: LegendOptions(
          showLegends: true, legendPosition: LegendPosition.bottom),
    ));
  }
}
