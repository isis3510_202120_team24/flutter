import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:mr_flutter/cubit/movie_trend_detail_cubit.dart';
import 'package:mr_flutter/screens/movie_trend_detail.dart';

import '../../../constants.dart';

class MovieCard extends StatelessWidget {
  final TrendMovie movie;
  const MovieCard({Key? key, required this.movie}) : super(key: key);
/*
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 7.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: CachedNetworkImage(
          imageUrl: movie.posterComplete,
          placeholder: (a, b) => CircularProgressIndicator(),
          fit: BoxFit.fill,
        ),
      ),
      //Center(child: Text('${movies[0].ratingScore}'))
      //GenreCard(genre: g)
    );
  }
  */
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BlocProvider<MovieTrendDetailCubit>(
                      create: (context) => MovieTrendDetailCubit(
                        context.read<FakeUserRepository>(),
                        context.read<ListRepository>(),
                      ),
                      child: MovieTrendDetail(movie: movie),
                    )));
      },
      child: Card(
        elevation: 0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    width: 150,
                    height: 200,
                    imageUrl: movie.posterComplete,
                    placeholder: (a, b) => CircularProgressIndicator(),
                    fit: BoxFit.fill,
                  )),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 7),
                    child: Text(
                      movie.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.varelaRound(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: kAccentRed),
                    ),
                  ),
                  Text(
                    movie.duration.toString() + ' min',
                    style: GoogleFonts.varelaRound(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        color: Colors.yellow,
                        size: 30,
                      ),
                      Text(
                        movie.ratingScore.toString() + '/10',
                        style: GoogleFonts.varelaRound(
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


/*
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 7.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            boxShadow: [kDefaultShadow],
            image: DecorationImage(
                fit: BoxFit.fill, image: NetworkImage(movie.posterComplete))),
      )
*/
