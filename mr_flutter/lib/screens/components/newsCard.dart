import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';

class NewsCard extends StatelessWidget {
  final MovieUpcoming news;
  const NewsCard({Key? key, required this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                //borderRadius: BorderRadius.circular(30),
                //boxShadow: [kDefaultShadow],
                image: DecorationImage(
                    //fit: BoxFit.fill,
                    image: AssetImage(news.posterComplete))),
            width: 200,
            height: 200,
          ),
          Flexible(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                news.title,
                style: GoogleFonts.ubuntuMono(fontSize: 25, color: kTextColor),
              ),
              Padding(
                padding: EdgeInsets.only(top: 11, bottom: 15),
                child: Text(news.overview,
                    style: GoogleFonts.ubuntu(fontSize: 15)),
              ),
              Text(
                news.releaseDate,
                style: GoogleFonts.ubuntu(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: kTextColor),
              )
            ],
          ))
        ],
      ),
    );
  }
}
