import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:mr_flutter/constants.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class NotificationRetention extends StatefulWidget {
  NotificationRetention(
      {Key? key, required this.movie, required this.title, required this.body})
      : super(key: key);
  TrendMovie movie;
  String body;
  String title;

  @override
  State<NotificationRetention> createState() => _NotificationRetentionState();
}

class _NotificationRetentionState extends State<NotificationRetention> {
  bool isLiked = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: SizedBox(
            width: 350.0,
            child: TextLiquidFill(
              text: 'MOVIE FOR YOU',
              waveColor: kAppRed,
              boxBackgroundColor: Colors.white,
              textStyle: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
              boxHeight: 50.0,
            ),
          )),
      backgroundColor: Colors.white,
      body: Stack(fit: StackFit.expand, children: [
        Positioned(
          child: CachedNetworkImage(
            imageUrl: widget.movie.posterComplete,
            fit: BoxFit.cover,
            placeholder: (context, url) =>
                Icon(Icons.auto_awesome_motion_rounded),
          ),
        ),
        //Image.network(
        //widget.movie.posterComplete,
        //fit: BoxFit.fill,
        //),

        /*
                Image.asset(
                  'assets/images/poster2.jpg',
                  fit: BoxFit.fill,
                ),*/

        Positioned.fill(
          top: 400,
          child: Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    primary: Color.fromRGBO(193, 0, 43, 0.5)),
                onPressed: () {},
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      widget.movie.title,
                      style: GoogleFonts.varelaRound(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      widget.movie.duration.toString() + 'min',
                      style: GoogleFonts.varelaRound(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned.fill(
          bottom: 10,
          child: Align(
            alignment: Alignment.bottomCenter,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: isLiked ? Colors.green : Color(0X47FFFFFF)),
                onPressed: () {
                  setState(() {
                    isLiked = true;
                  });
                },
                child: Text(isLiked ? 'Added' : 'Add to Favorites',
                    style: GoogleFonts.varelaRound(fontSize: 18))),
          ),
        )
      ]),
    );
  }
}
