import 'package:flutter/material.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import '../../../constants.dart';
import 'movieCard.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';

class MovieCarousel extends StatefulWidget {
  MovieCarousel({Key? key, required this.tms}) : super(key: key);
  List<TrendMovie> tms;

  @override
  _MovieCarouselState createState() => _MovieCarouselState();
}

class _MovieCarouselState extends State<MovieCarousel> {
  late PageController _pageController;
  int initialPage = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(
      viewportFraction: 0.75,
      initialPage: initialPage,
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: AspectRatio(
        aspectRatio: 1.3,
        child: PageView.builder(
            controller: _pageController,
            itemCount: widget.tms.length,
            itemBuilder: (context, index) =>
                buildMovieSlider(index, widget.tms)),
      ),
    );
  }

  Widget buildMovieSlider(int index, List<TrendMovie> tms) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (context, child) {
        return MovieCard(movie: tms[index]);
      },
    );
  }
}

/*
class MovieSwiper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: AspectRatio(
        aspectRatio: 0.85,
        child: ListView(
          children: <Widget>[]..addAll(createMovieCards(mov)),
          scrollDirection: Axis.horizontal,
        ),
      ),
    );
*/
    /*
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: CarouselSlider(
            options: CarouselOptions(
              enlargeCenterPage: true,
              enableInfiniteScroll: false,
              autoPlay: false,
            ),
            //items: []..addAll(createMovieCards(movieList)),
            items: movies.map((e) => MovieCard(movie: e)).toList(),
          ),
        ),
      ),
    );
    */
  
  /*
  List<Widget> createMovieCards(List<Movie> movies) {
    return movies.map((e) => MovieCard(movie: e)).toList();
  }
  */

