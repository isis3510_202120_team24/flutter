import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/constants.dart';

class ProfileTop extends StatefulWidget {
  @override
  _ProfileTopState createState() => _ProfileTopState();
}

class _ProfileTopState extends State<ProfileTop> {
  @override
  Widget build(BuildContext context) {
    final textScale = MediaQuery.of(context).textScaleFactor;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Expanded(
                child: Container(
              alignment: Alignment.centerRight,
              height: 50,
              width: 800,
              color: Colors.transparent,
              child: Text(
                "Test long very looooooong",
                overflow: TextOverflow.fade,
                maxLines: 1,
                style: TextStyle(
                    color: kTextColor,
                    fontSize: 30 * textScale,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.end,
                softWrap: false,
              ),
            )),

            IconButton(
                iconSize: 90,
                padding: const EdgeInsets.symmetric(horizontal: 26.0),
                onPressed: () {},
                icon: CachedNetworkImage(
                    imageUrl:
                        "https://meetanentrepreneur.lu/wp-content/uploads/2019/08/profil-linkedin.jpg",
                    placeholder: (context, url) =>
                        CircularProgressIndicator())),
            //"assets/images/profilepic.png"
          ],
        ),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              SizedBox(width: 20),
              ElevatedButton(
                  child: Text("Reset Preferences".toUpperCase(),
                      style: TextStyle(fontSize: 14)),
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kAppRed),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(3),
                              side: BorderSide(color: kAppRed)))),
                  onPressed: () => null),
              SizedBox(width: 20),
              ElevatedButton(
                  child: Text("Edit Profile".toUpperCase(),
                      style: TextStyle(fontSize: 14)),
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(kAppRed),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(3),
                              side: BorderSide(color: kAppRed)))),
                  onPressed: () => null)
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Container(
            constraints: BoxConstraints(maxWidth: 180),
            child: TextButton(
                style: TextButton.styleFrom(
                  primary: kTextColor,
                ),
                onPressed: () => null,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "My Lists",
                      //textScaleFactor: 1.7,
                      style: TextStyle(fontSize: 23 * textScale),
                    ),
                    SizedBox(width: 10),
                    Icon(
                      Icons.bookmark,
                      size: 40,
                    ),
                  ],
                )),
          ),
        )
      ],
    );
  }
}
