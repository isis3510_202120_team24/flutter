import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:mr_flutter/screens/movie_trend_detail.dart';
import 'package:mr_flutter/screens/upcoming_detail.dart';

import '../../../constants.dart';

class MovieCardU extends StatelessWidget {
  final movie;
  const MovieCardU({Key? key, required this.movie}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    print(movie);
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => UpcomingDetail(movie: movie)));
      },
      child: Card(
        elevation: 0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    width: 150,
                    height: 200,
                    imageUrl: movie['posterComplete'],
                    placeholder: (a, b) => CircularProgressIndicator(),
                    fit: BoxFit.fill,
                  )),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 7),
                    child: Text(
                      movie['title'],
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.varelaRound(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: kAccentRed),
                    ),
                  ),
                  Text(
                    movie['releaseDate'],
                    style: GoogleFonts.varelaRound(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


/*
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 7.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            boxShadow: [kDefaultShadow],
            image: DecorationImage(
                fit: BoxFit.fill, image: NetworkImage(movie.posterComplete))),
      )
*/
