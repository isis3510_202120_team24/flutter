import 'package:flutter/material.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/screens/components/newsCard.dart';

class NewsSwpier extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[]..addAll(createNews(news)),
      //physics: NeverScrollableScrollPhysics(),
      //addAutomaticKeepAlives: false,
      //scrollDirection: Axis.horizontal,
    );
  }
}

List<Widget> createNews(List<MovieUpcoming> news) {
  return news.map((e) => NewsCard(news: e)).toList();
}
