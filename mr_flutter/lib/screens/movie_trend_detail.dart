import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/cubit/movie_trend_detail_cubit.dart';

class MovieTrendDetail extends StatefulWidget {
  MovieTrendDetail({
    Key? key,
    required this.movie,
  }) : super(key: key);
  TrendMovie movie;

  @override
  State<MovieTrendDetail> createState() => _MovieTrendDetailState();
}

class _MovieTrendDetailState extends State<MovieTrendDetail> {
  bool isLiked = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return BlocConsumer<MovieTrendDetailCubit, MovieTrendDetailState>(
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        return Scaffold(
          backgroundColor: kAppRed,
          body: Padding(
            padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: size.width * 0.85,
                  height: size.height * 0.57,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: CachedNetworkImage(
                      imageUrl: widget.movie.posterComplete,
                      placeholder: (context, url) =>
                          Icon(Icons.auto_awesome_motion_rounded),
                    ),
                    //Image.network(
                    //widget.movie.posterComplete,
                    //fit: BoxFit.fill,
                    //),
                  ),
                  /*
                  Image.asset(
                    'assets/images/poster2.jpg',
                    fit: BoxFit.fill,
                  ),*/
                  decoration: BoxDecoration(
                      boxShadow: [kDefaultShadow],
                      borderRadius: BorderRadius.circular(15)),
                ),
                /*
                ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Image.asset(
                      'assets/images/poster2.jpg',
                      width: size.width * 0.8,
                      height: size.height * 0.7,
                    )),*/
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: Text(
                    widget.movie.title,
                    style: GoogleFonts.varelaRound(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 80),
                  child: Text(
                    widget.movie.duration.toString() + 'min',
                    style: GoogleFonts.varelaRound(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.normal),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 25),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(
                            widget.movie.numLikes.toString(),
                            style: GoogleFonts.varelaRound(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Total Likes',
                            style: GoogleFonts.varelaRound(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w100),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            '80%',
                            style: GoogleFonts.varelaRound(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Like Percentage',
                            style: GoogleFonts.varelaRound(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.normal),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            widget.movie.ratingScore.toString(),
                            style: GoogleFonts.varelaRound(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Rating',
                            style: GoogleFonts.varelaRound(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.normal),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: isLiked ? Colors.green : Color(0X47FFFFFF)),
                    onPressed: () async {
                      final movieTrendDetailCubit =
                          context.read<MovieTrendDetailCubit>();
                      bool res = await movieTrendDetailCubit.addMovieToList(
                          movieId: widget.movie.id);

                      setState(() {
                        isLiked = res;
                      });
                    },
                    child: Text(isLiked ? 'Added' : 'Add to Favorites',
                        style: GoogleFonts.varelaRound(fontSize: 18)))
              ],
            ),
          ),
        );
      },
    );
  }
}
