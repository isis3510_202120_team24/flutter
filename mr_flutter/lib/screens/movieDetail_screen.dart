import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/moviesAdapter.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/userAdapter.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/cubit/moviedetail_cubit.dart';
import 'dart:developer';
import 'dart:convert';
import 'package:rxdart/rxdart.dart';

class MovieDetail extends StatelessWidget {
  const MovieDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MoviedetailCubit>(
      create: (context) => MoviedetailCubit(context.read<MovieRepository>(),
          context.read<FakeUserRepository>(), context.read<ConnectionRepo>()),
      child: BigWidget(),
    );
  }
}

class BigWidget extends StatefulWidget {
  //final List<Movie> movies;
  const BigWidget({Key? key}) : super(key: key);
  //const MovieDetail({Key? key}) : super(key: key);
  @override
  //State<MovieDetail> createState() => _MovieDetailState();
  State<BigWidget> createState() {
    return _BigWidgetState();
  }
}

class _BigWidgetState extends State<BigWidget> {
  int actual = 0;
  String currentEmail = "";
  int movieId = -1;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoviedetailCubit, MovieDetailState>(
        builder: (context, state) {
      if (state is MovieDetailLoading) {
        return buildPreLoading();
      } else if (state is MovieDetailLoaded) {
        User u = state.user;
        List<RecomendedMovie> rm = state.rm;

        //log(rm.toString());

        return buildColumnInfo(u, rm);
      } else if (state is MovieDetailConected) {
        return buildLoading();
      } else if (state is MovieDetailNoConnection) {
        return noConnectionWidget();
      } else {
        return error();
      }
    });
  }

  Widget noConnectionWidget() {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey[600],
            ),
            onPressed: () {},
          ),
        ),
        backgroundColor: Colors.grey[900],
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.grey[700],
                  size: 80,
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [SizedBox(height: 10, child: Text(""))],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "You have no Connection!",
                  style: TextStyle(color: Colors.grey[600], fontSize: 23),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Come back Later",
                  style: TextStyle(color: Colors.grey[600], fontSize: 23),
                )
              ],
            )
          ],
        )));
  }

  Widget error() {
    return Center(
      child: Text('error'),
    );
  }

  Widget buildPreLoading() {
    final movieDetailCubit = context.read<MoviedetailCubit>();
    movieDetailCubit.checkConnectivity();

    //User u = movieDetailCubit.initUser();

    return Center(child: CircularProgressIndicator());
  }

  Widget buildLoading() {
    final movieDetailCubit = context.read<MoviedetailCubit>();
    movieDetailCubit.getRecomendedMovieInitial();
    return Center(child: CircularProgressIndicator());
  }

  Widget buildColumnInfo(User u, List<RecomendedMovie> pRM) {
    final movieDetailCubit = context.read<MoviedetailCubit>();
    List<RecomendedMovie> rm = pRM;
    var numLikes = 0;
    log('Inside Widget');
    log(rm.length.toString());
    List<StreamingPlataform> sps = rm[actual].streamingPlataforms;
    List<int> idLikedMovies = [];
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        color: kAppRed,
        child: Stack(
          children: <Widget>[
            Container(
              height: size.height,
              color: kAppRed,
            ),
            Container(
              height: size.height * .80,
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 4),
                      blurRadius: 10,
                      color: Colors.black26,
                    )
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40))),
            ),
            Container(
              height: size.height * .70,
              decoration: BoxDecoration(
                  color: Colors.black,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 7),
                      blurRadius: 10,
                      color: Colors.black26,
                    )
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(45),
                      bottomRight: Radius.circular(45))),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45),
                  image: DecorationImage(
                      image: NetworkImage(rm[actual].posterComplete),
                      //image: AssetImage('assets/images/poster4.jpg'),
                      fit: BoxFit.fill)),
              height: size.height * 0.70,
              width: size.width,
            ),
            Container(
              height: size.height * 0.70,
              width: size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45),
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter,
                      colors: [Colors.black.withOpacity(0.0), Colors.black])),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              width: size.width,
              height: size.height * 0.66,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  IconButton(
                      onPressed: () {
                        setState(() {
                          movieDetailCubit.likeRecomendation(
                              u.email, rm[actual].id, actual);
                          var a = movieDetailCubit.pressLike(actual);
                          actual = a;
                          numLikes++;
                          log(actual.toString());
                        });
                      },
                      icon: Icon(
                        CommunityMaterialIcons.heart,
                        color: Colors.greenAccent,
                        size: 45,
                      )),
                  IconButton(
                      onPressed: () {
                        setState(() {
                          actual++;
                        });
                      },
                      icon: Icon(
                        CommunityMaterialIcons.bookmark,
                        color: kTextLightColor,
                        size: 45,
                      )),
                  IconButton(
                      onPressed: () {
                        setState(() {
                          var a = movieDetailCubit.pressDislike(actual);
                          actual = a;
                        });
                      },
                      icon: Icon(
                        CommunityMaterialIcons.close,
                        color: kAccentRed,
                        size: 45,
                      ))
                ],
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              width: size.width,
              height: size.height * 0.78,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: _produceSPContainers(sps, context),
              ),
            ),
            Container(
                height: size.height * .93,
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    //'Schumacher es un documental dirigido por Hanns-Bruno Kammertöns, Vanessa Nöcker y Michael Wech, que aborda la vida y carrera profesional del expiloto alemán de Fórmula 1 Michael Schumacher.',
                    rm[actual].overview.split('.')[0],
                    style: GoogleFonts.ubuntu(
                        fontSize: 16, color: kTextLightColor),
                    textAlign: TextAlign.center,
                  ),
                )),
            Container(
              height: size.height * .98,
              alignment: Alignment.bottomCenter,
              child: Text(
                'Rating Score: ' + rm[actual].ratingScore.toString(),
                style: GoogleFonts.ubuntu(fontSize: 25, color: kTextLightColor),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }

  _produceSPContainers(List<StreamingPlataform> sps, BuildContext context) {
    return sps
        .map((e) => Container(
            height: 55,
            width: 55,
            child: CachedNetworkImage(
              imageUrl: e.logoPath,
              placeholder: (context, url) => Icon(Icons.live_tv),
            )))
        .toList();
  }
}
