import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mr_flutter/constants.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/cubit/stuff_cubit.dart';

class Stuff extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<StuffCubit, StuffState>(
      listener: (context, state) {
        state.maybeWhen(
          failedSignOut: (message) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content:
                  Text('Sometime is wrong with the SignOut. Try again later!'),
            ));
          },
          orElse: () {},
        );
      },
      builder: (context, state) {
        return state.when(
            initial: () => Scaffold(
                appBar: AppBar(
                  elevation: 0,
                  title: Text(
                    "Stuff",
                    style: TextStyle(color: kTextColor),
                  ),
                  leading: IconButton(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back_ios,
                          size: 40, color: Colors.black)),
                  backgroundColor: Colors.transparent,
                ),
                body: Column(
                  children: [
                    TextButton(
                      onPressed: () => signOut(context),
                      child: Text(
                        'Sign out',
                        style: GoogleFonts.ubuntuMono(
                            fontSize: 25,
                            color: kTextColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                )),
            failedSignOut: (e) => SizedBox());
      },
    );
  }
}

signOut(BuildContext context) async {
  final stuffCubit = context.read<StuffCubit>();
  await stuffCubit.signOut();
  Navigator.pop(context);
}
