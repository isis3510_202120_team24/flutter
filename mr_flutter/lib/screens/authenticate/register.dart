import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Helpers/notification.dart';
import 'package:mr_flutter/constants.dart';
import 'package:email_validator/email_validator.dart';
import 'package:mr_flutter/cubit/register_cubit.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class Register extends StatefulWidget {
  final Function? toggleView;
  Register({this.toggleView});
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();

  // Primero que se registre y luego creo usuario en firestore

  // TextField state
  String name = '';
  String email = '';
  String password = '';
  String birthday = '';
  bool messageDisconectedSend = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RegisterCubit, RegisterState>(
      listener: (context, state) {
        state.maybeWhen(
            failedRegister: (message) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                    'Sorry there was a problem trying to log in. Try again later!'),
              ));
            },
            connected: (isConnected, isLoading) {
              if (!isConnected) {
                messageDisconectedSend = true;
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                      'We detected internet problems. Please try again later!'),
                ));
              } else {
                if (messageDisconectedSend) {
                  messageDisconectedSend = false;
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text('Back online!'),
                  ));
                }
              }
            },
            notificationFirebase: (message) {
              if (message != null) {
                return NotificationHelper.NotificationFirebaseNoLogin(
                    title: message["title"]!, body: message["body"]!);
              }
            },
            orElse: () {});
      },
      builder: (context, state) {
        final registerCubit = context.read<RegisterCubit>();
        registerCubit.listenToConnectivity();
        return state.when(
          connected: (isConnected, isLoading) =>
              buildInitialInput(isConnected, isLoading),
          loadingRegister: buildLoading,
          failedRegister: (error) => buildInitialInput(true, false),
          notificationFirebase: (message) => buildInitialInput(true, false),
        );
      },
    );
  }

  Widget buildInitialInput(bool isConnected, bool isLoading) {
    if (isLoading) {
      return buildLoading();
    }
    var buttonRegister = TextButton(
      onPressed: () {
        if (birthday == '') {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('The birthday can be blank'),
          ));
        } else {
          if (_formKey.currentState!.validate()) {
            register(email, password, name, birthday.toString());
          }
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Text(
          'REGISTER',
          style: GoogleFonts.ubuntu(color: Colors.black),
        ),
      ),
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          side: BorderSide(color: kAppRed, width: 1)),
    );
    if (!isConnected) {
      buttonRegister = TextButton(
        onPressed: null,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Text(
            'REGISTER',
            style: GoogleFonts.ubuntu(color: Colors.black),
          ),
        ),
        style: TextButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            backgroundColor: Colors.grey),
      );
    }
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: IntrinsicHeight(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 50, bottom: 5),
                    child: Text(
                      'CREATE A NEW \n ACCOUNT',
                      style: GoogleFonts.ubuntuMono(
                          fontSize: 33,
                          color: kTextColor,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text(
                    'Already Registered? Log in here',
                    style: GoogleFonts.ubuntu(color: kTextColor, fontSize: 18),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 40),
                    child: TextFormField(
                      validator: (val) => val!.isEmpty ? "Enter Name" : null,
                      onChanged: (val) {
                        setState(() {
                          name = val;
                        });
                      },
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Enter your name'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 20, right: 20, top: 15, bottom: 15),
                    child: TextFormField(
                      validator: (value) => EmailValidator.validate(value ?? '')
                          ? null
                          : "Please enter a valid email",
                      onChanged: (val) {
                        setState(() {
                          email = val;
                        });
                      },
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Enter your email'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: TextFormField(
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Enter password";
                        }
                        if (val.length < 6) {
                          return "The password must be longer than 6 characters";
                        }
                        return null;
                      },
                      onChanged: (val) {
                        setState(() {
                          password = val;
                        });
                      },
                      obscureText: true,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Enter a password'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 20, right: 20, top: 15, bottom: 15),
                    child: TextFormField(
                      validator: (val) {
                        if (val!.isEmpty) {
                          return "Enter the password for validate";
                        }
                        if (val != password) {
                          return "The password don´t match";
                        }
                        return null;
                      },
                      onChanged: (val) {},
                      obscureText: true,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Repeat password'),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                        left: 20,
                        right: 20,
                        //top: 8,
                      ),
                      child: InputDatePickerFormField(
                        onDateSaved: (val) => birthday = val.toString(),
                        onDateSubmitted: (val) => birthday = val.toString(),
                        firstDate: DateTime.utc(1996, 1, 1),
                        lastDate: DateTime.now(),
                        fieldLabelText: "Birthday Date",
                      )),
                  buttonRegister,
                  TextButton(
                    onPressed: () {
                      widget.toggleView!();
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 46),
                      child: Text(
                        'SIGN IN',
                        style: GoogleFonts.ubuntu(color: Colors.black),
                      ),
                    ),
                    style: TextButton.styleFrom(
                      //side: BorderSide(color: kAppRed, width: 1),
                      primary: kAppRed,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Container(
      color: Colors.white,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void register(String email, String password, String name, String birthday) {
    final registerCubit = context.read<RegisterCubit>();
    registerCubit.register(email, password, name, birthday);
  }
}
