import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/cubit/login_cubit.dart';
import 'package:mr_flutter/Helpers/notification.dart';

class SignIn extends StatefulWidget {
  final Function? toggleView;
  SignIn({this.toggleView});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Invalid login'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text('Invalid login or password.'),
                Text('Try again.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  final _formKey = GlobalKey<FormState>();

  // TextField state
  String email = '';
  String password = '';
  bool messageDisconectedSend = false;
  @override
  void initState() {
    // TODO: implement initState
    final loginCubit = context.read<LoginCubit>();
    loginCubit.messageFirebase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginCubit, LoginState>(
      listener: (context, state) {
        state.maybeWhen(
            failedLogin: (message) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                    'Sorry there was a problem trying to log in. Try again later!'),
              ));
            },
            connected: (isConnected, isLoading) {
              if (!isConnected) {
                messageDisconectedSend = true;

                return NotificationHelper.BadNewsNotification(
                    'We detected internet problems. Please try again later!');
                // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //   content: Text(
                //       'We detected internet problems. Please try again later!'),
                // ));
              } else {
                if (messageDisconectedSend) {
                  messageDisconectedSend = false;
                  return NotificationHelper.GoodNewsNotification(
                      'Back online!');
                }
              }
            },
            notificationFirebase: (message) {
              if (message != null) {
                return NotificationHelper.NotificationFirebaseNoLogin(
                    title: message["title"]!, body: message["body"]!);
              }
            },
            orElse: () {});
      },
      builder: (context, state) {
        final loginCubit = context.read<LoginCubit>();
        loginCubit.listenToConnectivity();
        return state.when(
          connected: (isConnected, isLoading) =>
              buildInitialInput(isConnected, isLoading),
          loadingLogin: buildLoading,
          failedLogin: (error) => buildInitialInput(false, false),
          notificationFirebase: (message) => buildInitialInput(false, false),
        );
      },
    );
  }

  Widget buildInitialInput(bool isConnected, bool isLoading) {
    if (isLoading) {
      return buildLoading();
    }
    var buttonLogin = TextButton(
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          login(context, email, password);
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Text(
          'LOG IN',
          style: GoogleFonts.ubuntu(color: Colors.black),
        ),
      ),
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          side: BorderSide(color: kAppRed, width: 1)),
    );
    if (!isConnected) {
      buttonLogin = TextButton(
        onPressed: null,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Text(
            'LOG IN',
            style: GoogleFonts.ubuntu(color: Colors.black),
          ),
        ),
        style: TextButton.styleFrom(
          backgroundColor: Colors.grey,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      );
    }
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 50, bottom: 5),
                child: Text(
                  'LOGIN',
                  style: GoogleFonts.ubuntuMono(
                      fontSize: 35,
                      color: kTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                'Sign in to continue',
                style: GoogleFonts.ubuntu(color: kTextColor, fontSize: 20),
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 90),
                      child: TextFormField(
                        validator: (value) =>
                            EmailValidator.validate(value ?? '')
                                ? null
                                : "Please enter a valid email",
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Enter your email'),
                        onChanged: (val) {
                          setState(() {
                            email = val;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, top: 15, bottom: 30),
                      child: TextFormField(
                        obscuringCharacter: "*",
                        onChanged: (val) {
                          setState(() {
                            password = val;
                          });
                        },
                        obscureText: true,
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Enter your password'),
                      ),
                    ),
                    buttonLogin,
                    TextButton(
                      onPressed: () {
                        widget.toggleView!();
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 46),
                        child: Text(
                          'SIGN UP',
                          style: GoogleFonts.ubuntu(color: Colors.black),
                        ),
                      ),
                      style: TextButton.styleFrom(
                        //side: BorderSide(color: kAppRed, width: 1),
                        primary: kAppRed,
                      ),
                    ),
                    TextButton(
                      onPressed: () {}
                      /*
                            dynamic result = await _auth.signInAnon();
                            if (result == null) {
                              print("Error Sign in Anon");
                            } else {
                              print("Sign in!");
                              print(result);
                            }
                          },*/
                      ,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        child: Text(
                          'LOG IN ANONYMOUSLY',
                          style: GoogleFonts.ubuntu(color: Colors.grey),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Container(
      color: Colors.white,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  void login(BuildContext context, String email, String password) {
    final loginCubit = context.read<LoginCubit>();
    loginCubit.login(email, password);
  }

  void signOut(BuildContext context) {
    final loginCubit = context.read<LoginCubit>();
    loginCubit.signOut();
  }
}
