import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';
import 'package:mr_flutter/cubit/login_cubit.dart';
import 'package:mr_flutter/cubit/register_cubit.dart';
import 'package:mr_flutter/screens/authenticate/register.dart';
import 'package:mr_flutter/screens/authenticate/sign_in.dart';

class Authenticate extends StatefulWidget {
  Authenticate({
    Key? key,
  }) : super(key: key);
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool showSignIn = true;
  void toggleView() {
    setState(() {
      showSignIn = !showSignIn;
      print(showSignIn);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return BlocProvider<LoginCubit>(
        create: (context) => LoginCubit(
            context.read<FakeUserRepository>(),
            context.read<ConnectionRepo>(),
            null,
            context.read<MessageRepository>()),
        child: SignIn(toggleView: toggleView),
      );
    } else {
      return BlocProvider<RegisterCubit>(
        create: (context) => RegisterCubit(
            context.read<FakeUserRepository>(),
            context.read<ConnectionRepo>(),
            null,
            context.read<MessageRepository>()),
        child: Register(toggleView: toggleView),
      );
    }
  }
}
