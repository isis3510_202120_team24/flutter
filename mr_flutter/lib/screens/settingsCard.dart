import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:mr_flutter/constants.dart';

class SettingsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 50, bottom: 5),
                child: Text(
                  'Settings',
                  style: GoogleFonts.ubuntuMono(
                      fontSize: 35,
                      color: kTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 50, bottom: 5),
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    'Sign out',
                    style: GoogleFonts.ubuntuMono(
                        fontSize: 25,
                        color: kTextColor,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ]),
      ),
    );
  }
}
