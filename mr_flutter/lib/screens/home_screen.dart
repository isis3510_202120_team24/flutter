import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Helpers/notification.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';

import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:mr_flutter/cubit/home_screen_cubit.dart';
import 'package:mr_flutter/cubit/stuff_cubit.dart';
import 'package:mr_flutter/screens/components/movieCard.dart';
import 'package:mr_flutter/screens/components/notificationRetention.dart';

import 'package:mr_flutter/screens/movieDetail_screen.dart';
import 'package:mr_flutter/screens/profile_screen.dart';
import 'package:mr_flutter/screens/upcoming_screen.dart';
import 'package:mr_flutter/screens/stuff_screen.dart';
import 'package:overlay_support/overlay_support.dart';
import 'dart:math' as math;

import 'package:shake/shake.dart';

import 'components/movieSwiper.dart';
import 'components/newSwiper.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({
    Key? key,
  }) : super(key: key);
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  //final noticia = news[0];
  //final List<Movie> m = movies;
  int _actual = 0;
  late int _totalNotifications;
  PushNotification? _notificationInfo;

  //final List<Movie> moviess = getRecomendations();
  //final List<Widget> _navScreens = [Body(), Search()];
  late final FirebaseMessaging _messaging;

  @override
  Widget build(BuildContext context) {
    ShakeDetector detector = ShakeDetector.autoStart(onPhoneShake: () {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => MovieDetail()));
    });
    final List<Widget> _navScreens = [Body(), MovieDetail(), UpComingScreen()];

    return Scaffold(
      backgroundColor: Colors.white,
      body: _navScreens[_actual],
      floatingActionButton: Transform.scale(
          scale: 1.1,
          child: FloatingActionButton(
            onPressed: () {
              //onTabNavItem(1);f
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MovieDetail()));
            },
            tooltip: '',
            child: Transform.rotate(
                angle: math.pi / 2,
                child: Icon(CommunityMaterialIcons.autorenew, size: 45)),
            elevation: 2,
            backgroundColor: kAppRed,
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: kAppRed,
        onTap: onTabNavItem,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.yellow,
        selectedLabelStyle: GoogleFonts.ubuntuMono(),
        currentIndex: _actual,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              CommunityMaterialIcons.home,
              color: Colors.white,
              size: 30,
            ),
            label: '',
          ),
          BottomNavigationBarItem(
              icon: Opacity(
                  opacity: 0,
                  child: Icon(
                    CommunityMaterialIcons.location_enter,
                    color: Colors.white,
                  )),
              label: ''),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.new_releases,
                color: Colors.white,
                size: 30,
              ),
              label: '')
        ],
      ),
    );
  }

  onTabNavItem(int index) {
    setState(() {
      _actual = index;
    });
  }
}

/*
class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          MovieTrendCubit(MovieRepository(MovieServiceAdapter())),
      child: Column(),
    );
  }
}
*/
class PushNotification {
  PushNotification({
    this.title,
    this.body,
  });
  String? title;
  String? body;
}

class Body extends StatelessWidget {
  //final noticia = news[0];

  //final moviee = mov[1];
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeScreenCubit>(
      create: (context) => HomeScreenCubit(
          context.read<MovieRepository>(),
          context.read<FakeUserRepository>(),
          context.read<ConnectionRepo>(),
          context.read<MessageRepository>(),
          context.read<ListRepository>()),
      child: BigWidget(),
    );
  }
}

class BigWidget extends StatefulWidget {
  const BigWidget({
    Key? key,
  }) : super(key: key);
  @override
  State<BigWidget> createState() => _BigWidgetState();
}

class _BigWidgetState extends State<BigWidget> {
  @override
  void initState() {
    // TODO: implement initState
    final homeScreenCubit = context.read<HomeScreenCubit>();
    homeScreenCubit.messageFirebase();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeScreenCubit, HomeScreenState>(
      builder: (context, state) {
        if (state is Loading) {
          return buildPreLoading();
        } else if (state is Connected) {
          final homeCubit = context.read<HomeScreenCubit>();
          homeCubit.incrementNumber();
          return buildLoading();
        } else if (state is NoConnection) {
          User u = state.user;
          List<TrendMovie> tms = state.tms;
          return buildContentColumn(context, u, tms);
        } else if (state is Loaded) {
          User u = state.user;
          List<TrendMovie> tms = state.tms;
          return buildContentColumn(context, u, tms);
        } else if (state is NotificationFirebase) {
          User u = state.user;
          List<TrendMovie> tms = state.tms;
          Map<String, dynamic>? message = state.message;
          WidgetsBinding.instance!.addPostFrameCallback((Duration) {
            NotificationHelper.NotificationFirebaseLogin(
                title: message!["title"]!,
                body: message["body"]!,
                callBack: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NotificationRetention(
                              movie: tms[message['movie']],
                              title: message["title"]!,
                              body: message["body"]!)));
                });
          });
          return buildContentColumn(context, u, tms);
        } else {
          return error();
        }
      },
    );
  }

  Widget error() {
    return Center(
      child: Text('error'),
    );
  }

  Widget buildPreLoading() {
    final homeCubit = context.read<HomeScreenCubit>();

    homeCubit.checkConnectivity();

    //User u = movieDetailCubit.initUser();

    return Center(child: CircularProgressIndicator());
  }

  Widget buildLoading() {
    final homeCubit = context.read<HomeScreenCubit>();

    homeCubit.getTrendMovies();
    return Center(child: CircularProgressIndicator());
  }

  Widget buildContentColumn(
      BuildContext context, User u, List<TrendMovie> tms) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        buildAppBar(context, u),
        Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              'Trends',
              style: GoogleFonts.ubuntuMono(fontSize: 30, color: kTextColor),
            )),
        Expanded(
            child: RefreshIndicator(
          onRefresh: () {
            return context.read<HomeScreenCubit>().getTrendMovies();
          },
          child: ListView.builder(
            itemBuilder: (context, index) => MovieCard(
              movie: tms[index],
            ),
            itemCount: tms.length,
          ),
        ) /* <Widget>[
            MovieCard(movie: tms[0]),
            MovieCard(movie: tms[1]),
            MovieCard(movie: tms[2]),
            MovieCard(movie: tms[3]),
            MovieCard(movie: tms[4]),
            MovieCard(movie: tms[5]),
            MovieCard(movie: tms[6]),
            */
            ),

        //MovieCarousel(
        //tms: tms,
        //),
        //MovieCard(movie: moviee),
        //Expanded(
        //child: MovieCard(movie: moviee),
        //),
/*
        Padding(
          padding: EdgeInsets.only(left: 15, top: 0),
          child: Text(
            'News',
            style: GoogleFonts.ubuntuMono(fontSize: 30, color: kTextColor),
          ),
        ),
        //NewsCard(news: noticia)
        Expanded(child: NewsSwpier()),
*/
        //MovieCard(movie: moviee),
      ],
    );
  }
}

AppBar buildAppBar(BuildContext context, User u) {
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 0,
    leading: IconButton(
      padding: const EdgeInsets.only(left: 2.0),
      //icon: SvgPicture.asset("assets/icons/menu.svg"),
      icon: Icon(
        CommunityMaterialIcons.menu,
        color: Colors.black,
        size: 36,
      ),
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BlocProvider(
                      create: (_) => StuffCubit(RepositoryProvider.of(context)),
                      child: Stuff(),
                    )));
      },
    ),
    actions: <Widget>[
      TextButton(
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Profile()));
          },
          child: Text(
            u.name,
            overflow: TextOverflow.fade,
            maxLines: 1,
            style: TextStyle(color: kTextColor, fontSize: 20),
          )),
      IconButton(
          padding: const EdgeInsets.symmetric(horizontal: 2.0),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Profile()));
          },
          icon: new Image.asset("assets/images/profilepic.png"))
    ],
  );
}
