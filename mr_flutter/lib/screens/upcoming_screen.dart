import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/cubit/upcoming_cubit.dart';
import 'package:mr_flutter/screens/components/movieCardU.dart';

import 'components/movieCard.dart';

class UpComingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<UpcomingCubit>(
      create: (context) => UpcomingCubit(
        context.read<MovieRepository>(),
        context.read<ConnectionRepo>(),
      ),
      child: Center(
        child: BigWidget(),
      ),
    );
  }
}

class BigWidget extends StatefulWidget {
  const BigWidget({
    Key? key,
  }) : super(key: key);
  @override
  State<BigWidget> createState() => _BigWidgetState();
}

class _BigWidgetState extends State<BigWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpcomingCubit, UpcomingState>(
      builder: (context, state) {
        if (state is Loading) {
          return buildPreLoading();
        } else if (state is Connected) {
          return buildLoading();
        } else if (state is NoConnection) {
          return noConnectionWidget();
        } else if (state is Loaded) {
          var ums = state.upcomingMovie;
          print(ums);
          print(ums.length);
          return buildContentColumn(context, ums);
        } else {
          return error();
        }
      },
    );
  }

  Widget noConnectionWidget() {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey[600],
            ),
            onPressed: () {},
          ),
        ),
        backgroundColor: Colors.grey[900],
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.grey[700],
                  size: 80,
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [SizedBox(height: 10, child: Text(""))],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "No upcoming movies!",
                  style: TextStyle(color: Colors.grey[600], fontSize: 23),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Check your internet connection",
                  style: TextStyle(color: Colors.grey[600], fontSize: 23),
                )
              ],
            )
          ],
        )));
  }

  Widget error() {
    return Center(
      child: Text('error'),
    );
  }

  Widget buildPreLoading() {
    final upcomingCubit = context.read<UpcomingCubit>();

    upcomingCubit.checkConnectivity();

    //User u = movieDetailCubit.initUser();

    return Center(child: CircularProgressIndicator());
  }

  Widget buildLoading() {
    final upcomingCubit = context.read<UpcomingCubit>();

    upcomingCubit.getUpcoming();
    return Center(child: CircularProgressIndicator());
  }

  Widget buildContentColumn(BuildContext context, ums) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.fromLTRB(20, 35, 0, 0),
            child: Text(
              'Upcoming Movie',
              style: GoogleFonts.ubuntuMono(fontSize: 30, color: kTextColor),
            )),
        Expanded(
          child: RefreshIndicator(
            onRefresh: () {
              return context.read<UpcomingCubit>().getUpcoming();
            },
            child: ListView.builder(
              itemBuilder: (context, index) => MovieCardU(movie: ums[index]),
              itemCount: ums.length,
            ),
          ),
        ),
      ],
    );
  }
}
