import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/bloc/app_bloc.dart';
import 'package:mr_flutter/screens/authenticate/authenticate.dart';
import 'package:provider/provider.dart';

import 'home_screen.dart';
import 'splash_Screen.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //final user = Provider.of<User?>(context);
    return BlocProvider<AppBloc>(create: (_) {
      var bloc = AppBloc(context.read<FakeUserRepository>());

      return bloc;
    }, child: BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        return state.when(
            authenticated: (user) => HomeScreen(),
            unauthenticated: () => Authenticate(),
            loading: () => SplashScreen());
      },
    ));

/* 
    BlocBuilder<AppBloc, AppState>(
      bloc: AppBloc(_fakeUserRepository),
      builder: (context, state) {
        return state.when(
            authenticated: (user) => HomeScreen(),
            unauthenticated: () =>
                Authenticate(fakeUserRepository: _fakeUserRepository));
      },
    );
    final HomeScreen _home = HomeScreen();

    // Return home or authenticate widget
    return _home; */

    //return TestWidget();
    //return _home;
    //return SignIn();
    //return Register();
  }
}

/* class AppView extends StatelessWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FlowBuilder<app>(
        state: context.select((AppBloc bloc) => bloc.state),
        onGeneratePages: onGenerateAppViewPages,
      ),
    );
  }
} */
