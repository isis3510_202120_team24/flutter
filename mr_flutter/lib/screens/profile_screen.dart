import 'package:cached_network_image/cached_network_image.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';
import 'package:mr_flutter/cubit/edit_cubit.dart';
import 'package:mr_flutter/cubit/liked_movies_screen_cubit_cubit.dart';
import 'package:mr_flutter/cubit/profile_cubit.dart';
import 'package:mr_flutter/screens/liked_movies_screen.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/screens/edit_profile.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileCubit>(
      create: (context) => ProfileCubit(
        context.read<FakeUserRepository>(),
        context.read<ConnectionRepo>(),
        context.read,
      ),
      child: ProfileView(),
    );
  }
}

class ProfileView extends StatefulWidget {
  final name = "test";
  @override
  State<ProfileView> createState() {
    return _ProfileState();
  }
}

class _ProfileState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileCubit, ProfileState>(builder: (context, state) {
      if (state is ProfileLoading) {
        return buildLoading();
      } else if (state is ProfileLoaded) {
        bool c = state.connected;
        User u = state.user;
        Map<String, double> p = state.pie;
        return allProfile(context, u, p, c);
      } else {
        return Center(
          child: Text("oopsies"),
        );
      }
    });
  }

  Widget buildLoading() {
    final profileCubit = context.read<ProfileCubit>();
    profileCubit.getUser();
    //User u = movieDetailCubit.initUser();

    return Center(child: CircularProgressIndicator());
  }

  Widget allProfile(
      BuildContext context, User u, Map<String, double> p, bool c) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        //bottomNavigationBar: Navbar(),
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios, size: 40, color: Colors.black)),
          backgroundColor: Colors.transparent,
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            Column(mainAxisSize: MainAxisSize.min, children: [
              Wrap(
                children: [
                  profileTop(context, u, c),
                  Container(
                    child: TextButton(
                      child: BlocBuilder<ProfileCubit, ProfileState>(
                          builder: (context, state) {
                            return getPie(context, p);
                          },
                          key: UniqueKey()),
                      onPressed: () {
                        setState(() {
                          final profileCubit = context.read<ProfileCubit>();
                          profileCubit.getPie();
                          profileCubit.updateConection();
                        });

                        final snackBar = SnackBar(
                          content: Text(
                              'There is no connectivity. Showing saved stats'),
                          action: SnackBarAction(
                            label: 'Close',
                            onPressed: () {},
                          ),
                        );
                        if (!c) {
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      },
                    ),
                    alignment: Alignment.topCenter,
                  ),
                ],
              ),
            ]),
          ],
        ));
  }
}

class Snackbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ElevatedButton(
        onPressed: () {
          final snackBar = SnackBar(
            content: Text('Have a nice weekend!'),
            action: SnackBarAction(
              label: 'Close',
              onPressed: () {},
            ),
          );

          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        },
        child: Text('Open Snackbar'),
      ),
    );
  }
}

Map<String, double> jsonData = {
  "wows": 7.0,
  "wows2": 19.0,
  "wows3": 10.0,
  "wows4": 8.0
};
Widget getPie(BuildContext context, p) {
  return Center(
      child: PieChart(
    dataMap: p,
    legendOptions: LegendOptions(
      showLegends: true,
      legendPosition: LegendPosition.bottom,
    ),
    chartValuesOptions: ChartValuesOptions(showChartValuesInPercentage: true),
  ));
}

Widget profileTop(BuildContext context, User u, bool c) {
  final textScale = MediaQuery.of(context).textScaleFactor;
  return Column(
    key: UniqueKey(),
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
              child: Container(
            alignment: Alignment.centerRight,
            height: 50,
            width: 800,
            color: Colors.transparent,
            child: Text(
              u.name,
              overflow: TextOverflow.fade,
              maxLines: 1,
              style: TextStyle(
                  color: kTextColor,
                  fontSize: 30 * textScale,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.end,
              softWrap: false,
            ),
          )),

          IconButton(
              iconSize: 90,
              padding: const EdgeInsets.symmetric(horizontal: 26.0),
              onPressed: () {},
              icon: CachedNetworkImage(
                  imageUrl: u.image,
                  placeholder: (context, url) =>
                      Icon(CommunityMaterialIcons.emoticon_happy_outline))),
          //"assets/images/profilepic.png"
        ],
      ),
      Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(width: 20),
            ElevatedButton(
              child: Text("Reset Preferences".toUpperCase(),
                  style: TextStyle(fontSize: 14)),
              style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.white),
                  backgroundColor: MaterialStateProperty.all<Color>(kAppRed),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(3),
                          side: BorderSide(color: kAppRed)))),
              onPressed: () {
                final profileCubit = context.read<ProfileCubit>();
                profileCubit.deleteUser();
                profileCubit.updateConection();

                final snackBar = SnackBar(
                  content:
                      Text('There is no connectivity. Showing saved stats'),
                  action: SnackBarAction(
                    label: 'Close',
                    onPressed: () {},
                  ),
                );
                if (!c) {
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              },
            ),
            SizedBox(width: 20),
            ElevatedButton(
                child: Text("Edit Profile".toUpperCase(),
                    style: TextStyle(fontSize: 14)),
                style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                    backgroundColor: MaterialStateProperty.all<Color>(kAppRed),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(3),
                            side: BorderSide(color: kAppRed)))),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BlocProvider<EditCubit>(
                              create: (context) => EditCubit(
                                    context.read<FakeUserRepository>(),
                                  ),
                              child: Edit())));
                })
          ],
        ),
      ),
      Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        TextButton(
            style: TextButton.styleFrom(
              primary: kTextColor,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          BlocProvider<LikedMoviesScreenCubit>(
                              create: (context) => LikedMoviesScreenCubit(
                                    context.read<FakeUserRepository>(),
                                    context.read<ConnectionRepo>(),
                                    context.read<ListRepository>(),
                                  ),
                              child: LikedMoviesScreen())));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Liked Movies",
                  //textScaleFactor: 1.7,
                  style: TextStyle(fontSize: 23 * textScale),
                ),
                SizedBox(width: 10),
                Icon(
                  Icons.bookmark,
                  size: 40,
                ),
              ],
            )),
      ]),
    ],
  );
}
