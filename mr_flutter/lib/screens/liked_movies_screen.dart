import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/Data_Layer/models/listU.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/cubit/liked_movies_screen_cubit_cubit.dart';

class LikedMoviesScreen extends StatefulWidget {
  @override
  _LikedMoviesScreenState createState() => _LikedMoviesScreenState();
}

class _LikedMoviesScreenState extends State<LikedMoviesScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LikedMoviesScreenCubit, LikedMoviesScreenCubitState>(
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        final likedMoviesCubit = context.read<LikedMoviesScreenCubit>();
        likedMoviesCubit.loadList();
        return state.when(
          loading: () => Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
          loaded: (list) => loadedBuild(context, list),
          deleted: () => Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      },
    );
  }
}

Widget loadedBuild(BuildContext context, ListU list) {
  return Scaffold(
      body: Container(
          child: ListView.builder(
    itemCount: list.movies.length,
    itemBuilder: (context, index) {
      Movie movie = list.movies[index];
      return Card(
        elevation: 0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 10, 0),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    width: 150,
                    height: 200,
                    imageUrl: movie.posterComplete,
                    placeholder: (a, b) => CircularProgressIndicator(),
                    fit: BoxFit.fill,
                  )),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 7),
                    child: Text(
                      movie.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: GoogleFonts.varelaRound(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: kAccentRed),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        color: Colors.yellow,
                        size: 30,
                      ),
                      Text(
                        movie.ratingScore.toString() + '/10',
                        style: GoogleFonts.varelaRound(
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Colors.red),
                onPressed: () {
                  final likedMoviesCubit =
                      context.read<LikedMoviesScreenCubit>();
                  likedMoviesCubit.deleteMovieFromList(index);
                },
                child: Icon(Icons.delete))
          ],
        ),
      );
    },
  )));
}
