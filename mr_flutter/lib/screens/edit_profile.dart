import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mr_flutter/constants.dart';
import 'package:mr_flutter/cubit/edit_cubit.dart';

class Edit extends StatefulWidget {
  final Function? toggleView;
  Edit({this.toggleView});
  @override
  _EditInState createState() => _EditInState();
}

class _EditInState extends State<Edit> {
  String name = '';
  String birthday = '';
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Invalid login'),
          content: SingleChildScrollView(
            child: ListBody(
              children: const <Widget>[
                Text(''),
                Text('Try again.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  final _formKey = GlobalKey<FormState>();

  // TextField state
  String email = '';
  String password = '';
  bool messageDisconectedSend = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<EditCubit, EditState>(
      listener: (context, state) {},
      builder: (context, state) {
        return state.when(
            initial: () => buildInitialInput(true, false),
            sendEdit: buildLoading);
      },
    );
  }

  Widget buildInitialInput(bool isConnected, bool isLoading) {
    if (isLoading) {
      return buildLoading();
    }
    var buttonLogin = TextButton(
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          //login(context, email, password);
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 50),
        child: Text(
          'Submit',
          style: GoogleFonts.ubuntu(color: Colors.black),
        ),
      ),
      style: TextButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          side: BorderSide(color: kAppRed, width: 1)),
    );
    if (!isConnected) {
      buttonLogin = TextButton(
        onPressed: null,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50),
          child: Text(
            'Edit',
            style: GoogleFonts.ubuntu(color: Colors.black),
          ),
        ),
        style: TextButton.styleFrom(
          backgroundColor: Colors.grey,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
      );
    }
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 50, bottom: 5),
                child: Text(
                  'Edit Profile',
                  style: GoogleFonts.ubuntuMono(
                      fontSize: 35,
                      color: kTextColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                '',
                style: GoogleFonts.ubuntu(color: kTextColor, fontSize: 20),
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 90),
                      child: TextFormField(
                        decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Enter your name'),
                        onChanged: (val) {
                          setState(() {
                            email = val;
                          });
                        },
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            left: 20, right: 20, top: 15, bottom: 30),
                        child: InputDatePickerFormField(
                          onDateSaved: (val) => birthday = val.toString(),
                          onDateSubmitted: (val) => birthday = val.toString(),
                          firstDate: DateTime.utc(1996, 1, 1),
                          lastDate: DateTime.now(),
                          fieldLabelText: "Birthday Date",
                        )),
                    buttonLogin,
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Container(
      color: Colors.white,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
