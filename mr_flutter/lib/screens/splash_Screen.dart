import 'package:flutter/material.dart';
import 'package:mr_flutter/constants.dart';

class SplashScreen extends StatelessWidget {
  final ImageProvider logo = AssetImage("images/logo_rienpa.png");

  @override
  Widget build(BuildContext context) {
    return Container(
        color: kAppRed,
        child: Image.asset(
          'assets/images/logo.png',
          width: 100,
          height: 100,
        )

        /*
           Icon(
            Icons.change_circle,
            size: 150,
            color: Colors.white,
          ),*/
        );
  }
}
