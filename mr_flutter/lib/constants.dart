import 'package:flutter/material.dart';

// Color Scheme
const kTextLightColor = Color(0XFFFDF0D5);
const kTextColor = Color(0XFF003049);
const kAppRed = Color(0XFFC1121F);
const kAccentRed = Color(0XFF780000);

const kDefaultShadow = BoxShadow(
  offset: Offset(0, 7),
  blurRadius: 4,
  spreadRadius: 4,
  color: Colors.black26,
);
