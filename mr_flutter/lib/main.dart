import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/listAdapter.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/moviesAdapter.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/userAdapter.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/screens/wrapper.dart';
import 'package:overlay_support/overlay_support.dart';
import 'Data_Layer/Service_Adapters/moviesAdapter.dart';

Future<Map<String, String?>?> _checkForInitialMessage(message) async {
  FirebaseMessaging _messaging = FirebaseMessaging.instance;
  RemoteMessage? initialMessage = await _messaging.getInitialMessage();

  if (initialMessage != null) {
    // Parse the message received
    Map<String, dynamic>? notification =
        MessageRepository.messageInterpreter(initialMessage);
    print(notification);
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Initialize FlutterFire:
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_checkForInitialMessage);
  final authenticationRepository =
      FakeUserRepository(userAdapter: UserAdapter());

  final movieRepo = MovieRepository(MovieServiceAdapter());
  final listRepo = ListRepository(ListAdapter());

  final connectionRepository = ConnectionRepo();
  final messageRepository =
      MessageRepository(messaging: FirebaseMessaging.instance);
  messageRepository.registerNotification();
  InternetConnectionChecker().checkInterval = Duration(seconds: 2);
  messageRepository.checkForInitialMessage('asdasd');

  runApp(MyApp(
      authenticationRepository: authenticationRepository,
      connectionRepository: connectionRepository,
      movieRepo: movieRepo,
      messageRepository: messageRepository,
      listRepo: listRepo));
}

class MyApp extends StatelessWidget {
  const MyApp(
      {Key? key,
      required MovieRepository movieRepo,
      required FakeUserRepository authenticationRepository,
      required ConnectionRepo connectionRepository,
      required MessageRepository messageRepository,
      required ListRepository listRepo})
      : _authenticationRepository = authenticationRepository,
        _connectionRepository = connectionRepository,
        _movieRepository = movieRepo,
        _messageRepository = messageRepository,
        _listRepository = listRepo,
        super(key: key);

  final FakeUserRepository _authenticationRepository;
  final ConnectionRepo _connectionRepository;
  final MovieRepository _movieRepository;
  final MessageRepository _messageRepository;
  final ListRepository _listRepository;

  Widget build(BuildContext context) {
    return OverlaySupport(
      child: MultiRepositoryProvider(

          // Initialize FlutterFire:
          providers: [
            RepositoryProvider.value(value: _authenticationRepository),
            RepositoryProvider.value(value: _connectionRepository),
            RepositoryProvider.value(value: _movieRepository),
            RepositoryProvider.value(value: _messageRepository),
            RepositoryProvider.value(value: _listRepository),
          ],
          child: MaterialApp(
            title: 'Movie Roolette',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: Wrapper(),
          )

          // Otherwise, show something whilst waiting for initialization to complete

          ),
    );
  }
}
