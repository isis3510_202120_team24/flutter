import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

class NotificationHelper {
  static OverlaySupportEntry GoodNewsNotification(String message) {
    return showSimpleNotification(Text(message),
        background: Colors.green,
        position: NotificationPosition.bottom,
        autoDismiss: true, trailing: Builder(builder: (context) {
      return TextButton(
          onPressed: () {
            OverlaySupportEntry.of(context)!.dismiss();
          },
          child: Text(
            'Dismiss',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ));
    }), duration: Duration(seconds: 3));
  }

  static OverlaySupportEntry BadNewsNotification(String message) {
    return showSimpleNotification(Text(message),
        background: Colors.deepOrangeAccent,
        position: NotificationPosition.bottom,
        autoDismiss: true, trailing: Builder(builder: (context) {
      return TextButton(
          onPressed: () {
            OverlaySupportEntry.of(context)!.dismiss();
          },
          child: Text(
            'Dismiss',
            style: TextStyle(color: Colors.amber),
          ));
    }), duration: Duration(seconds: 3));
  }

  static OverlaySupportEntry NotificationFirebaseNoLogin(
      {required String title, required String body}) {
    return showSimpleNotification(
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          child: Column(
            children: [
              Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
              Text(body),
            ],
          ),
        ),
        background: Colors.red[700],
        position: NotificationPosition.top,
        autoDismiss: true, trailing: Builder(builder: (context) {
      return TextButton(
          onPressed: () {
            OverlaySupportEntry.of(context)!.dismiss();
          },
          child: Text(
            'Dismiss',
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ));
    }), duration: Duration(seconds: 3));
  }

  static OverlaySupportEntry NotificationFirebaseLogin(
      {required String title,
      required String body,
      void Function()? callBack}) {
    return showSimpleNotification(
        Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          child: Column(
            children: [
              Text(title, style: TextStyle(fontWeight: FontWeight.bold)),
              Text(body),
              TextButton(
                  onPressed: callBack,
                  child: Text(
                    'Yes Please!',
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ))
            ],
          ),
        ),
        background: Colors.red[700],
        position: NotificationPosition.top,
        autoDismiss: true,
        duration: Duration(seconds: 4));
  }
}
