class MovieUpcoming {
  String title;
  bool adult;
  double voteAverage;
  String posterComplete;
  String releaseDate;
  String overview;

  MovieUpcoming(
    this.title,
    this.adult,
    this.voteAverage,
    this.posterComplete,
    this.releaseDate,
    this.overview,
  );
}

List<MovieUpcoming> news = [
  MovieUpcoming(
      'Hotel Transylvania 3',
      true,
      20,
      'assets/images/poster3.jpg',
      'releaseDate1',
      'El nuevo y misterioso invento de Van Helsing transforma a Drac y sus amigos en humanos, y a Johnny en un monstruo.'),
  MovieUpcoming(
      'Squid Games',
      true,
      20,
      'assets/images/poster1.jpeg',
      'releaseDate2',
      'Cientos de personas con dificultades económicas aceptan una extraña invitación a un juego de supervivencia. Les espera un premio millonario, pero hay mucho en juego.'),
  MovieUpcoming(
      'Jumangi',
      true,
      20,
      'assets/images/poster2.jpg',
      'releaseDate3',
      'Es una película estadounidense de acción, aventuras y comedia de 2017, dirigida por Jake Kasdan y escrita por Chris McKenna, Erik Sommers, Scott Rosenberg y Jeff Pinkner.'),
  MovieUpcoming(
      'Schumacher',
      true,
      20,
      'assets/images/poster4.jpg',
      'releaseDate3',
      'Schumacher es un documental dirigido por Hanns-Bruno Kammertöns, Vanessa Nöcker y Michael Wech, que aborda la vida y carrera profesional del expiloto alemán de Fórmula 1 Michael Schumacher.'),
];
