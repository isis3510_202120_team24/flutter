// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'listU.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ListU _$ListUFromJson(Map<String, dynamic> json) {
  return _ListU.fromJson(json);
}

/// @nodoc
class _$ListUTearOff {
  const _$ListUTearOff();

  _ListU call({String name = '', List<Movie> movies = const []}) {
    return _ListU(
      name: name,
      movies: movies,
    );
  }

  ListU fromJson(Map<String, Object> json) {
    return ListU.fromJson(json);
  }
}

/// @nodoc
const $ListU = _$ListUTearOff();

/// @nodoc
mixin _$ListU {
  String get name => throw _privateConstructorUsedError;
  List<Movie> get movies => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListUCopyWith<ListU> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListUCopyWith<$Res> {
  factory $ListUCopyWith(ListU value, $Res Function(ListU) then) =
      _$ListUCopyWithImpl<$Res>;
  $Res call({String name, List<Movie> movies});
}

/// @nodoc
class _$ListUCopyWithImpl<$Res> implements $ListUCopyWith<$Res> {
  _$ListUCopyWithImpl(this._value, this._then);

  final ListU _value;
  // ignore: unused_field
  final $Res Function(ListU) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? movies = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      movies: movies == freezed
          ? _value.movies
          : movies // ignore: cast_nullable_to_non_nullable
              as List<Movie>,
    ));
  }
}

/// @nodoc
abstract class _$ListUCopyWith<$Res> implements $ListUCopyWith<$Res> {
  factory _$ListUCopyWith(_ListU value, $Res Function(_ListU) then) =
      __$ListUCopyWithImpl<$Res>;
  @override
  $Res call({String name, List<Movie> movies});
}

/// @nodoc
class __$ListUCopyWithImpl<$Res> extends _$ListUCopyWithImpl<$Res>
    implements _$ListUCopyWith<$Res> {
  __$ListUCopyWithImpl(_ListU _value, $Res Function(_ListU) _then)
      : super(_value, (v) => _then(v as _ListU));

  @override
  _ListU get _value => super._value as _ListU;

  @override
  $Res call({
    Object? name = freezed,
    Object? movies = freezed,
  }) {
    return _then(_ListU(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      movies: movies == freezed
          ? _value.movies
          : movies // ignore: cast_nullable_to_non_nullable
              as List<Movie>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListU implements _ListU {
  const _$_ListU({this.name = '', this.movies = const []});

  factory _$_ListU.fromJson(Map<String, dynamic> json) =>
      _$$_ListUFromJson(json);

  @JsonKey(defaultValue: '')
  @override
  final String name;
  @JsonKey(defaultValue: const [])
  @override
  final List<Movie> movies;

  @override
  String toString() {
    return 'ListU(name: $name, movies: $movies)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ListU &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.movies, movies) ||
                const DeepCollectionEquality().equals(other.movies, movies)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(movies);

  @JsonKey(ignore: true)
  @override
  _$ListUCopyWith<_ListU> get copyWith =>
      __$ListUCopyWithImpl<_ListU>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListUToJson(this);
  }
}

abstract class _ListU implements ListU {
  const factory _ListU({String name, List<Movie> movies}) = _$_ListU;

  factory _ListU.fromJson(Map<String, dynamic> json) = _$_ListU.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  List<Movie> get movies => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ListUCopyWith<_ListU> get copyWith => throw _privateConstructorUsedError;
}
