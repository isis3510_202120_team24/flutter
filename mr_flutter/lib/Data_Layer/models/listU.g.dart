// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'listU.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ListU _$$_ListUFromJson(Map<String, dynamic> json) => _$_ListU(
      name: json['name'] as String? ?? '',
      movies: (json['movies'] as List<dynamic>?)
              ?.map((e) => Movie.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
    );

Map<String, dynamic> _$$_ListUToJson(_$_ListU instance) => <String, dynamic>{
      'name': instance.name,
      'movies': instance.movies,
    };
