import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
part 'genre.freezed.dart';
part 'genre.g.dart';

Genre genreFromJson(String str) => Genre.fromJson(json.decode(str));

String genreToJson(Genre data) => json.encode(data.toJson());

@freezed
class Genre with _$Genre {
  const factory Genre({required String name}) = _Genre;

  factory Genre.fromJson(Map<String, dynamic> json) => _$GenreFromJson(json);
}
