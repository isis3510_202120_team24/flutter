// To parse this JSON data, do
//
//     final movie = movieFromJson(jsonString);

import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
import 'cast.dart';
import 'director.dart';
import 'genre.dart';
import 'sp_country.dart';
import 'streamingplatform.dart';
part 'movie.freezed.dart';
part 'movie.g.dart';

Movie movieFromJson(String str) => Movie.fromJson(json.decode(str));

String movieToJson(Movie data) => json.encode(data.toJson());

@freezed
class Movie with _$Movie {
  const factory Movie({
    @Default('')
        String overview,
    @Default('')
        String releaseDate,
    @JsonKey()
    @Default([Director(name: '', id: 0, profilePath: '')])
        List<Director> directors,
    @Default('')
        String title,
    @Default(-1)
        int numLikes,
    @Default(-1.0)
        double ratingScore,
    @JsonKey()
    @Default([
      Cast(popularity: 0.0, name: '', characterName: '', id: 0, profilePath: '')
    ])
        List<Cast> cast,
    @Default(-1)
        int tmdbId,
    @JsonKey()
    @Default([Genre(name: 'no_category')])
        List<Genre> genres,
    @Default(-1)
        int id,
    @Default(-1)
        int runTime,
    @Default(false)
        bool adult,
    @Default('')
        String posterComplete,
    @Default(-1)
        int budget,
    @JsonKey()
    @Default([
      SPCountry(
          streamingPlataform:
              StreamingPlataform(logoPath: '', id: -1, providerName: ''),
          countryId: '00')
    ])
        List<SPCountry> plataformsByCountry,
  }) = _Movie;

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);
}

List<Movie> mov = [
  Movie(
      tmdbId: 238,
      id: 1,
      title: 'Test',
      adult: true,
      ratingScore: 5.0,
      posterComplete:
          'https://pbs.twimg.com/media/E8rinOWXoAYJ8w8?format=jpg&name=large',
      runTime: 100,
      releaseDate: '23/1/19',
      overview:
          'Schumacher is a revealing, yet incomplete documentary of a Formula 1 legend. The documentary film about his rise through the ranks is unparalleled to almost anyone in the racing world. At the time of his retirement in 2012.',
      budget: 22222222,
      numLikes: 20,
      directors: [
        Director(name: 'marko Antonio', profilePath: 'xyz', id: 0)
      ],
      genres: [
        Genre(name: "Comedy")
      ],
      plataformsByCountry: [
        SPCountry(
            streamingPlataform: StreamingPlataform(
                providerName: 'netflix',
                logoPath:
                    'https://1000marcas.net/wp-content/uploads/2020/01/Logo-Netflix.png',
                id: 0),
            countryId: 'CO')
      ],
      cast: [
        Cast(
            name: 'El viejo Leo',
            characterName: 'Jake',
            profilePath: 'profilePath',
            popularity: 20.0,
            id: 0),
      ]),
  Movie(
      tmdbId: 240,
      id: 3,
      title: 'Test3',
      adult: true,
      ratingScore: 5.0,
      posterComplete:
          'https://pbs.twimg.com/media/E8rinOWXoAYJ8w8?format=jpg&name=large',
      runTime: 100,
      releaseDate: '23/1/19',
      overview:
          'Schumacher is a revealing, yet incomplete documentary of a Formula 1 legend. The documentary film about his rise through the ranks is unparalleled to almost anyone in the racing world. At the time of his retirement in 2012.',
      budget: 22222222,
      numLikes: 20,
      directors: [
        Director(name: 'marko Antonio', profilePath: 'xyz', id: 0)
      ],
      genres: [
        Genre(name: "Comedy")
      ],
      plataformsByCountry: [
        SPCountry(
            streamingPlataform: StreamingPlataform(
                providerName: 'netflix',
                logoPath:
                    'https://1000marcas.net/wp-content/uploads/2020/01/Logo-Netflix.png',
                id: 0),
            countryId: 'CO')
      ],
      cast: [
        Cast(
            name: 'El viejo Leo',
            characterName: 'Jake',
            profilePath: 'profilePath',
            popularity: 20.0,
            id: 0),
      ]),
  Movie(
      tmdbId: 239,
      id: 2,
      title: 'Test2',
      adult: true,
      ratingScore: 5.0,
      posterComplete:
          'https://pbs.twimg.com/media/E8rinOWXoAYJ8w8?format=jpg&name=large',
      runTime: 100,
      releaseDate: '23/1/19',
      overview:
          'Schumacher is a revealing, yet incomplete documentary of a Formula 1 legend. The documentary film about his rise through the ranks is unparalleled to almost anyone in the racing world. At the time of his retirement in 2012.',
      budget: 22222222,
      numLikes: 20,
      directors: [
        Director(name: 'marko Antonio', profilePath: 'xyz', id: 0)
      ],
      genres: [
        Genre(name: "Comedy")
      ],
      plataformsByCountry: [
        SPCountry(
            streamingPlataform: StreamingPlataform(
                providerName: 'netflix',
                logoPath:
                    'https://1000marcas.net/wp-content/uploads/2020/01/Logo-Netflix.png',
                id: 0),
            countryId: 'CO')
      ],
      cast: [
        Cast(
            name: 'El viejo Leo',
            characterName: 'Jake',
            profilePath: 'profilePath',
            popularity: 20.0,
            id: 0),
      ]),
];
