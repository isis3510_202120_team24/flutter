// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sp_country.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SPCountry _$SPCountryFromJson(Map<String, dynamic> json) {
  return _SPCountry.fromJson(json);
}

/// @nodoc
class _$SPCountryTearOff {
  const _$SPCountryTearOff();

  _SPCountry call(
      {required StreamingPlataform streamingPlataform,
      required String countryId}) {
    return _SPCountry(
      streamingPlataform: streamingPlataform,
      countryId: countryId,
    );
  }

  SPCountry fromJson(Map<String, Object> json) {
    return SPCountry.fromJson(json);
  }
}

/// @nodoc
const $SPCountry = _$SPCountryTearOff();

/// @nodoc
mixin _$SPCountry {
  StreamingPlataform get streamingPlataform =>
      throw _privateConstructorUsedError;
  String get countryId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SPCountryCopyWith<SPCountry> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SPCountryCopyWith<$Res> {
  factory $SPCountryCopyWith(SPCountry value, $Res Function(SPCountry) then) =
      _$SPCountryCopyWithImpl<$Res>;
  $Res call({StreamingPlataform streamingPlataform, String countryId});

  $StreamingPlataformCopyWith<$Res> get streamingPlataform;
}

/// @nodoc
class _$SPCountryCopyWithImpl<$Res> implements $SPCountryCopyWith<$Res> {
  _$SPCountryCopyWithImpl(this._value, this._then);

  final SPCountry _value;
  // ignore: unused_field
  final $Res Function(SPCountry) _then;

  @override
  $Res call({
    Object? streamingPlataform = freezed,
    Object? countryId = freezed,
  }) {
    return _then(_value.copyWith(
      streamingPlataform: streamingPlataform == freezed
          ? _value.streamingPlataform
          : streamingPlataform // ignore: cast_nullable_to_non_nullable
              as StreamingPlataform,
      countryId: countryId == freezed
          ? _value.countryId
          : countryId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }

  @override
  $StreamingPlataformCopyWith<$Res> get streamingPlataform {
    return $StreamingPlataformCopyWith<$Res>(_value.streamingPlataform,
        (value) {
      return _then(_value.copyWith(streamingPlataform: value));
    });
  }
}

/// @nodoc
abstract class _$SPCountryCopyWith<$Res> implements $SPCountryCopyWith<$Res> {
  factory _$SPCountryCopyWith(
          _SPCountry value, $Res Function(_SPCountry) then) =
      __$SPCountryCopyWithImpl<$Res>;
  @override
  $Res call({StreamingPlataform streamingPlataform, String countryId});

  @override
  $StreamingPlataformCopyWith<$Res> get streamingPlataform;
}

/// @nodoc
class __$SPCountryCopyWithImpl<$Res> extends _$SPCountryCopyWithImpl<$Res>
    implements _$SPCountryCopyWith<$Res> {
  __$SPCountryCopyWithImpl(_SPCountry _value, $Res Function(_SPCountry) _then)
      : super(_value, (v) => _then(v as _SPCountry));

  @override
  _SPCountry get _value => super._value as _SPCountry;

  @override
  $Res call({
    Object? streamingPlataform = freezed,
    Object? countryId = freezed,
  }) {
    return _then(_SPCountry(
      streamingPlataform: streamingPlataform == freezed
          ? _value.streamingPlataform
          : streamingPlataform // ignore: cast_nullable_to_non_nullable
              as StreamingPlataform,
      countryId: countryId == freezed
          ? _value.countryId
          : countryId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SPCountry implements _SPCountry {
  const _$_SPCountry(
      {required this.streamingPlataform, required this.countryId});

  factory _$_SPCountry.fromJson(Map<String, dynamic> json) =>
      _$$_SPCountryFromJson(json);

  @override
  final StreamingPlataform streamingPlataform;
  @override
  final String countryId;

  @override
  String toString() {
    return 'SPCountry(streamingPlataform: $streamingPlataform, countryId: $countryId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SPCountry &&
            (identical(other.streamingPlataform, streamingPlataform) ||
                const DeepCollectionEquality()
                    .equals(other.streamingPlataform, streamingPlataform)) &&
            (identical(other.countryId, countryId) ||
                const DeepCollectionEquality()
                    .equals(other.countryId, countryId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(streamingPlataform) ^
      const DeepCollectionEquality().hash(countryId);

  @JsonKey(ignore: true)
  @override
  _$SPCountryCopyWith<_SPCountry> get copyWith =>
      __$SPCountryCopyWithImpl<_SPCountry>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SPCountryToJson(this);
  }
}

abstract class _SPCountry implements SPCountry {
  const factory _SPCountry(
      {required StreamingPlataform streamingPlataform,
      required String countryId}) = _$_SPCountry;

  factory _SPCountry.fromJson(Map<String, dynamic> json) =
      _$_SPCountry.fromJson;

  @override
  StreamingPlataform get streamingPlataform =>
      throw _privateConstructorUsedError;
  @override
  String get countryId => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$SPCountryCopyWith<_SPCountry> get copyWith =>
      throw _privateConstructorUsedError;
}
