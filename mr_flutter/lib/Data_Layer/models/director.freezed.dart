// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'director.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Director _$DirectorFromJson(Map<String, dynamic> json) {
  return _Director.fromJson(json);
}

/// @nodoc
class _$DirectorTearOff {
  const _$DirectorTearOff();

  _Director call(
      {required String name, required int id, required String profilePath}) {
    return _Director(
      name: name,
      id: id,
      profilePath: profilePath,
    );
  }

  Director fromJson(Map<String, Object> json) {
    return Director.fromJson(json);
  }
}

/// @nodoc
const $Director = _$DirectorTearOff();

/// @nodoc
mixin _$Director {
  String get name => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  String get profilePath => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DirectorCopyWith<Director> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DirectorCopyWith<$Res> {
  factory $DirectorCopyWith(Director value, $Res Function(Director) then) =
      _$DirectorCopyWithImpl<$Res>;
  $Res call({String name, int id, String profilePath});
}

/// @nodoc
class _$DirectorCopyWithImpl<$Res> implements $DirectorCopyWith<$Res> {
  _$DirectorCopyWithImpl(this._value, this._then);

  final Director _value;
  // ignore: unused_field
  final $Res Function(Director) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? id = freezed,
    Object? profilePath = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      profilePath: profilePath == freezed
          ? _value.profilePath
          : profilePath // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$DirectorCopyWith<$Res> implements $DirectorCopyWith<$Res> {
  factory _$DirectorCopyWith(_Director value, $Res Function(_Director) then) =
      __$DirectorCopyWithImpl<$Res>;
  @override
  $Res call({String name, int id, String profilePath});
}

/// @nodoc
class __$DirectorCopyWithImpl<$Res> extends _$DirectorCopyWithImpl<$Res>
    implements _$DirectorCopyWith<$Res> {
  __$DirectorCopyWithImpl(_Director _value, $Res Function(_Director) _then)
      : super(_value, (v) => _then(v as _Director));

  @override
  _Director get _value => super._value as _Director;

  @override
  $Res call({
    Object? name = freezed,
    Object? id = freezed,
    Object? profilePath = freezed,
  }) {
    return _then(_Director(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      profilePath: profilePath == freezed
          ? _value.profilePath
          : profilePath // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Director implements _Director {
  const _$_Director(
      {required this.name, required this.id, required this.profilePath});

  factory _$_Director.fromJson(Map<String, dynamic> json) =>
      _$$_DirectorFromJson(json);

  @override
  final String name;
  @override
  final int id;
  @override
  final String profilePath;

  @override
  String toString() {
    return 'Director(name: $name, id: $id, profilePath: $profilePath)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Director &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.profilePath, profilePath) ||
                const DeepCollectionEquality()
                    .equals(other.profilePath, profilePath)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(profilePath);

  @JsonKey(ignore: true)
  @override
  _$DirectorCopyWith<_Director> get copyWith =>
      __$DirectorCopyWithImpl<_Director>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DirectorToJson(this);
  }
}

abstract class _Director implements Director {
  const factory _Director(
      {required String name,
      required int id,
      required String profilePath}) = _$_Director;

  factory _Director.fromJson(Map<String, dynamic> json) = _$_Director.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  int get id => throw _privateConstructorUsedError;
  @override
  String get profilePath => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$DirectorCopyWith<_Director> get copyWith =>
      throw _privateConstructorUsedError;
}
