import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
part 'cast.freezed.dart';
part 'cast.g.dart';

Cast castFromJson(String str) => Cast.fromJson(json.decode(str));

String castToJson(Cast data) => json.encode(data.toJson());

@freezed
class Cast with _$Cast {
  const factory Cast({
    required double popularity,
    required String name,
    required String characterName,
    required int id,
    required String profilePath,
  }) = _Cast;

  factory Cast.fromJson(Map<String, dynamic> json) => _$CastFromJson(json);
}
