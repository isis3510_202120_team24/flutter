// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Movie _$$_MovieFromJson(Map<String, dynamic> json) => _$_Movie(
      overview: json['overview'] as String? ?? '',
      releaseDate: json['releaseDate'] as String? ?? '',
      directors: (json['directors'] as List<dynamic>?)
              ?.map((e) => Director.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [Director(name: '', id: 0, profilePath: '')],
      title: json['title'] as String? ?? '',
      numLikes: json['numLikes'] as int? ?? -1,
      ratingScore: (json['ratingScore'] as num?)?.toDouble() ?? -1.0,
      cast: (json['cast'] as List<dynamic>?)
              ?.map((e) => Cast.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [
            Cast(
                popularity: 0.0,
                name: '',
                characterName: '',
                id: 0,
                profilePath: '')
          ],
      tmdbId: json['tmdbId'] as int? ?? -1,
      genres: (json['genres'] as List<dynamic>?)
              ?.map((e) => Genre.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [Genre(name: 'no_category')],
      id: json['id'] as int? ?? -1,
      runTime: json['runTime'] as int? ?? -1,
      adult: json['adult'] as bool? ?? false,
      posterComplete: json['posterComplete'] as String? ?? '',
      budget: json['budget'] as int? ?? -1,
      plataformsByCountry: (json['plataformsByCountry'] as List<dynamic>?)
              ?.map((e) => SPCountry.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [
            SPCountry(
                streamingPlataform:
                    StreamingPlataform(logoPath: '', id: -1, providerName: ''),
                countryId: '00')
          ],
    );

Map<String, dynamic> _$$_MovieToJson(_$_Movie instance) => <String, dynamic>{
      'overview': instance.overview,
      'releaseDate': instance.releaseDate,
      'directors': instance.directors,
      'title': instance.title,
      'numLikes': instance.numLikes,
      'ratingScore': instance.ratingScore,
      'cast': instance.cast,
      'tmdbId': instance.tmdbId,
      'genres': instance.genres,
      'id': instance.id,
      'runTime': instance.runTime,
      'adult': instance.adult,
      'posterComplete': instance.posterComplete,
      'budget': instance.budget,
      'plataformsByCountry': instance.plataformsByCountry,
    };
