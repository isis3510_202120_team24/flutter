// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'streamingplatform.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_StreamingPlataform _$$_StreamingPlataformFromJson(
        Map<String, dynamic> json) =>
    _$_StreamingPlataform(
      logoPath: json['logoPath'] as String,
      id: json['id'] as int,
      providerName: json['providerName'] as String,
    );

Map<String, dynamic> _$$_StreamingPlataformToJson(
        _$_StreamingPlataform instance) =>
    <String, dynamic>{
      'logoPath': instance.logoPath,
      'id': instance.id,
      'providerName': instance.providerName,
    };
