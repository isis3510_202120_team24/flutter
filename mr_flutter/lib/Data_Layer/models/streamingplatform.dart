import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
part 'streamingplatform.freezed.dart';
part 'streamingplatform.g.dart';

StreamingPlataform streamingPlatformFromJson(String str) =>
    StreamingPlataform.fromJson(json.decode(str));

String streamingPlatformToJson(StreamingPlataform data) =>
    json.encode(data.toJson());

@freezed
class StreamingPlataform with _$StreamingPlataform {
  const factory StreamingPlataform({
    required String logoPath,
    required int id,
    required String providerName,
  }) = _StreamingPlataform;

  factory StreamingPlataform.fromJson(Map<String, dynamic> json) =>
      _$StreamingPlataformFromJson(json);
}
