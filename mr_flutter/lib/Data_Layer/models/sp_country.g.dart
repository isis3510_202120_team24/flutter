// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sp_country.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SPCountry _$$_SPCountryFromJson(Map<String, dynamic> json) => _$_SPCountry(
      streamingPlataform: StreamingPlataform.fromJson(
          json['streamingPlataform'] as Map<String, dynamic>),
      countryId: json['countryId'] as String,
    );

Map<String, dynamic> _$$_SPCountryToJson(_$_SPCountry instance) =>
    <String, dynamic>{
      'streamingPlataform': instance.streamingPlataform,
      'countryId': instance.countryId,
    };
