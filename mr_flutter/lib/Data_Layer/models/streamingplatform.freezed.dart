// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'streamingplatform.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

StreamingPlataform _$StreamingPlataformFromJson(Map<String, dynamic> json) {
  return _StreamingPlataform.fromJson(json);
}

/// @nodoc
class _$StreamingPlataformTearOff {
  const _$StreamingPlataformTearOff();

  _StreamingPlataform call(
      {required String logoPath,
      required int id,
      required String providerName}) {
    return _StreamingPlataform(
      logoPath: logoPath,
      id: id,
      providerName: providerName,
    );
  }

  StreamingPlataform fromJson(Map<String, Object> json) {
    return StreamingPlataform.fromJson(json);
  }
}

/// @nodoc
const $StreamingPlataform = _$StreamingPlataformTearOff();

/// @nodoc
mixin _$StreamingPlataform {
  String get logoPath => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  String get providerName => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StreamingPlataformCopyWith<StreamingPlataform> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StreamingPlataformCopyWith<$Res> {
  factory $StreamingPlataformCopyWith(
          StreamingPlataform value, $Res Function(StreamingPlataform) then) =
      _$StreamingPlataformCopyWithImpl<$Res>;
  $Res call({String logoPath, int id, String providerName});
}

/// @nodoc
class _$StreamingPlataformCopyWithImpl<$Res>
    implements $StreamingPlataformCopyWith<$Res> {
  _$StreamingPlataformCopyWithImpl(this._value, this._then);

  final StreamingPlataform _value;
  // ignore: unused_field
  final $Res Function(StreamingPlataform) _then;

  @override
  $Res call({
    Object? logoPath = freezed,
    Object? id = freezed,
    Object? providerName = freezed,
  }) {
    return _then(_value.copyWith(
      logoPath: logoPath == freezed
          ? _value.logoPath
          : logoPath // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      providerName: providerName == freezed
          ? _value.providerName
          : providerName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$StreamingPlataformCopyWith<$Res>
    implements $StreamingPlataformCopyWith<$Res> {
  factory _$StreamingPlataformCopyWith(
          _StreamingPlataform value, $Res Function(_StreamingPlataform) then) =
      __$StreamingPlataformCopyWithImpl<$Res>;
  @override
  $Res call({String logoPath, int id, String providerName});
}

/// @nodoc
class __$StreamingPlataformCopyWithImpl<$Res>
    extends _$StreamingPlataformCopyWithImpl<$Res>
    implements _$StreamingPlataformCopyWith<$Res> {
  __$StreamingPlataformCopyWithImpl(
      _StreamingPlataform _value, $Res Function(_StreamingPlataform) _then)
      : super(_value, (v) => _then(v as _StreamingPlataform));

  @override
  _StreamingPlataform get _value => super._value as _StreamingPlataform;

  @override
  $Res call({
    Object? logoPath = freezed,
    Object? id = freezed,
    Object? providerName = freezed,
  }) {
    return _then(_StreamingPlataform(
      logoPath: logoPath == freezed
          ? _value.logoPath
          : logoPath // ignore: cast_nullable_to_non_nullable
              as String,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      providerName: providerName == freezed
          ? _value.providerName
          : providerName // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_StreamingPlataform implements _StreamingPlataform {
  const _$_StreamingPlataform(
      {required this.logoPath, required this.id, required this.providerName});

  factory _$_StreamingPlataform.fromJson(Map<String, dynamic> json) =>
      _$$_StreamingPlataformFromJson(json);

  @override
  final String logoPath;
  @override
  final int id;
  @override
  final String providerName;

  @override
  String toString() {
    return 'StreamingPlataform(logoPath: $logoPath, id: $id, providerName: $providerName)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _StreamingPlataform &&
            (identical(other.logoPath, logoPath) ||
                const DeepCollectionEquality()
                    .equals(other.logoPath, logoPath)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.providerName, providerName) ||
                const DeepCollectionEquality()
                    .equals(other.providerName, providerName)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(logoPath) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(providerName);

  @JsonKey(ignore: true)
  @override
  _$StreamingPlataformCopyWith<_StreamingPlataform> get copyWith =>
      __$StreamingPlataformCopyWithImpl<_StreamingPlataform>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StreamingPlataformToJson(this);
  }
}

abstract class _StreamingPlataform implements StreamingPlataform {
  const factory _StreamingPlataform(
      {required String logoPath,
      required int id,
      required String providerName}) = _$_StreamingPlataform;

  factory _StreamingPlataform.fromJson(Map<String, dynamic> json) =
      _$_StreamingPlataform.fromJson;

  @override
  String get logoPath => throw _privateConstructorUsedError;
  @override
  int get id => throw _privateConstructorUsedError;
  @override
  String get providerName => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$StreamingPlataformCopyWith<_StreamingPlataform> get copyWith =>
      throw _privateConstructorUsedError;
}
