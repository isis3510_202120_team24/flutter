// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return _Movie.fromJson(json);
}

/// @nodoc
class _$MovieTearOff {
  const _$MovieTearOff();

  _Movie call(
      {String overview = '',
      String releaseDate = '',
      @JsonKey() List<Director> directors = const [
        Director(name: '', id: 0, profilePath: '')
      ],
      String title = '',
      int numLikes = -1,
      double ratingScore = -1.0,
      @JsonKey() List<Cast> cast = const [
        Cast(
            popularity: 0.0,
            name: '',
            characterName: '',
            id: 0,
            profilePath: '')
      ],
      int tmdbId = -1,
      @JsonKey() List<Genre> genres = const [Genre(name: 'no_category')],
      int id = -1,
      int runTime = -1,
      bool adult = false,
      String posterComplete = '',
      int budget = -1,
      @JsonKey() List<SPCountry> plataformsByCountry = const [
        SPCountry(
            streamingPlataform:
                StreamingPlataform(logoPath: '', id: -1, providerName: ''),
            countryId: '00')
      ]}) {
    return _Movie(
      overview: overview,
      releaseDate: releaseDate,
      directors: directors,
      title: title,
      numLikes: numLikes,
      ratingScore: ratingScore,
      cast: cast,
      tmdbId: tmdbId,
      genres: genres,
      id: id,
      runTime: runTime,
      adult: adult,
      posterComplete: posterComplete,
      budget: budget,
      plataformsByCountry: plataformsByCountry,
    );
  }

  Movie fromJson(Map<String, Object> json) {
    return Movie.fromJson(json);
  }
}

/// @nodoc
const $Movie = _$MovieTearOff();

/// @nodoc
mixin _$Movie {
  String get overview => throw _privateConstructorUsedError;
  String get releaseDate => throw _privateConstructorUsedError;
  @JsonKey()
  List<Director> get directors => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  int get numLikes => throw _privateConstructorUsedError;
  double get ratingScore => throw _privateConstructorUsedError;
  @JsonKey()
  List<Cast> get cast => throw _privateConstructorUsedError;
  int get tmdbId => throw _privateConstructorUsedError;
  @JsonKey()
  List<Genre> get genres => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  int get runTime => throw _privateConstructorUsedError;
  bool get adult => throw _privateConstructorUsedError;
  String get posterComplete => throw _privateConstructorUsedError;
  int get budget => throw _privateConstructorUsedError;
  @JsonKey()
  List<SPCountry> get plataformsByCountry => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieCopyWith<Movie> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieCopyWith<$Res> {
  factory $MovieCopyWith(Movie value, $Res Function(Movie) then) =
      _$MovieCopyWithImpl<$Res>;
  $Res call(
      {String overview,
      String releaseDate,
      @JsonKey() List<Director> directors,
      String title,
      int numLikes,
      double ratingScore,
      @JsonKey() List<Cast> cast,
      int tmdbId,
      @JsonKey() List<Genre> genres,
      int id,
      int runTime,
      bool adult,
      String posterComplete,
      int budget,
      @JsonKey() List<SPCountry> plataformsByCountry});
}

/// @nodoc
class _$MovieCopyWithImpl<$Res> implements $MovieCopyWith<$Res> {
  _$MovieCopyWithImpl(this._value, this._then);

  final Movie _value;
  // ignore: unused_field
  final $Res Function(Movie) _then;

  @override
  $Res call({
    Object? overview = freezed,
    Object? releaseDate = freezed,
    Object? directors = freezed,
    Object? title = freezed,
    Object? numLikes = freezed,
    Object? ratingScore = freezed,
    Object? cast = freezed,
    Object? tmdbId = freezed,
    Object? genres = freezed,
    Object? id = freezed,
    Object? runTime = freezed,
    Object? adult = freezed,
    Object? posterComplete = freezed,
    Object? budget = freezed,
    Object? plataformsByCountry = freezed,
  }) {
    return _then(_value.copyWith(
      overview: overview == freezed
          ? _value.overview
          : overview // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      directors: directors == freezed
          ? _value.directors
          : directors // ignore: cast_nullable_to_non_nullable
              as List<Director>,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      numLikes: numLikes == freezed
          ? _value.numLikes
          : numLikes // ignore: cast_nullable_to_non_nullable
              as int,
      ratingScore: ratingScore == freezed
          ? _value.ratingScore
          : ratingScore // ignore: cast_nullable_to_non_nullable
              as double,
      cast: cast == freezed
          ? _value.cast
          : cast // ignore: cast_nullable_to_non_nullable
              as List<Cast>,
      tmdbId: tmdbId == freezed
          ? _value.tmdbId
          : tmdbId // ignore: cast_nullable_to_non_nullable
              as int,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      runTime: runTime == freezed
          ? _value.runTime
          : runTime // ignore: cast_nullable_to_non_nullable
              as int,
      adult: adult == freezed
          ? _value.adult
          : adult // ignore: cast_nullable_to_non_nullable
              as bool,
      posterComplete: posterComplete == freezed
          ? _value.posterComplete
          : posterComplete // ignore: cast_nullable_to_non_nullable
              as String,
      budget: budget == freezed
          ? _value.budget
          : budget // ignore: cast_nullable_to_non_nullable
              as int,
      plataformsByCountry: plataformsByCountry == freezed
          ? _value.plataformsByCountry
          : plataformsByCountry // ignore: cast_nullable_to_non_nullable
              as List<SPCountry>,
    ));
  }
}

/// @nodoc
abstract class _$MovieCopyWith<$Res> implements $MovieCopyWith<$Res> {
  factory _$MovieCopyWith(_Movie value, $Res Function(_Movie) then) =
      __$MovieCopyWithImpl<$Res>;
  @override
  $Res call(
      {String overview,
      String releaseDate,
      @JsonKey() List<Director> directors,
      String title,
      int numLikes,
      double ratingScore,
      @JsonKey() List<Cast> cast,
      int tmdbId,
      @JsonKey() List<Genre> genres,
      int id,
      int runTime,
      bool adult,
      String posterComplete,
      int budget,
      @JsonKey() List<SPCountry> plataformsByCountry});
}

/// @nodoc
class __$MovieCopyWithImpl<$Res> extends _$MovieCopyWithImpl<$Res>
    implements _$MovieCopyWith<$Res> {
  __$MovieCopyWithImpl(_Movie _value, $Res Function(_Movie) _then)
      : super(_value, (v) => _then(v as _Movie));

  @override
  _Movie get _value => super._value as _Movie;

  @override
  $Res call({
    Object? overview = freezed,
    Object? releaseDate = freezed,
    Object? directors = freezed,
    Object? title = freezed,
    Object? numLikes = freezed,
    Object? ratingScore = freezed,
    Object? cast = freezed,
    Object? tmdbId = freezed,
    Object? genres = freezed,
    Object? id = freezed,
    Object? runTime = freezed,
    Object? adult = freezed,
    Object? posterComplete = freezed,
    Object? budget = freezed,
    Object? plataformsByCountry = freezed,
  }) {
    return _then(_Movie(
      overview: overview == freezed
          ? _value.overview
          : overview // ignore: cast_nullable_to_non_nullable
              as String,
      releaseDate: releaseDate == freezed
          ? _value.releaseDate
          : releaseDate // ignore: cast_nullable_to_non_nullable
              as String,
      directors: directors == freezed
          ? _value.directors
          : directors // ignore: cast_nullable_to_non_nullable
              as List<Director>,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      numLikes: numLikes == freezed
          ? _value.numLikes
          : numLikes // ignore: cast_nullable_to_non_nullable
              as int,
      ratingScore: ratingScore == freezed
          ? _value.ratingScore
          : ratingScore // ignore: cast_nullable_to_non_nullable
              as double,
      cast: cast == freezed
          ? _value.cast
          : cast // ignore: cast_nullable_to_non_nullable
              as List<Cast>,
      tmdbId: tmdbId == freezed
          ? _value.tmdbId
          : tmdbId // ignore: cast_nullable_to_non_nullable
              as int,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      runTime: runTime == freezed
          ? _value.runTime
          : runTime // ignore: cast_nullable_to_non_nullable
              as int,
      adult: adult == freezed
          ? _value.adult
          : adult // ignore: cast_nullable_to_non_nullable
              as bool,
      posterComplete: posterComplete == freezed
          ? _value.posterComplete
          : posterComplete // ignore: cast_nullable_to_non_nullable
              as String,
      budget: budget == freezed
          ? _value.budget
          : budget // ignore: cast_nullable_to_non_nullable
              as int,
      plataformsByCountry: plataformsByCountry == freezed
          ? _value.plataformsByCountry
          : plataformsByCountry // ignore: cast_nullable_to_non_nullable
              as List<SPCountry>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Movie implements _Movie {
  const _$_Movie(
      {this.overview = '',
      this.releaseDate = '',
      @JsonKey()
          this.directors = const [Director(name: '', id: 0, profilePath: '')],
      this.title = '',
      this.numLikes = -1,
      this.ratingScore = -1.0,
      @JsonKey()
          this.cast = const [
        Cast(
            popularity: 0.0,
            name: '',
            characterName: '',
            id: 0,
            profilePath: '')
      ],
      this.tmdbId = -1,
      @JsonKey()
          this.genres = const [Genre(name: 'no_category')],
      this.id = -1,
      this.runTime = -1,
      this.adult = false,
      this.posterComplete = '',
      this.budget = -1,
      @JsonKey()
          this.plataformsByCountry = const [
        SPCountry(
            streamingPlataform:
                StreamingPlataform(logoPath: '', id: -1, providerName: ''),
            countryId: '00')
      ]});

  factory _$_Movie.fromJson(Map<String, dynamic> json) =>
      _$$_MovieFromJson(json);

  @JsonKey(defaultValue: '')
  @override
  final String overview;
  @JsonKey(defaultValue: '')
  @override
  final String releaseDate;
  @override
  @JsonKey()
  final List<Director> directors;
  @JsonKey(defaultValue: '')
  @override
  final String title;
  @JsonKey(defaultValue: -1)
  @override
  final int numLikes;
  @JsonKey(defaultValue: -1.0)
  @override
  final double ratingScore;
  @override
  @JsonKey()
  final List<Cast> cast;
  @JsonKey(defaultValue: -1)
  @override
  final int tmdbId;
  @override
  @JsonKey()
  final List<Genre> genres;
  @JsonKey(defaultValue: -1)
  @override
  final int id;
  @JsonKey(defaultValue: -1)
  @override
  final int runTime;
  @JsonKey(defaultValue: false)
  @override
  final bool adult;
  @JsonKey(defaultValue: '')
  @override
  final String posterComplete;
  @JsonKey(defaultValue: -1)
  @override
  final int budget;
  @override
  @JsonKey()
  final List<SPCountry> plataformsByCountry;

  @override
  String toString() {
    return 'Movie(overview: $overview, releaseDate: $releaseDate, directors: $directors, title: $title, numLikes: $numLikes, ratingScore: $ratingScore, cast: $cast, tmdbId: $tmdbId, genres: $genres, id: $id, runTime: $runTime, adult: $adult, posterComplete: $posterComplete, budget: $budget, plataformsByCountry: $plataformsByCountry)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Movie &&
            (identical(other.overview, overview) ||
                const DeepCollectionEquality()
                    .equals(other.overview, overview)) &&
            (identical(other.releaseDate, releaseDate) ||
                const DeepCollectionEquality()
                    .equals(other.releaseDate, releaseDate)) &&
            (identical(other.directors, directors) ||
                const DeepCollectionEquality()
                    .equals(other.directors, directors)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.numLikes, numLikes) ||
                const DeepCollectionEquality()
                    .equals(other.numLikes, numLikes)) &&
            (identical(other.ratingScore, ratingScore) ||
                const DeepCollectionEquality()
                    .equals(other.ratingScore, ratingScore)) &&
            (identical(other.cast, cast) ||
                const DeepCollectionEquality().equals(other.cast, cast)) &&
            (identical(other.tmdbId, tmdbId) ||
                const DeepCollectionEquality().equals(other.tmdbId, tmdbId)) &&
            (identical(other.genres, genres) ||
                const DeepCollectionEquality().equals(other.genres, genres)) &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.runTime, runTime) ||
                const DeepCollectionEquality()
                    .equals(other.runTime, runTime)) &&
            (identical(other.adult, adult) ||
                const DeepCollectionEquality().equals(other.adult, adult)) &&
            (identical(other.posterComplete, posterComplete) ||
                const DeepCollectionEquality()
                    .equals(other.posterComplete, posterComplete)) &&
            (identical(other.budget, budget) ||
                const DeepCollectionEquality().equals(other.budget, budget)) &&
            (identical(other.plataformsByCountry, plataformsByCountry) ||
                const DeepCollectionEquality()
                    .equals(other.plataformsByCountry, plataformsByCountry)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(overview) ^
      const DeepCollectionEquality().hash(releaseDate) ^
      const DeepCollectionEquality().hash(directors) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(numLikes) ^
      const DeepCollectionEquality().hash(ratingScore) ^
      const DeepCollectionEquality().hash(cast) ^
      const DeepCollectionEquality().hash(tmdbId) ^
      const DeepCollectionEquality().hash(genres) ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(runTime) ^
      const DeepCollectionEquality().hash(adult) ^
      const DeepCollectionEquality().hash(posterComplete) ^
      const DeepCollectionEquality().hash(budget) ^
      const DeepCollectionEquality().hash(plataformsByCountry);

  @JsonKey(ignore: true)
  @override
  _$MovieCopyWith<_Movie> get copyWith =>
      __$MovieCopyWithImpl<_Movie>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieToJson(this);
  }
}

abstract class _Movie implements Movie {
  const factory _Movie(
      {String overview,
      String releaseDate,
      @JsonKey() List<Director> directors,
      String title,
      int numLikes,
      double ratingScore,
      @JsonKey() List<Cast> cast,
      int tmdbId,
      @JsonKey() List<Genre> genres,
      int id,
      int runTime,
      bool adult,
      String posterComplete,
      int budget,
      @JsonKey() List<SPCountry> plataformsByCountry}) = _$_Movie;

  factory _Movie.fromJson(Map<String, dynamic> json) = _$_Movie.fromJson;

  @override
  String get overview => throw _privateConstructorUsedError;
  @override
  String get releaseDate => throw _privateConstructorUsedError;
  @override
  @JsonKey()
  List<Director> get directors => throw _privateConstructorUsedError;
  @override
  String get title => throw _privateConstructorUsedError;
  @override
  int get numLikes => throw _privateConstructorUsedError;
  @override
  double get ratingScore => throw _privateConstructorUsedError;
  @override
  @JsonKey()
  List<Cast> get cast => throw _privateConstructorUsedError;
  @override
  int get tmdbId => throw _privateConstructorUsedError;
  @override
  @JsonKey()
  List<Genre> get genres => throw _privateConstructorUsedError;
  @override
  int get id => throw _privateConstructorUsedError;
  @override
  int get runTime => throw _privateConstructorUsedError;
  @override
  bool get adult => throw _privateConstructorUsedError;
  @override
  String get posterComplete => throw _privateConstructorUsedError;
  @override
  int get budget => throw _privateConstructorUsedError;
  @override
  @JsonKey()
  List<SPCountry> get plataformsByCountry => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MovieCopyWith<_Movie> get copyWith => throw _privateConstructorUsedError;
}
