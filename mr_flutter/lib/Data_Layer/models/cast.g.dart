// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cast.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Cast _$$_CastFromJson(Map<String, dynamic> json) => _$_Cast(
      popularity: (json['popularity'] as num).toDouble(),
      name: json['name'] as String,
      characterName: json['characterName'] as String,
      id: json['id'] as int,
      profilePath: json['profilePath'] as String,
    );

Map<String, dynamic> _$$_CastToJson(_$_Cast instance) => <String, dynamic>{
      'popularity': instance.popularity,
      'name': instance.name,
      'characterName': instance.characterName,
      'id': instance.id,
      'profilePath': instance.profilePath,
    };
