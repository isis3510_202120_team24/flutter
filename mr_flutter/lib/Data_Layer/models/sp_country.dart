import 'streamingplatform.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'sp_country.freezed.dart';
part 'sp_country.g.dart';

SPCountry spCountryFromJson(String str) => SPCountry.fromJson(json.decode(str));

String spCountryformToJson(SPCountry data) => json.encode(data.toJson());

@freezed
abstract class SPCountry with _$SPCountry {
  const factory SPCountry({
    required StreamingPlataform streamingPlataform,
    required String countryId,
  }) = _SPCountry;

  factory SPCountry.fromJson(Map<String, dynamic> json) =>
      _$SPCountryFromJson(json);
}
