import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
part 'director.freezed.dart';
part 'director.g.dart';

Director directorFromJson(String str) => Director.fromJson(json.decode(str));

String directorToJson(Director data) => json.encode(data.toJson());

@freezed
class Director with _$Director {
  const factory Director({
    required String name,
    required int id,
    required String profilePath,
  }) = _Director;

  factory Director.fromJson(Map<String, dynamic> json) =>
      _$DirectorFromJson(json);
}
