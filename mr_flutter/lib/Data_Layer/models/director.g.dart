// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'director.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Director _$$_DirectorFromJson(Map<String, dynamic> json) => _$_Director(
      name: json['name'] as String,
      id: json['id'] as int,
      profilePath: json['profilePath'] as String,
    );

Map<String, dynamic> _$$_DirectorToJson(_$_Director instance) =>
    <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
      'profilePath': instance.profilePath,
    };
