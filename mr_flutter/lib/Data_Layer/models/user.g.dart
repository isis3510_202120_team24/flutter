// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_User _$$_UserFromJson(Map<String, dynamic> json) => _$_User(
      email: json['email'] as String,
      numRecomendedMovies: json['numRecomendedMovies'] as int,
      numLikedRecomendedMovies: json['numLikedRecomendedMovies'] as int,
      timeInApp: json['timeInApp'] as int,
      avgPassBeforeLike: (json['avgPassBeforeLike'] as num?)?.toDouble() ?? 1.0,
      numLogin: json['numLogin'] as int,
      image: json['image'] as String,
      name: json['name'] as String,
      birthday: json['birthday'] as String,
      lists: (json['lists'] as List<dynamic>?)
              ?.map((e) => ListElement.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
      stats: (json['stats'] as List<dynamic>?)
              ?.map((e) => Stat.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [],
    );

Map<String, dynamic> _$$_UserToJson(_$_User instance) => <String, dynamic>{
      'email': instance.email,
      'numRecomendedMovies': instance.numRecomendedMovies,
      'numLikedRecomendedMovies': instance.numLikedRecomendedMovies,
      'timeInApp': instance.timeInApp,
      'avgPassBeforeLike': instance.avgPassBeforeLike,
      'numLogin': instance.numLogin,
      'image': instance.image,
      'name': instance.name,
      'birthday': instance.birthday,
      'lists': instance.lists,
      'stats': instance.stats,
    };

_$_ListElement _$$_ListElementFromJson(Map<String, dynamic> json) =>
    _$_ListElement(
      userEmail: json['userEmail'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_ListElementToJson(_$_ListElement instance) =>
    <String, dynamic>{
      'userEmail': instance.userEmail,
      'name': instance.name,
    };

_$_Stat _$$_StatFromJson(Map<String, dynamic> json) => _$_Stat(
      userEmail: json['userEmail'] as String,
      genreId: json['genreId'] as int,
      genre: GenreInfo.fromJson(json['genre'] as Map<String, dynamic>),
      amount: json['amount'] as int,
    );

Map<String, dynamic> _$$_StatToJson(_$_Stat instance) => <String, dynamic>{
      'userEmail': instance.userEmail,
      'genreId': instance.genreId,
      'genre': instance.genre,
      'amount': instance.amount,
    };

_$_GenreInfo _$$_GenreInfoFromJson(Map<String, dynamic> json) => _$_GenreInfo(
      id: json['id'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_GenreInfoToJson(_$_GenreInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
