// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';

part 'user.freezed.dart';
part 'user.g.dart';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

@freezed
abstract class User with _$User {
  const factory User({
    required String email,
    required int numRecomendedMovies,
    required int numLikedRecomendedMovies,
    required int timeInApp,
    @Default(1.0) double avgPassBeforeLike,
    required int numLogin,
    required String image,
    required String name,
    required String birthday,
    @Default([]) List<ListElement> lists,
    @Default([]) List<Stat> stats,
  }) = _User;

  static const empty = User(
      email: '',
      numRecomendedMovies: 0,
      numLikedRecomendedMovies: 0,
      timeInApp: 0,
      avgPassBeforeLike: 0,
      numLogin: 0,
      image: '',
      name: '',
      birthday: '',
      lists: [],
      stats: []);
  static const RegisterUser = User(
      email: 'kola@gmail.com',
      numRecomendedMovies: 0,
      numLikedRecomendedMovies: 0,
      timeInApp: 0,
      avgPassBeforeLike: 0.0,
      numLogin: 0,
      image: '',
      name: '',
      birthday: '',
      lists: [],
      stats: []);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

@freezed
abstract class ListElement with _$ListElement {
  const factory ListElement({
    required String userEmail,
    required String name,
  }) = _ListElement;

  factory ListElement.fromJson(Map<String, dynamic> json) =>
      _$ListElementFromJson(json);
}

@freezed
abstract class Stat with _$Stat {
  const factory Stat({
    required String userEmail,
    required int genreId,
    required GenreInfo genre,
    required int amount,
  }) = _Stat;

  factory Stat.fromJson(Map<String, dynamic> json) => _$StatFromJson(json);
}

@freezed
abstract class GenreInfo with _$GenreInfo {
  const factory GenreInfo({
    required int id,
    required String name,
  }) = _GenreInfo;

  factory GenreInfo.fromJson(Map<String, dynamic> json) =>
      _$GenreInfoFromJson(json);
}
