import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
part 'listU.freezed.dart';
part 'listU.g.dart';

@freezed
abstract class ListU with _$ListU {
  const factory ListU({
    @Default('') String name,
    @Default([]) List<Movie> movies,
  }) = _ListU;

  factory ListU.fromJson(Map<String, dynamic> json) => _$ListUFromJson(json);
}
