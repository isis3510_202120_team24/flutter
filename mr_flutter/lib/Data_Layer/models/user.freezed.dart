// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

User _$UserFromJson(Map<String, dynamic> json) {
  return _User.fromJson(json);
}

/// @nodoc
class _$UserTearOff {
  const _$UserTearOff();

  _User call(
      {required String email,
      required int numRecomendedMovies,
      required int numLikedRecomendedMovies,
      required int timeInApp,
      double avgPassBeforeLike = 1.0,
      required int numLogin,
      required String image,
      required String name,
      required String birthday,
      List<ListElement> lists = const [],
      List<Stat> stats = const []}) {
    return _User(
      email: email,
      numRecomendedMovies: numRecomendedMovies,
      numLikedRecomendedMovies: numLikedRecomendedMovies,
      timeInApp: timeInApp,
      avgPassBeforeLike: avgPassBeforeLike,
      numLogin: numLogin,
      image: image,
      name: name,
      birthday: birthday,
      lists: lists,
      stats: stats,
    );
  }

  User fromJson(Map<String, Object> json) {
    return User.fromJson(json);
  }
}

/// @nodoc
const $User = _$UserTearOff();

/// @nodoc
mixin _$User {
  String get email => throw _privateConstructorUsedError;
  int get numRecomendedMovies => throw _privateConstructorUsedError;
  int get numLikedRecomendedMovies => throw _privateConstructorUsedError;
  int get timeInApp => throw _privateConstructorUsedError;
  double get avgPassBeforeLike => throw _privateConstructorUsedError;
  int get numLogin => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get birthday => throw _privateConstructorUsedError;
  List<ListElement> get lists => throw _privateConstructorUsedError;
  List<Stat> get stats => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res>;
  $Res call(
      {String email,
      int numRecomendedMovies,
      int numLikedRecomendedMovies,
      int timeInApp,
      double avgPassBeforeLike,
      int numLogin,
      String image,
      String name,
      String birthday,
      List<ListElement> lists,
      List<Stat> stats});
}

/// @nodoc
class _$UserCopyWithImpl<$Res> implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  final User _value;
  // ignore: unused_field
  final $Res Function(User) _then;

  @override
  $Res call({
    Object? email = freezed,
    Object? numRecomendedMovies = freezed,
    Object? numLikedRecomendedMovies = freezed,
    Object? timeInApp = freezed,
    Object? avgPassBeforeLike = freezed,
    Object? numLogin = freezed,
    Object? image = freezed,
    Object? name = freezed,
    Object? birthday = freezed,
    Object? lists = freezed,
    Object? stats = freezed,
  }) {
    return _then(_value.copyWith(
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      numRecomendedMovies: numRecomendedMovies == freezed
          ? _value.numRecomendedMovies
          : numRecomendedMovies // ignore: cast_nullable_to_non_nullable
              as int,
      numLikedRecomendedMovies: numLikedRecomendedMovies == freezed
          ? _value.numLikedRecomendedMovies
          : numLikedRecomendedMovies // ignore: cast_nullable_to_non_nullable
              as int,
      timeInApp: timeInApp == freezed
          ? _value.timeInApp
          : timeInApp // ignore: cast_nullable_to_non_nullable
              as int,
      avgPassBeforeLike: avgPassBeforeLike == freezed
          ? _value.avgPassBeforeLike
          : avgPassBeforeLike // ignore: cast_nullable_to_non_nullable
              as double,
      numLogin: numLogin == freezed
          ? _value.numLogin
          : numLogin // ignore: cast_nullable_to_non_nullable
              as int,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      birthday: birthday == freezed
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as String,
      lists: lists == freezed
          ? _value.lists
          : lists // ignore: cast_nullable_to_non_nullable
              as List<ListElement>,
      stats: stats == freezed
          ? _value.stats
          : stats // ignore: cast_nullable_to_non_nullable
              as List<Stat>,
    ));
  }
}

/// @nodoc
abstract class _$UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$UserCopyWith(_User value, $Res Function(_User) then) =
      __$UserCopyWithImpl<$Res>;
  @override
  $Res call(
      {String email,
      int numRecomendedMovies,
      int numLikedRecomendedMovies,
      int timeInApp,
      double avgPassBeforeLike,
      int numLogin,
      String image,
      String name,
      String birthday,
      List<ListElement> lists,
      List<Stat> stats});
}

/// @nodoc
class __$UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements _$UserCopyWith<$Res> {
  __$UserCopyWithImpl(_User _value, $Res Function(_User) _then)
      : super(_value, (v) => _then(v as _User));

  @override
  _User get _value => super._value as _User;

  @override
  $Res call({
    Object? email = freezed,
    Object? numRecomendedMovies = freezed,
    Object? numLikedRecomendedMovies = freezed,
    Object? timeInApp = freezed,
    Object? avgPassBeforeLike = freezed,
    Object? numLogin = freezed,
    Object? image = freezed,
    Object? name = freezed,
    Object? birthday = freezed,
    Object? lists = freezed,
    Object? stats = freezed,
  }) {
    return _then(_User(
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      numRecomendedMovies: numRecomendedMovies == freezed
          ? _value.numRecomendedMovies
          : numRecomendedMovies // ignore: cast_nullable_to_non_nullable
              as int,
      numLikedRecomendedMovies: numLikedRecomendedMovies == freezed
          ? _value.numLikedRecomendedMovies
          : numLikedRecomendedMovies // ignore: cast_nullable_to_non_nullable
              as int,
      timeInApp: timeInApp == freezed
          ? _value.timeInApp
          : timeInApp // ignore: cast_nullable_to_non_nullable
              as int,
      avgPassBeforeLike: avgPassBeforeLike == freezed
          ? _value.avgPassBeforeLike
          : avgPassBeforeLike // ignore: cast_nullable_to_non_nullable
              as double,
      numLogin: numLogin == freezed
          ? _value.numLogin
          : numLogin // ignore: cast_nullable_to_non_nullable
              as int,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      birthday: birthday == freezed
          ? _value.birthday
          : birthday // ignore: cast_nullable_to_non_nullable
              as String,
      lists: lists == freezed
          ? _value.lists
          : lists // ignore: cast_nullable_to_non_nullable
              as List<ListElement>,
      stats: stats == freezed
          ? _value.stats
          : stats // ignore: cast_nullable_to_non_nullable
              as List<Stat>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_User implements _User {
  const _$_User(
      {required this.email,
      required this.numRecomendedMovies,
      required this.numLikedRecomendedMovies,
      required this.timeInApp,
      this.avgPassBeforeLike = 1.0,
      required this.numLogin,
      required this.image,
      required this.name,
      required this.birthday,
      this.lists = const [],
      this.stats = const []});

  factory _$_User.fromJson(Map<String, dynamic> json) => _$$_UserFromJson(json);

  @override
  final String email;
  @override
  final int numRecomendedMovies;
  @override
  final int numLikedRecomendedMovies;
  @override
  final int timeInApp;
  @JsonKey(defaultValue: 1.0)
  @override
  final double avgPassBeforeLike;
  @override
  final int numLogin;
  @override
  final String image;
  @override
  final String name;
  @override
  final String birthday;
  @JsonKey(defaultValue: const [])
  @override
  final List<ListElement> lists;
  @JsonKey(defaultValue: const [])
  @override
  final List<Stat> stats;

  @override
  String toString() {
    return 'User(email: $email, numRecomendedMovies: $numRecomendedMovies, numLikedRecomendedMovies: $numLikedRecomendedMovies, timeInApp: $timeInApp, avgPassBeforeLike: $avgPassBeforeLike, numLogin: $numLogin, image: $image, name: $name, birthday: $birthday, lists: $lists, stats: $stats)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _User &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.numRecomendedMovies, numRecomendedMovies) ||
                const DeepCollectionEquality()
                    .equals(other.numRecomendedMovies, numRecomendedMovies)) &&
            (identical(
                    other.numLikedRecomendedMovies, numLikedRecomendedMovies) ||
                const DeepCollectionEquality().equals(
                    other.numLikedRecomendedMovies,
                    numLikedRecomendedMovies)) &&
            (identical(other.timeInApp, timeInApp) ||
                const DeepCollectionEquality()
                    .equals(other.timeInApp, timeInApp)) &&
            (identical(other.avgPassBeforeLike, avgPassBeforeLike) ||
                const DeepCollectionEquality()
                    .equals(other.avgPassBeforeLike, avgPassBeforeLike)) &&
            (identical(other.numLogin, numLogin) ||
                const DeepCollectionEquality()
                    .equals(other.numLogin, numLogin)) &&
            (identical(other.image, image) ||
                const DeepCollectionEquality().equals(other.image, image)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.birthday, birthday) ||
                const DeepCollectionEquality()
                    .equals(other.birthday, birthday)) &&
            (identical(other.lists, lists) ||
                const DeepCollectionEquality().equals(other.lists, lists)) &&
            (identical(other.stats, stats) ||
                const DeepCollectionEquality().equals(other.stats, stats)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(numRecomendedMovies) ^
      const DeepCollectionEquality().hash(numLikedRecomendedMovies) ^
      const DeepCollectionEquality().hash(timeInApp) ^
      const DeepCollectionEquality().hash(avgPassBeforeLike) ^
      const DeepCollectionEquality().hash(numLogin) ^
      const DeepCollectionEquality().hash(image) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(birthday) ^
      const DeepCollectionEquality().hash(lists) ^
      const DeepCollectionEquality().hash(stats);

  @JsonKey(ignore: true)
  @override
  _$UserCopyWith<_User> get copyWith =>
      __$UserCopyWithImpl<_User>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserToJson(this);
  }
}

abstract class _User implements User {
  const factory _User(
      {required String email,
      required int numRecomendedMovies,
      required int numLikedRecomendedMovies,
      required int timeInApp,
      double avgPassBeforeLike,
      required int numLogin,
      required String image,
      required String name,
      required String birthday,
      List<ListElement> lists,
      List<Stat> stats}) = _$_User;

  factory _User.fromJson(Map<String, dynamic> json) = _$_User.fromJson;

  @override
  String get email => throw _privateConstructorUsedError;
  @override
  int get numRecomendedMovies => throw _privateConstructorUsedError;
  @override
  int get numLikedRecomendedMovies => throw _privateConstructorUsedError;
  @override
  int get timeInApp => throw _privateConstructorUsedError;
  @override
  double get avgPassBeforeLike => throw _privateConstructorUsedError;
  @override
  int get numLogin => throw _privateConstructorUsedError;
  @override
  String get image => throw _privateConstructorUsedError;
  @override
  String get name => throw _privateConstructorUsedError;
  @override
  String get birthday => throw _privateConstructorUsedError;
  @override
  List<ListElement> get lists => throw _privateConstructorUsedError;
  @override
  List<Stat> get stats => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$UserCopyWith<_User> get copyWith => throw _privateConstructorUsedError;
}

ListElement _$ListElementFromJson(Map<String, dynamic> json) {
  return _ListElement.fromJson(json);
}

/// @nodoc
class _$ListElementTearOff {
  const _$ListElementTearOff();

  _ListElement call({required String userEmail, required String name}) {
    return _ListElement(
      userEmail: userEmail,
      name: name,
    );
  }

  ListElement fromJson(Map<String, Object> json) {
    return ListElement.fromJson(json);
  }
}

/// @nodoc
const $ListElement = _$ListElementTearOff();

/// @nodoc
mixin _$ListElement {
  String get userEmail => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListElementCopyWith<ListElement> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListElementCopyWith<$Res> {
  factory $ListElementCopyWith(
          ListElement value, $Res Function(ListElement) then) =
      _$ListElementCopyWithImpl<$Res>;
  $Res call({String userEmail, String name});
}

/// @nodoc
class _$ListElementCopyWithImpl<$Res> implements $ListElementCopyWith<$Res> {
  _$ListElementCopyWithImpl(this._value, this._then);

  final ListElement _value;
  // ignore: unused_field
  final $Res Function(ListElement) _then;

  @override
  $Res call({
    Object? userEmail = freezed,
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      userEmail: userEmail == freezed
          ? _value.userEmail
          : userEmail // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$ListElementCopyWith<$Res>
    implements $ListElementCopyWith<$Res> {
  factory _$ListElementCopyWith(
          _ListElement value, $Res Function(_ListElement) then) =
      __$ListElementCopyWithImpl<$Res>;
  @override
  $Res call({String userEmail, String name});
}

/// @nodoc
class __$ListElementCopyWithImpl<$Res> extends _$ListElementCopyWithImpl<$Res>
    implements _$ListElementCopyWith<$Res> {
  __$ListElementCopyWithImpl(
      _ListElement _value, $Res Function(_ListElement) _then)
      : super(_value, (v) => _then(v as _ListElement));

  @override
  _ListElement get _value => super._value as _ListElement;

  @override
  $Res call({
    Object? userEmail = freezed,
    Object? name = freezed,
  }) {
    return _then(_ListElement(
      userEmail: userEmail == freezed
          ? _value.userEmail
          : userEmail // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListElement implements _ListElement {
  const _$_ListElement({required this.userEmail, required this.name});

  factory _$_ListElement.fromJson(Map<String, dynamic> json) =>
      _$$_ListElementFromJson(json);

  @override
  final String userEmail;
  @override
  final String name;

  @override
  String toString() {
    return 'ListElement(userEmail: $userEmail, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ListElement &&
            (identical(other.userEmail, userEmail) ||
                const DeepCollectionEquality()
                    .equals(other.userEmail, userEmail)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userEmail) ^
      const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$ListElementCopyWith<_ListElement> get copyWith =>
      __$ListElementCopyWithImpl<_ListElement>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListElementToJson(this);
  }
}

abstract class _ListElement implements ListElement {
  const factory _ListElement(
      {required String userEmail, required String name}) = _$_ListElement;

  factory _ListElement.fromJson(Map<String, dynamic> json) =
      _$_ListElement.fromJson;

  @override
  String get userEmail => throw _privateConstructorUsedError;
  @override
  String get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ListElementCopyWith<_ListElement> get copyWith =>
      throw _privateConstructorUsedError;
}

Stat _$StatFromJson(Map<String, dynamic> json) {
  return _Stat.fromJson(json);
}

/// @nodoc
class _$StatTearOff {
  const _$StatTearOff();

  _Stat call(
      {required String userEmail,
      required int genreId,
      required GenreInfo genre,
      required int amount}) {
    return _Stat(
      userEmail: userEmail,
      genreId: genreId,
      genre: genre,
      amount: amount,
    );
  }

  Stat fromJson(Map<String, Object> json) {
    return Stat.fromJson(json);
  }
}

/// @nodoc
const $Stat = _$StatTearOff();

/// @nodoc
mixin _$Stat {
  String get userEmail => throw _privateConstructorUsedError;
  int get genreId => throw _privateConstructorUsedError;
  GenreInfo get genre => throw _privateConstructorUsedError;
  int get amount => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $StatCopyWith<Stat> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StatCopyWith<$Res> {
  factory $StatCopyWith(Stat value, $Res Function(Stat) then) =
      _$StatCopyWithImpl<$Res>;
  $Res call({String userEmail, int genreId, GenreInfo genre, int amount});

  $GenreInfoCopyWith<$Res> get genre;
}

/// @nodoc
class _$StatCopyWithImpl<$Res> implements $StatCopyWith<$Res> {
  _$StatCopyWithImpl(this._value, this._then);

  final Stat _value;
  // ignore: unused_field
  final $Res Function(Stat) _then;

  @override
  $Res call({
    Object? userEmail = freezed,
    Object? genreId = freezed,
    Object? genre = freezed,
    Object? amount = freezed,
  }) {
    return _then(_value.copyWith(
      userEmail: userEmail == freezed
          ? _value.userEmail
          : userEmail // ignore: cast_nullable_to_non_nullable
              as String,
      genreId: genreId == freezed
          ? _value.genreId
          : genreId // ignore: cast_nullable_to_non_nullable
              as int,
      genre: genre == freezed
          ? _value.genre
          : genre // ignore: cast_nullable_to_non_nullable
              as GenreInfo,
      amount: amount == freezed
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $GenreInfoCopyWith<$Res> get genre {
    return $GenreInfoCopyWith<$Res>(_value.genre, (value) {
      return _then(_value.copyWith(genre: value));
    });
  }
}

/// @nodoc
abstract class _$StatCopyWith<$Res> implements $StatCopyWith<$Res> {
  factory _$StatCopyWith(_Stat value, $Res Function(_Stat) then) =
      __$StatCopyWithImpl<$Res>;
  @override
  $Res call({String userEmail, int genreId, GenreInfo genre, int amount});

  @override
  $GenreInfoCopyWith<$Res> get genre;
}

/// @nodoc
class __$StatCopyWithImpl<$Res> extends _$StatCopyWithImpl<$Res>
    implements _$StatCopyWith<$Res> {
  __$StatCopyWithImpl(_Stat _value, $Res Function(_Stat) _then)
      : super(_value, (v) => _then(v as _Stat));

  @override
  _Stat get _value => super._value as _Stat;

  @override
  $Res call({
    Object? userEmail = freezed,
    Object? genreId = freezed,
    Object? genre = freezed,
    Object? amount = freezed,
  }) {
    return _then(_Stat(
      userEmail: userEmail == freezed
          ? _value.userEmail
          : userEmail // ignore: cast_nullable_to_non_nullable
              as String,
      genreId: genreId == freezed
          ? _value.genreId
          : genreId // ignore: cast_nullable_to_non_nullable
              as int,
      genre: genre == freezed
          ? _value.genre
          : genre // ignore: cast_nullable_to_non_nullable
              as GenreInfo,
      amount: amount == freezed
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Stat implements _Stat {
  const _$_Stat(
      {required this.userEmail,
      required this.genreId,
      required this.genre,
      required this.amount});

  factory _$_Stat.fromJson(Map<String, dynamic> json) => _$$_StatFromJson(json);

  @override
  final String userEmail;
  @override
  final int genreId;
  @override
  final GenreInfo genre;
  @override
  final int amount;

  @override
  String toString() {
    return 'Stat(userEmail: $userEmail, genreId: $genreId, genre: $genre, amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Stat &&
            (identical(other.userEmail, userEmail) ||
                const DeepCollectionEquality()
                    .equals(other.userEmail, userEmail)) &&
            (identical(other.genreId, genreId) ||
                const DeepCollectionEquality()
                    .equals(other.genreId, genreId)) &&
            (identical(other.genre, genre) ||
                const DeepCollectionEquality().equals(other.genre, genre)) &&
            (identical(other.amount, amount) ||
                const DeepCollectionEquality().equals(other.amount, amount)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(userEmail) ^
      const DeepCollectionEquality().hash(genreId) ^
      const DeepCollectionEquality().hash(genre) ^
      const DeepCollectionEquality().hash(amount);

  @JsonKey(ignore: true)
  @override
  _$StatCopyWith<_Stat> get copyWith =>
      __$StatCopyWithImpl<_Stat>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_StatToJson(this);
  }
}

abstract class _Stat implements Stat {
  const factory _Stat(
      {required String userEmail,
      required int genreId,
      required GenreInfo genre,
      required int amount}) = _$_Stat;

  factory _Stat.fromJson(Map<String, dynamic> json) = _$_Stat.fromJson;

  @override
  String get userEmail => throw _privateConstructorUsedError;
  @override
  int get genreId => throw _privateConstructorUsedError;
  @override
  GenreInfo get genre => throw _privateConstructorUsedError;
  @override
  int get amount => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$StatCopyWith<_Stat> get copyWith => throw _privateConstructorUsedError;
}

GenreInfo _$GenreInfoFromJson(Map<String, dynamic> json) {
  return _GenreInfo.fromJson(json);
}

/// @nodoc
class _$GenreInfoTearOff {
  const _$GenreInfoTearOff();

  _GenreInfo call({required int id, required String name}) {
    return _GenreInfo(
      id: id,
      name: name,
    );
  }

  GenreInfo fromJson(Map<String, Object> json) {
    return GenreInfo.fromJson(json);
  }
}

/// @nodoc
const $GenreInfo = _$GenreInfoTearOff();

/// @nodoc
mixin _$GenreInfo {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GenreInfoCopyWith<GenreInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GenreInfoCopyWith<$Res> {
  factory $GenreInfoCopyWith(GenreInfo value, $Res Function(GenreInfo) then) =
      _$GenreInfoCopyWithImpl<$Res>;
  $Res call({int id, String name});
}

/// @nodoc
class _$GenreInfoCopyWithImpl<$Res> implements $GenreInfoCopyWith<$Res> {
  _$GenreInfoCopyWithImpl(this._value, this._then);

  final GenreInfo _value;
  // ignore: unused_field
  final $Res Function(GenreInfo) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$GenreInfoCopyWith<$Res> implements $GenreInfoCopyWith<$Res> {
  factory _$GenreInfoCopyWith(
          _GenreInfo value, $Res Function(_GenreInfo) then) =
      __$GenreInfoCopyWithImpl<$Res>;
  @override
  $Res call({int id, String name});
}

/// @nodoc
class __$GenreInfoCopyWithImpl<$Res> extends _$GenreInfoCopyWithImpl<$Res>
    implements _$GenreInfoCopyWith<$Res> {
  __$GenreInfoCopyWithImpl(_GenreInfo _value, $Res Function(_GenreInfo) _then)
      : super(_value, (v) => _then(v as _GenreInfo));

  @override
  _GenreInfo get _value => super._value as _GenreInfo;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
  }) {
    return _then(_GenreInfo(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_GenreInfo implements _GenreInfo {
  const _$_GenreInfo({required this.id, required this.name});

  factory _$_GenreInfo.fromJson(Map<String, dynamic> json) =>
      _$$_GenreInfoFromJson(json);

  @override
  final int id;
  @override
  final String name;

  @override
  String toString() {
    return 'GenreInfo(id: $id, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _GenreInfo &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$GenreInfoCopyWith<_GenreInfo> get copyWith =>
      __$GenreInfoCopyWithImpl<_GenreInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GenreInfoToJson(this);
  }
}

abstract class _GenreInfo implements GenreInfo {
  const factory _GenreInfo({required int id, required String name}) =
      _$_GenreInfo;

  factory _GenreInfo.fromJson(Map<String, dynamic> json) =
      _$_GenreInfo.fromJson;

  @override
  int get id => throw _privateConstructorUsedError;
  @override
  String get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$GenreInfoCopyWith<_GenreInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
