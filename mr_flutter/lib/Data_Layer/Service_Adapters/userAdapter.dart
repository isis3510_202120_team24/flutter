import 'dart:convert';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';

class UserAdapter {
  FirebaseFunctions functions = FirebaseFunctions.instance;
  List<User> user = [];

  Future<User> createUser(String email, String name, String birthday) async {
    HttpsCallable callable = functions.httpsCallable('createUser');
    HttpsCallableResult result = await callable.call({
      'email': email,
      'name': name,
      'birthday': birthday,
      'avgPassBeforeLike': 0.0
    });

    print(result);
    if (result.data == null) {
      throw "The user can't be created";
    }
    return createUserFromDB(result.data);
  }

  Future<User> getUser(String email) async {
    HttpsCallable callable = functions.httpsCallable('getUser');
    HttpsCallableResult result = await callable.call(email);
    print(result);
    // TODO
    if (result.data == null) {
      throw "The user can't be created";
    }
    return createUserFromDB(result.data);
  }

  Future<User> updateUser(Map<dynamic, dynamic> ujson) async {
    HttpsCallable callable = functions.httpsCallable('updateUser');
    HttpsCallableResult result = await callable.call(ujson);
    print(result);
    // TODO
    if (result.data == null) {
      throw "The user can't be created";
    }
    return createUserFromDB(result.data);
  }

  Future<User> deleteUser(Map<dynamic, dynamic> ujson) async {
    HttpsCallable callable = functions.httpsCallable('getUser');
    HttpsCallableResult result = await callable.call(ujson);
    print(result);
    // TODO
    if (result.data == null) {
      throw "The user can't be created";
    }
    return createUserFromDB(result.data);
  }

/* 
  Future<void> getUserWithLikes() async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getUserWithLikes');
    final results =
        await callable.call({"email": "tr.gasparoni@uniandes.edu.co"});
    List res = results.data;
    //user.add(res);
    print(res);
    //return user; // ["Apple", "Banana", "Cherry", "Date", "Fig", "Grapes"]
  }

  Future<User> getUserDummyLikes() async {
    List<User> users = [];
    //HttpsCallable callable = FirebaseFunctions.instance.httpsCallable('getUser');
    final results =
        await _userRepository.fetchUser("tr.gasparoni@uniandes.edu.co");
    //await callable.call({"userEmail": "tr.gasparoni@uniandes.edu.co"});
    final user = results;
    //user.add(res);
    log(json.encode(user));
    User temp = userFromJson(json.encode(user));
    users.add(temp);
    print(users[0]);
    return users[0];
    //return user; // ["Apple", "Banana", "Cherry", "Date", "Fig", "Grapes"]
  } */

  User createUserFromDB(dynamic data) {
    String a = jsonEncode(data);
    Map<String, dynamic> b = jsonDecode(a);
    User user = User.fromJson(b);
    return user;
  }
}
