import 'dart:async';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'dart:developer';

class MovieServiceAdapter {
  // Implementacion con api Sebas
  FirebaseFunctions f = FirebaseFunctions.instance;
  //FakeUserRepository fr = FakeUserRepository(userAdapter: null);

  //List<Movie> movies = [];

  Future<dynamic> getUpcoming() async {
    var upcoming = <Map>[];
    var baseUrl = 'https://www.themoviedb.org/t/p/w1280';
    log('Entro a upcoming');
    var request = http.Request(
        'GET',
        Uri.parse(
            'https://api.themoviedb.org/3/movie/upcoming?api_key=f9312051edb733bda71c27e87c02b892'));

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var r = await response.stream.bytesToString();
      var rr = json.decode(r);
      //Map<String, dynamic> temp = {};
      String title;
      String releaseDate;
      String posterComplete;
      bool m18;
      double rating;

      for (var i = 0; i < rr['results'].length; i++) {
        Map<String, dynamic> temp = {};
        title = rr['results'][i]['title'];
        releaseDate = rr['results'][i]['release_date'];
        posterComplete = baseUrl + rr['results'][i]['poster_path'];
        m18 = rr['results'][i]['adult'];
        rating = rr['results'][i]['vote_average'].toDouble();
        temp['title'] = title;
        temp['releaseDate'] = releaseDate;
        temp['posterComplete'] = posterComplete;
        temp['m18'] = m18;
        temp['rating'] = rating;
        log(i.toString());
        log(json.encode(temp));
        upcoming.add(temp);
      }
    } else {
      print(response.reasonPhrase);
    }
    return upcoming;
  }

  Future<List<Movie>> getMovies() async {
    List<Movie> movies = [];
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getMovies');
    final results = await callable.call({});
    final listMovies = results.data;

    for (var movie in listMovies) {
      Movie temp = movieFromJson(json.encode(movie));
      movies.add(temp);
    }

    return movies;
  }

  Future<List<Movie>> getTrends() async {
    List<Movie> movies = [];
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getTrends');
    final results = await callable.call(20);
    final listMovies = results.data;

    for (var movie in listMovies) {
      Movie temp = movieFromJson(json.encode(movie));
      movies.add(temp);
    }
    return movies;
  }

  Future<List<Movie>> getRecommendedMock(
      String email, List<int> movieIds, String countryCode) async {
    List<Movie> movies = [];
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getRecordMock');
    final results = await callable.call({
      'userEmail': email,
      'moviesIds': movieIds,
      'countryId': countryCode,
    });
    final listMovies = results.data;
    log(json.encode(listMovies[0]));
    for (var movie in listMovies) {
      Movie temp = movieFromJson(json.encode(movie));
      movies.add(temp);
    }
    //print(movies[0]);
    return movies;
  }

  Future<List<Movie>> getRecommendations(
      String email, List<int> movieIds, String countryCode) async {
    List<Movie> movies = [];
    print('Entro get');
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getRecommendations');
    final results = await callable.call(
        {'userEmail': email, 'moviesIds': movieIds, 'countryId': countryCode});
    final listMovies = results.data;
    for (var movie in listMovies) {
      log(json.encode(movie));
      Movie temp = movieFromJson(json.encode(movie));
      movies.add(temp);
    }
    //print(movies.length);
    log(json.encode(movies[0]));
    return movies;
  }

  Future<void> likeRecommendations(
      String currentEmail, int movieId, int actual) async {
    List<Movie> movies = [];
    print('Entro get');
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('likeRecomendation');
    final results = await callable.call({
      'userEmail': currentEmail,
      'movieId': movieId,
      'passNum': actual,
    });
    //final listMovies = results.data;
  }

  Future<String> getMovieGenre(String title) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getMovie');
    final results = await callable.call({'title': title});
    final movie = results.data;
    var name = movie['genres'][0]['name'];
    return name;
  }
}
