import 'dart:convert';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:mr_flutter/Data_Layer/models/listU.dart';

class ListAdapter {
  FirebaseFunctions f = FirebaseFunctions.instance;

  Future<ListU> getList(
      {required String userEmail, required String name}) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('getLists');
    final results = await callable.call({'userEmail': userEmail, 'name': name});

    var listMovies = jsonEncode(results.data);
    if (listMovies == null) {
      return ListU();
    }

    return ListU.fromJson(jsonDecode(listMovies));
  }

  Future<void> createList(
      {required String name, required String userEmail}) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('createList');
    await callable.call({
      'userEmail': userEmail,
      'name': name,
    });
  }

  Future<void> addMovieToList(
      {required String name,
      required String userEmail,
      required int movieId}) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('addMovieToList');
    await callable.call({
      'userEmail': userEmail,
      'name': name,
      'movieId': movieId,
    });
  }

  void updateList(
      {required String name,
      required String userEmail,
      required ListU listU}) async {
    HttpsCallable callable =
        FirebaseFunctions.instance.httpsCallable('updateList');
    Map<String, dynamic> jsonListU = listU.toJson();
    jsonListU['userEmail'] = userEmail;
    // await callable.call(jsonListU);
  }
}
