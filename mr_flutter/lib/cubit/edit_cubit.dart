import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
part 'edit_state.dart';
part 'edit_cubit.freezed.dart';

class EditCubit extends Cubit<EditState> {
  final FakeUserRepository _fakeUserRepository;

  EditCubit(this._fakeUserRepository) : super(EditState.initial());
}
