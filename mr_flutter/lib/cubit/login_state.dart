part of 'login_cubit.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState.loadingLogin() = _LoadingLogin;
  const factory LoginState.failedLogin(String message) = _FailedLogin;

  const factory LoginState.connected({
    required bool isConnected,
    required bool isLoading,
  }) = _Connected;
  const factory LoginState.notificationFirebase(Map<String, dynamic>? message) =
      _NotificationFirebase;
}
