part of 'upcoming_cubit.dart';

@immutable
abstract class UpcomingState {
  const UpcomingState();
}

class Loading extends UpcomingState {
  const Loading();
}

class Loaded extends UpcomingState {
  final upcomingMovie;
  const Loaded(this.upcomingMovie);
}

class NoConnection extends UpcomingState {
  const NoConnection();
}

class Connected extends UpcomingState {
  const Connected();
}




/*
@freezed
class HomeScreenState with _$HomeScreenState {
  const factory HomeScreenState.initial() = _Initial;
  const factory HomeScreenState.loading() = _Loading;
  const factory HomeScreenState.loaded(
      User user, List<TrendMovie> trs, List<News> news) = _Loaded;
  const factory HomeScreenState.error(String message) = _Error;
  const factory HomeScreenState.notificationFirebase(
      Map<String, String>? message) = _NotificationFirebase;
}
*/