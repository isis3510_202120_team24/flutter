// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'stuff_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$StuffStateTearOff {
  const _$StuffStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _FailedSignOut failedSignOut(String message) {
    return _FailedSignOut(
      message,
    );
  }
}

/// @nodoc
const $StuffState = _$StuffStateTearOff();

/// @nodoc
mixin _$StuffState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String message) failedSignOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String message)? failedSignOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String message)? failedSignOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FailedSignOut value) failedSignOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FailedSignOut value)? failedSignOut,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FailedSignOut value)? failedSignOut,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StuffStateCopyWith<$Res> {
  factory $StuffStateCopyWith(
          StuffState value, $Res Function(StuffState) then) =
      _$StuffStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$StuffStateCopyWithImpl<$Res> implements $StuffStateCopyWith<$Res> {
  _$StuffStateCopyWithImpl(this._value, this._then);

  final StuffState _value;
  // ignore: unused_field
  final $Res Function(StuffState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$StuffStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'StuffState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String message) failedSignOut,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String message)? failedSignOut,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String message)? failedSignOut,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FailedSignOut value) failedSignOut,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FailedSignOut value)? failedSignOut,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FailedSignOut value)? failedSignOut,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements StuffState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$FailedSignOutCopyWith<$Res> {
  factory _$FailedSignOutCopyWith(
          _FailedSignOut value, $Res Function(_FailedSignOut) then) =
      __$FailedSignOutCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$FailedSignOutCopyWithImpl<$Res> extends _$StuffStateCopyWithImpl<$Res>
    implements _$FailedSignOutCopyWith<$Res> {
  __$FailedSignOutCopyWithImpl(
      _FailedSignOut _value, $Res Function(_FailedSignOut) _then)
      : super(_value, (v) => _then(v as _FailedSignOut));

  @override
  _FailedSignOut get _value => super._value as _FailedSignOut;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_FailedSignOut(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FailedSignOut implements _FailedSignOut {
  const _$_FailedSignOut(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'StuffState.failedSignOut(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FailedSignOut &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$FailedSignOutCopyWith<_FailedSignOut> get copyWith =>
      __$FailedSignOutCopyWithImpl<_FailedSignOut>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String message) failedSignOut,
  }) {
    return failedSignOut(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String message)? failedSignOut,
  }) {
    return failedSignOut?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String message)? failedSignOut,
    required TResult orElse(),
  }) {
    if (failedSignOut != null) {
      return failedSignOut(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_FailedSignOut value) failedSignOut,
  }) {
    return failedSignOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FailedSignOut value)? failedSignOut,
  }) {
    return failedSignOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_FailedSignOut value)? failedSignOut,
    required TResult orElse(),
  }) {
    if (failedSignOut != null) {
      return failedSignOut(this);
    }
    return orElse();
  }
}

abstract class _FailedSignOut implements StuffState {
  const factory _FailedSignOut(String message) = _$_FailedSignOut;

  String get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FailedSignOutCopyWith<_FailedSignOut> get copyWith =>
      throw _privateConstructorUsedError;
}
