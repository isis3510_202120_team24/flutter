import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/user.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';

part 'register_state.dart';
part 'register_cubit.freezed.dart';

class RegisterCubit extends Cubit<RegisterState> {
  final FakeUserRepository _fakeUserRepository;
  final ConnectionRepo _connectionRepository;
  final MessageRepository _messageRepository;

  var _internetStream;
  bool _isLoading = false;
  var _notificationStream;

  RegisterCubit(this._fakeUserRepository, this._connectionRepository,
      this._internetStream, this._messageRepository)
      : super(RegisterState.connected(true, false));

  Future<void> messageFirebase() async {
    if (_notificationStream != null) {
      await _notificationStream.cancel();
    }
    _notificationStream =
        _messageRepository.streamNotification().listen((event) {
      emit(RegisterState.notificationFirebase(event));
    });
  }

  Future<void> listenToConnectivity() async {
    if (_internetStream != null) {
      await _internetStream.cancel();
    }
    emit(RegisterState.connected(
        await _connectionRepository.isConnectedToInternet(), _isLoading));
    _internetStream = _connectionRepository.streamConnectedToInternet().listen(
      (bool isConnected) {
        if (isConnected) {
          if (_isLoading) {
            emit(const RegisterState.connected(true, true));
          } else {
            emit(const RegisterState.connected(true, false));
          }
        } else {
          emit(const RegisterState.connected(false, false));
        }
      },
    );
  }

  Future<void> register(
      String email, String password, String name, String birthday) async {
    try {
      _isLoading = true;
      emit(RegisterState.loadingRegister());
      final user = await _fakeUserRepository.registerWithEmailAndPassword(
          email, password, name, birthday);
    } catch (e) {
      _isLoading = false;
      emit(RegisterState.failedRegister(e.toString()));
    }
  }

  @override
  Future<void> close() {
    // TODO: change when internetStream is activate again
    _internetStream.cancel();
    return super.close();
  }
}
