part of 'edit_cubit.dart';

@freezed
class EditState with _$EditState {
  const factory EditState.initial() = _Initial;
  const factory EditState.sendEdit() = _SendEdit;
}
