part of 'moviedetail_cubit.dart';

@immutable
abstract class MovieDetailState {
  const MovieDetailState();
}

class MovieDetailLoading extends MovieDetailState {
  const MovieDetailLoading();
}

class MovieDetailLoaded extends MovieDetailState {
  final User user;
  final List<RecomendedMovie> rm;
  const MovieDetailLoaded(this.user, this.rm);
}

class MovieDetailLiked extends MovieDetailState {
  final int index;
  const MovieDetailLiked(this.index);
}

class MovieDetailNoConnection extends MovieDetailState {
  const MovieDetailNoConnection();
}

class MovieDetailConected extends MovieDetailState {
  const MovieDetailConected();
}
