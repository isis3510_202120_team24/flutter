part of 'profile_cubit.dart';

@immutable
abstract class ProfileState {
  const ProfileState();
}

class ProfileLoading extends ProfileState {
  const ProfileLoading();
}

class ProfileLoaded extends ProfileState {
  final User user;
  final Map<String, double> pie;
  final bool connected;
  const ProfileLoaded(this.user, this.pie, this.connected);
}

class ProfileNotConnected extends ProfileState {
  final User user;
  final Map<String, double> pie;
  final bool connected;
  const ProfileNotConnected(this.user, this.pie, this.connected);
}

class ProfilePie extends ProfileState {
  final Map<String, double> pie;
  final User user;
  const ProfilePie(this.user, this.pie);
}
