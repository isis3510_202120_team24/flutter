import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/user.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';

part 'movie_trend_detail_state.dart';
part 'movie_trend_detail_cubit.freezed.dart';

class MovieTrendDetailCubit extends Cubit<MovieTrendDetailState> {
  final ListRepository _listRepository;
  final FakeUserRepository _userRepository;

  MovieTrendDetailCubit(this._userRepository, this._listRepository)
      : super(MovieTrendDetailState.initial());

  Future<bool> addMovieToList({required int movieId}) async {
    User user = await _userRepository.currentUser;

    try {
      await _listRepository.addMovieToList(
          userEmail: user.email, movieId: movieId);
      return true;
    } on Exception catch (e) {
      print(e);
      return false;
    }
  }
}
