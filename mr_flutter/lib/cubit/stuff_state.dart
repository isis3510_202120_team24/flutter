part of 'stuff_cubit.dart';

@freezed
class StuffState with _$StuffState {
  const factory StuffState.initial() = _Initial;
  const factory StuffState.failedSignOut(String message) = _FailedSignOut;
}
