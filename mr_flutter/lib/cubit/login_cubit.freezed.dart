// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'login_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$LoginStateTearOff {
  const _$LoginStateTearOff();

  _LoadingLogin loadingLogin() {
    return const _LoadingLogin();
  }

  _FailedLogin failedLogin(String message) {
    return _FailedLogin(
      message,
    );
  }

  _Connected connected({required bool isConnected, required bool isLoading}) {
    return _Connected(
      isConnected: isConnected,
      isLoading: isLoading,
    );
  }

  _NotificationFirebase notificationFirebase(Map<String, dynamic>? message) {
    return _NotificationFirebase(
      message,
    );
  }
}

/// @nodoc
const $LoginState = _$LoginStateTearOff();

/// @nodoc
mixin _$LoginState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingLogin,
    required TResult Function(String message) failedLogin,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingLogin value) loadingLogin,
    required TResult Function(_FailedLogin value) failedLogin,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginStateCopyWith<$Res> {
  factory $LoginStateCopyWith(
          LoginState value, $Res Function(LoginState) then) =
      _$LoginStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$LoginStateCopyWithImpl<$Res> implements $LoginStateCopyWith<$Res> {
  _$LoginStateCopyWithImpl(this._value, this._then);

  final LoginState _value;
  // ignore: unused_field
  final $Res Function(LoginState) _then;
}

/// @nodoc
abstract class _$LoadingLoginCopyWith<$Res> {
  factory _$LoadingLoginCopyWith(
          _LoadingLogin value, $Res Function(_LoadingLogin) then) =
      __$LoadingLoginCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingLoginCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements _$LoadingLoginCopyWith<$Res> {
  __$LoadingLoginCopyWithImpl(
      _LoadingLogin _value, $Res Function(_LoadingLogin) _then)
      : super(_value, (v) => _then(v as _LoadingLogin));

  @override
  _LoadingLogin get _value => super._value as _LoadingLogin;
}

/// @nodoc

class _$_LoadingLogin implements _LoadingLogin {
  const _$_LoadingLogin();

  @override
  String toString() {
    return 'LoginState.loadingLogin()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadingLogin);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingLogin,
    required TResult Function(String message) failedLogin,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return loadingLogin();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return loadingLogin?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (loadingLogin != null) {
      return loadingLogin();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingLogin value) loadingLogin,
    required TResult Function(_FailedLogin value) failedLogin,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return loadingLogin(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return loadingLogin?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (loadingLogin != null) {
      return loadingLogin(this);
    }
    return orElse();
  }
}

abstract class _LoadingLogin implements LoginState {
  const factory _LoadingLogin() = _$_LoadingLogin;
}

/// @nodoc
abstract class _$FailedLoginCopyWith<$Res> {
  factory _$FailedLoginCopyWith(
          _FailedLogin value, $Res Function(_FailedLogin) then) =
      __$FailedLoginCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$FailedLoginCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements _$FailedLoginCopyWith<$Res> {
  __$FailedLoginCopyWithImpl(
      _FailedLogin _value, $Res Function(_FailedLogin) _then)
      : super(_value, (v) => _then(v as _FailedLogin));

  @override
  _FailedLogin get _value => super._value as _FailedLogin;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_FailedLogin(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FailedLogin implements _FailedLogin {
  const _$_FailedLogin(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'LoginState.failedLogin(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FailedLogin &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$FailedLoginCopyWith<_FailedLogin> get copyWith =>
      __$FailedLoginCopyWithImpl<_FailedLogin>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingLogin,
    required TResult Function(String message) failedLogin,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return failedLogin(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return failedLogin?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (failedLogin != null) {
      return failedLogin(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingLogin value) loadingLogin,
    required TResult Function(_FailedLogin value) failedLogin,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return failedLogin(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return failedLogin?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (failedLogin != null) {
      return failedLogin(this);
    }
    return orElse();
  }
}

abstract class _FailedLogin implements LoginState {
  const factory _FailedLogin(String message) = _$_FailedLogin;

  String get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FailedLoginCopyWith<_FailedLogin> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ConnectedCopyWith<$Res> {
  factory _$ConnectedCopyWith(
          _Connected value, $Res Function(_Connected) then) =
      __$ConnectedCopyWithImpl<$Res>;
  $Res call({bool isConnected, bool isLoading});
}

/// @nodoc
class __$ConnectedCopyWithImpl<$Res> extends _$LoginStateCopyWithImpl<$Res>
    implements _$ConnectedCopyWith<$Res> {
  __$ConnectedCopyWithImpl(_Connected _value, $Res Function(_Connected) _then)
      : super(_value, (v) => _then(v as _Connected));

  @override
  _Connected get _value => super._value as _Connected;

  @override
  $Res call({
    Object? isConnected = freezed,
    Object? isLoading = freezed,
  }) {
    return _then(_Connected(
      isConnected: isConnected == freezed
          ? _value.isConnected
          : isConnected // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_Connected implements _Connected {
  const _$_Connected({required this.isConnected, required this.isLoading});

  @override
  final bool isConnected;
  @override
  final bool isLoading;

  @override
  String toString() {
    return 'LoginState.connected(isConnected: $isConnected, isLoading: $isLoading)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Connected &&
            (identical(other.isConnected, isConnected) ||
                const DeepCollectionEquality()
                    .equals(other.isConnected, isConnected)) &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isConnected) ^
      const DeepCollectionEquality().hash(isLoading);

  @JsonKey(ignore: true)
  @override
  _$ConnectedCopyWith<_Connected> get copyWith =>
      __$ConnectedCopyWithImpl<_Connected>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingLogin,
    required TResult Function(String message) failedLogin,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return connected(isConnected, isLoading);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return connected?.call(isConnected, isLoading);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (connected != null) {
      return connected(isConnected, isLoading);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingLogin value) loadingLogin,
    required TResult Function(_FailedLogin value) failedLogin,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return connected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return connected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (connected != null) {
      return connected(this);
    }
    return orElse();
  }
}

abstract class _Connected implements LoginState {
  const factory _Connected(
      {required bool isConnected, required bool isLoading}) = _$_Connected;

  bool get isConnected => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ConnectedCopyWith<_Connected> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NotificationFirebaseCopyWith<$Res> {
  factory _$NotificationFirebaseCopyWith(_NotificationFirebase value,
          $Res Function(_NotificationFirebase) then) =
      __$NotificationFirebaseCopyWithImpl<$Res>;
  $Res call({Map<String, dynamic>? message});
}

/// @nodoc
class __$NotificationFirebaseCopyWithImpl<$Res>
    extends _$LoginStateCopyWithImpl<$Res>
    implements _$NotificationFirebaseCopyWith<$Res> {
  __$NotificationFirebaseCopyWithImpl(
      _NotificationFirebase _value, $Res Function(_NotificationFirebase) _then)
      : super(_value, (v) => _then(v as _NotificationFirebase));

  @override
  _NotificationFirebase get _value => super._value as _NotificationFirebase;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_NotificationFirebase(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>?,
    ));
  }
}

/// @nodoc

class _$_NotificationFirebase implements _NotificationFirebase {
  const _$_NotificationFirebase(this.message);

  @override
  final Map<String, dynamic>? message;

  @override
  String toString() {
    return 'LoginState.notificationFirebase(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NotificationFirebase &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$NotificationFirebaseCopyWith<_NotificationFirebase> get copyWith =>
      __$NotificationFirebaseCopyWithImpl<_NotificationFirebase>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingLogin,
    required TResult Function(String message) failedLogin,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return notificationFirebase(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return notificationFirebase?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingLogin,
    TResult Function(String message)? failedLogin,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (notificationFirebase != null) {
      return notificationFirebase(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingLogin value) loadingLogin,
    required TResult Function(_FailedLogin value) failedLogin,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return notificationFirebase(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return notificationFirebase?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingLogin value)? loadingLogin,
    TResult Function(_FailedLogin value)? failedLogin,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (notificationFirebase != null) {
      return notificationFirebase(this);
    }
    return orElse();
  }
}

abstract class _NotificationFirebase implements LoginState {
  const factory _NotificationFirebase(Map<String, dynamic>? message) =
      _$_NotificationFirebase;

  Map<String, dynamic>? get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$NotificationFirebaseCopyWith<_NotificationFirebase> get copyWith =>
      throw _privateConstructorUsedError;
}
