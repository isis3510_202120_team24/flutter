part of 'home_screen_cubit.dart';

@immutable
abstract class HomeScreenState {
  const HomeScreenState();
}

class Loading extends HomeScreenState {
  const Loading();
}

class Loaded extends HomeScreenState {
  final User user;
  final List<TrendMovie> tms;
  const Loaded(this.user, this.tms);
}

class NoConnection extends HomeScreenState {
  final User user;
  final List<TrendMovie> tms;
  const NoConnection(this.user, this.tms);
}

class Connected extends HomeScreenState {
  const Connected();
}

class NotificationFirebase extends HomeScreenState {
  final Map<String, dynamic>? message;
  final User user;
  final List<TrendMovie> tms;
  const NotificationFirebase(this.user, this.tms, this.message);
}




/*
@freezed
class HomeScreenState with _$HomeScreenState {
  const factory HomeScreenState.initial() = _Initial;
  const factory HomeScreenState.loading() = _Loading;
  const factory HomeScreenState.loaded(
      User user, List<TrendMovie> trs, List<News> news) = _Loaded;
  const factory HomeScreenState.error(String message) = _Error;
  const factory HomeScreenState.notificationFirebase(
      Map<String, String>? message) = _NotificationFirebase;
}
*/