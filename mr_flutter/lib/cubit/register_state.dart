part of 'register_cubit.dart';

@freezed
class RegisterState with _$RegisterState {
  const factory RegisterState.loadingRegister() = _LoadingRegister;

  const factory RegisterState.failedRegister(String message) = _FailedRegister;
  const factory RegisterState.connected(bool isConnected, bool isLoading) =
      _Connected;
  const factory RegisterState.notificationFirebase(
      Map<String, dynamic>? message) = _NotificationFirebase;
}
