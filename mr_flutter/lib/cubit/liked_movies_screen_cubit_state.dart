part of 'liked_movies_screen_cubit_cubit.dart';

@freezed
class LikedMoviesScreenCubitState with _$LikedMoviesScreenCubitState {
  const factory LikedMoviesScreenCubitState.loading() = _Loading;
  const factory LikedMoviesScreenCubitState.loaded(ListU list) = _Loaded;
  const factory LikedMoviesScreenCubitState.deleted() = _Deleted;
}
