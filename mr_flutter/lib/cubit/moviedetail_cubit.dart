import 'dart:isolate';
import 'package:flutter/foundation.dart';
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/userAdapter.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:mr_flutter/database_helper.dart';
import 'dart:math';
import 'dart:developer' as dv;
import 'package:http/http.dart' as http;
import 'dart:convert';
part 'moviedetail_state.dart';

class MoviedetailCubit extends Cubit<MovieDetailState> {
  final MovieRepository _movieRepository;
  final FakeUserRepository _fakeUserRepository;
  final ConnectionRepo _connectionRepository;

  MoviedetailCubit(this._movieRepository, this._fakeUserRepository,
      this._connectionRepository)
      : super(MovieDetailLoading());

  Future<String> getCountry() async {
    http.Response countryData =
        await http.get(Uri.parse('http://ip-api.com/json'));
    Map data = jsonDecode(countryData.body);
    String countryCode = data['countryCode'];
    return countryCode;
  }

  Future<void> checkConnectivity() async {
    bool connectivity = await _connectionRepository.isConnectedToInternet();
    if (connectivity) {
      emit(MovieDetailConected());
    } else {
      emit(MovieDetailNoConnection());
    }
  }

  Future<void> getRecomendedMovies(List<int> likedMoviesIds) async {
    String countryCode = await getCountry();
    User user = await _fakeUserRepository.currentUser;
    var email = user.email;
    List<RecomendedMovie> listRM = await _movieRepository.getRecommended(
        email, likedMoviesIds, countryCode);
    await DatabaseHelper.instance.addRecoMovies(listRM);
  }

  Future<void> getRecomendedMovieInitial() async {
    String countryCode = await getCountry();
    User user = await _fakeUserRepository.currentUser;
    var email = user.email;
    var ran = new Random();
    List<int> ranMovieIds = [];
    for (var i = 0; i < 5; i++) {
      var ranNum = ran.nextInt(9000);
      ranMovieIds.add(ranNum);
    }
    dv.log(ranMovieIds.length.toString());

    List<RecomendedMovie> listRM =
        await _movieRepository.getRecommended(email, ranMovieIds, countryCode);

    await DatabaseHelper.instance.addRecoMovies(listRM);
    List<RecomendedMovie> rm = await readMovies(6);
    emit(MovieDetailLoaded(user, rm));
  }

  Future<List<RecomendedMovie>> readMovies(int amountMovies) async {
    List<RecomendedMovie> listRM =
        await DatabaseHelper.instance.getRecoMovies(amountMovies);
    return listRM;
  }

  likeRecomendation(String currentEmail, int movieId, int actual) async {
    await _movieRepository.likeRecommendations(currentEmail, movieId, actual);
  }

/*
  Future<List<RecomendedMovie>> getRecomendedMovieNext() async {
    List<RecomendedMovie> listRM = await _movieRepository.getRecommended();
    return listRM;
  }
  */
  have2update(int actual) {}

  updateMovieDb(List<int> likedMovieIds) async {
    final rm =
        await compute<List<int>, void>(getRecomendedMovies, likedMovieIds);
  }

  int pressLike(int a) {
    //addLike([1,2,34])

    a++;
    return a;
  }

  bool validateMoreNeeded(int a) {
    var ans = false;
    if (a == 3) {
      ans = true;
    }
    return ans;
  }

  int pressDislike(int a) {
    // addDisLike()
    print('Entro a press Like');
    a++;
    return a;
  }
}
