// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'register_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$RegisterStateTearOff {
  const _$RegisterStateTearOff();

  _LoadingRegister loadingRegister() {
    return const _LoadingRegister();
  }

  _FailedRegister failedRegister(String message) {
    return _FailedRegister(
      message,
    );
  }

  _Connected connected(bool isConnected, bool isLoading) {
    return _Connected(
      isConnected,
      isLoading,
    );
  }

  _NotificationFirebase notificationFirebase(Map<String, dynamic>? message) {
    return _NotificationFirebase(
      message,
    );
  }
}

/// @nodoc
const $RegisterState = _$RegisterStateTearOff();

/// @nodoc
mixin _$RegisterState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingRegister,
    required TResult Function(String message) failedRegister,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingRegister value) loadingRegister,
    required TResult Function(_FailedRegister value) failedRegister,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterStateCopyWith<$Res> {
  factory $RegisterStateCopyWith(
          RegisterState value, $Res Function(RegisterState) then) =
      _$RegisterStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$RegisterStateCopyWithImpl<$Res>
    implements $RegisterStateCopyWith<$Res> {
  _$RegisterStateCopyWithImpl(this._value, this._then);

  final RegisterState _value;
  // ignore: unused_field
  final $Res Function(RegisterState) _then;
}

/// @nodoc
abstract class _$LoadingRegisterCopyWith<$Res> {
  factory _$LoadingRegisterCopyWith(
          _LoadingRegister value, $Res Function(_LoadingRegister) then) =
      __$LoadingRegisterCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadingRegisterCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res>
    implements _$LoadingRegisterCopyWith<$Res> {
  __$LoadingRegisterCopyWithImpl(
      _LoadingRegister _value, $Res Function(_LoadingRegister) _then)
      : super(_value, (v) => _then(v as _LoadingRegister));

  @override
  _LoadingRegister get _value => super._value as _LoadingRegister;
}

/// @nodoc

class _$_LoadingRegister implements _LoadingRegister {
  const _$_LoadingRegister();

  @override
  String toString() {
    return 'RegisterState.loadingRegister()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadingRegister);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingRegister,
    required TResult Function(String message) failedRegister,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return loadingRegister();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return loadingRegister?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (loadingRegister != null) {
      return loadingRegister();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingRegister value) loadingRegister,
    required TResult Function(_FailedRegister value) failedRegister,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return loadingRegister(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return loadingRegister?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (loadingRegister != null) {
      return loadingRegister(this);
    }
    return orElse();
  }
}

abstract class _LoadingRegister implements RegisterState {
  const factory _LoadingRegister() = _$_LoadingRegister;
}

/// @nodoc
abstract class _$FailedRegisterCopyWith<$Res> {
  factory _$FailedRegisterCopyWith(
          _FailedRegister value, $Res Function(_FailedRegister) then) =
      __$FailedRegisterCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$FailedRegisterCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res>
    implements _$FailedRegisterCopyWith<$Res> {
  __$FailedRegisterCopyWithImpl(
      _FailedRegister _value, $Res Function(_FailedRegister) _then)
      : super(_value, (v) => _then(v as _FailedRegister));

  @override
  _FailedRegister get _value => super._value as _FailedRegister;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_FailedRegister(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_FailedRegister implements _FailedRegister {
  const _$_FailedRegister(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'RegisterState.failedRegister(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _FailedRegister &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$FailedRegisterCopyWith<_FailedRegister> get copyWith =>
      __$FailedRegisterCopyWithImpl<_FailedRegister>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingRegister,
    required TResult Function(String message) failedRegister,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return failedRegister(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return failedRegister?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (failedRegister != null) {
      return failedRegister(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingRegister value) loadingRegister,
    required TResult Function(_FailedRegister value) failedRegister,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return failedRegister(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return failedRegister?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (failedRegister != null) {
      return failedRegister(this);
    }
    return orElse();
  }
}

abstract class _FailedRegister implements RegisterState {
  const factory _FailedRegister(String message) = _$_FailedRegister;

  String get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FailedRegisterCopyWith<_FailedRegister> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ConnectedCopyWith<$Res> {
  factory _$ConnectedCopyWith(
          _Connected value, $Res Function(_Connected) then) =
      __$ConnectedCopyWithImpl<$Res>;
  $Res call({bool isConnected, bool isLoading});
}

/// @nodoc
class __$ConnectedCopyWithImpl<$Res> extends _$RegisterStateCopyWithImpl<$Res>
    implements _$ConnectedCopyWith<$Res> {
  __$ConnectedCopyWithImpl(_Connected _value, $Res Function(_Connected) _then)
      : super(_value, (v) => _then(v as _Connected));

  @override
  _Connected get _value => super._value as _Connected;

  @override
  $Res call({
    Object? isConnected = freezed,
    Object? isLoading = freezed,
  }) {
    return _then(_Connected(
      isConnected == freezed
          ? _value.isConnected
          : isConnected // ignore: cast_nullable_to_non_nullable
              as bool,
      isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_Connected implements _Connected {
  const _$_Connected(this.isConnected, this.isLoading);

  @override
  final bool isConnected;
  @override
  final bool isLoading;

  @override
  String toString() {
    return 'RegisterState.connected(isConnected: $isConnected, isLoading: $isLoading)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Connected &&
            (identical(other.isConnected, isConnected) ||
                const DeepCollectionEquality()
                    .equals(other.isConnected, isConnected)) &&
            (identical(other.isLoading, isLoading) ||
                const DeepCollectionEquality()
                    .equals(other.isLoading, isLoading)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isConnected) ^
      const DeepCollectionEquality().hash(isLoading);

  @JsonKey(ignore: true)
  @override
  _$ConnectedCopyWith<_Connected> get copyWith =>
      __$ConnectedCopyWithImpl<_Connected>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingRegister,
    required TResult Function(String message) failedRegister,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return connected(isConnected, isLoading);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return connected?.call(isConnected, isLoading);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (connected != null) {
      return connected(isConnected, isLoading);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingRegister value) loadingRegister,
    required TResult Function(_FailedRegister value) failedRegister,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return connected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return connected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (connected != null) {
      return connected(this);
    }
    return orElse();
  }
}

abstract class _Connected implements RegisterState {
  const factory _Connected(bool isConnected, bool isLoading) = _$_Connected;

  bool get isConnected => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ConnectedCopyWith<_Connected> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NotificationFirebaseCopyWith<$Res> {
  factory _$NotificationFirebaseCopyWith(_NotificationFirebase value,
          $Res Function(_NotificationFirebase) then) =
      __$NotificationFirebaseCopyWithImpl<$Res>;
  $Res call({Map<String, dynamic>? message});
}

/// @nodoc
class __$NotificationFirebaseCopyWithImpl<$Res>
    extends _$RegisterStateCopyWithImpl<$Res>
    implements _$NotificationFirebaseCopyWith<$Res> {
  __$NotificationFirebaseCopyWithImpl(
      _NotificationFirebase _value, $Res Function(_NotificationFirebase) _then)
      : super(_value, (v) => _then(v as _NotificationFirebase));

  @override
  _NotificationFirebase get _value => super._value as _NotificationFirebase;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_NotificationFirebase(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>?,
    ));
  }
}

/// @nodoc

class _$_NotificationFirebase implements _NotificationFirebase {
  const _$_NotificationFirebase(this.message);

  @override
  final Map<String, dynamic>? message;

  @override
  String toString() {
    return 'RegisterState.notificationFirebase(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NotificationFirebase &&
            (identical(other.message, message) ||
                const DeepCollectionEquality().equals(other.message, message)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(message);

  @JsonKey(ignore: true)
  @override
  _$NotificationFirebaseCopyWith<_NotificationFirebase> get copyWith =>
      __$NotificationFirebaseCopyWithImpl<_NotificationFirebase>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loadingRegister,
    required TResult Function(String message) failedRegister,
    required TResult Function(bool isConnected, bool isLoading) connected,
    required TResult Function(Map<String, dynamic>? message)
        notificationFirebase,
  }) {
    return notificationFirebase(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
  }) {
    return notificationFirebase?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loadingRegister,
    TResult Function(String message)? failedRegister,
    TResult Function(bool isConnected, bool isLoading)? connected,
    TResult Function(Map<String, dynamic>? message)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (notificationFirebase != null) {
      return notificationFirebase(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_LoadingRegister value) loadingRegister,
    required TResult Function(_FailedRegister value) failedRegister,
    required TResult Function(_Connected value) connected,
    required TResult Function(_NotificationFirebase value) notificationFirebase,
  }) {
    return notificationFirebase(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
  }) {
    return notificationFirebase?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_LoadingRegister value)? loadingRegister,
    TResult Function(_FailedRegister value)? failedRegister,
    TResult Function(_Connected value)? connected,
    TResult Function(_NotificationFirebase value)? notificationFirebase,
    required TResult orElse(),
  }) {
    if (notificationFirebase != null) {
      return notificationFirebase(this);
    }
    return orElse();
  }
}

abstract class _NotificationFirebase implements RegisterState {
  const factory _NotificationFirebase(Map<String, dynamic>? message) =
      _$_NotificationFirebase;

  Map<String, dynamic>? get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$NotificationFirebaseCopyWith<_NotificationFirebase> get copyWith =>
      throw _privateConstructorUsedError;
}
