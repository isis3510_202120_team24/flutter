import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  final FakeUserRepository _fakeUserRepository;
  final ConnectionRepo _connectionRepository;
  var _internetStream;
  ProfileCubit(
    this._fakeUserRepository,
    this._connectionRepository,
    this._internetStream,
  ) : super(ProfileLoading());

  Future<void> getUser() async {
    User user = await _fakeUserRepository.currentUser;
    bool connected = await _connectionRepository.isConnectedToInternet();
    Map<String, double> pie = await _fakeUserRepository.pieData;
    print("this is the pie");
    print(pie);
    emit(ProfileLoaded(user, pie, connected));
  }

  Future<void> updateConection() async {
    User user = await _fakeUserRepository.currentUser;
    bool connected = await _connectionRepository.isConnectedToInternet();
    Map<String, double> pie = await _fakeUserRepository.pieData;
    print("this is the pie");
    print(pie);

    emit(ProfileLoaded(user, pie, connected));
  }

  Future<void> getPie() async {
    Map<String, double> pie = await _fakeUserRepository.updatePie();
    bool connected = await _connectionRepository.isConnectedToInternet();
    User user = await _fakeUserRepository.currentUser;
    emit(ProfileLoaded(user, pie, connected));
    print("this is the NEW pie");
    print(pie);
  }

  Future<void> deleteUser() async {
    User user = await _fakeUserRepository.deleteUser();
    Map<String, double> pie = await _fakeUserRepository.updatePie();
    bool connected = await _connectionRepository.isConnectedToInternet();
    emit(ProfileLoaded(user, pie, connected));
    print("this is the NEW pie");
    print(pie);
  }
}
