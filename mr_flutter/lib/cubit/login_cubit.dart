import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';

part 'login_state.dart';
part 'login_cubit.freezed.dart';

class LoginCubit extends Cubit<LoginState> {
  final FakeUserRepository _fakeUserRepository;
  final ConnectionRepo _connectionRepository;
  final MessageRepository _messageRepository;
  var _internetStream;
  var _notificationStream;
  bool _isLoading = false;

  LoginCubit(this._fakeUserRepository, this._connectionRepository,
      this._internetStream, this._messageRepository)
      : super(LoginState.connected(isConnected: true, isLoading: false));

  Future<void> messageFirebase() async {
    if (_notificationStream != null) {
      await _notificationStream.cancel();
    }
    _notificationStream =
        _messageRepository.streamNotification().listen((event) {
      emit(LoginState.notificationFirebase(event));
    });
  }

  // Future<void> initialMessage() async {
  //   Map<String, String?>? message =
  //       await _messageRepository.checkForInitialMessage();
  //   if (message != null) {
  //     emit(LoginState.notificationFirebase(message));
  //   }
  // }

  Future<void> login(String email, String password) async {
    try {
      _isLoading = true;
      emit(LoginState.loadingLogin());
      final user =
          await _fakeUserRepository.signWithEmailAndPassword(email, password);
    } catch (e) {
      emit(LoginState.failedLogin(e.toString()));
      _isLoading = false;
    }
  }

  Future<void> signOut() async {
    await _fakeUserRepository.signOut();
  }

  Future<void> listenToConnectivity() async {
    if (_internetStream != null) {
      await _internetStream.cancel();
    }
    emit(LoginState.connected(
        isConnected: await _connectionRepository.isConnectedToInternet(),
        isLoading: _isLoading));
    _internetStream = _connectionRepository.streamConnectedToInternet().listen(
      (bool isConnected) {
        if (isConnected) {
          if (_isLoading) {
            emit(
                const LoginState.connected(isConnected: true, isLoading: true));
          } else {
            emit(const LoginState.connected(
                isConnected: true, isLoading: false));
          }
        } else {
          emit(
              const LoginState.connected(isConnected: false, isLoading: false));
        }
      },
    );
  }

  @override
  Future<void> close() {
    // TODO: change when internetStream is activate again
    _internetStream.cancel();
    _notificationStream.cancel();
    return super.close();
  }
}
