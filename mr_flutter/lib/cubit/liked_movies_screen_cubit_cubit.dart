import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/listU.dart';
import 'package:mr_flutter/Data_Layer/models/user.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';

part 'liked_movies_screen_cubit_state.dart';
part 'liked_movies_screen_cubit_cubit.freezed.dart';

class LikedMoviesScreenCubit extends Cubit<LikedMoviesScreenCubitState> {
  final FakeUserRepository _fakeUserRepository;
  final ConnectionRepo _connectionRepository;
  final ListRepository _listRepository;

  LikedMoviesScreenCubit(this._fakeUserRepository, this._connectionRepository,
      this._listRepository)
      : super(LikedMoviesScreenCubitState.loading());

  Future<void> loadList() async {
    bool connected = await _connectionRepository.isConnectedToInternet();
    User user = await _fakeUserRepository.currentUser;
    ListU list = ListU();

    if (connected) {
      list = await _listRepository.getListFromInternet(userEmail: user.email);
    } else {
      list = await _listRepository.getListCached(userEmail: user.email);
    }

    emit(LikedMoviesScreenCubitState.loaded(list));
  }

  Future<void> deleteMovieFromList(int posDelete) async {
    User user = await _fakeUserRepository.currentUser;
    ListU list = _listRepository.deleteMovieFromList(
        userEmail: user.email, posDelete: posDelete);
    emit(LikedMoviesScreenCubitState.deleted());
    emit(LikedMoviesScreenCubitState.loaded(list));
  }
}
