import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';

part 'stuff_state.dart';
part 'stuff_cubit.freezed.dart';

class StuffCubit extends Cubit<StuffState> {
  final FakeUserRepository _fakeUserRepository;

  StuffCubit(this._fakeUserRepository) : super(StuffState.initial());
  Future<void> signOut() async {
    try {
      await _fakeUserRepository.signOut();
    } catch (e) {
      emit(StuffState.failedSignOut(e.toString()));
    }
  }
}
