import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/user.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/listRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/Repository_Layer/models/news.dart';
import 'package:mr_flutter/Repository_Layer/models/trendMovie.dart';
import 'package:mr_flutter/database_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_screen_state.dart';

class HomeScreenCubit extends Cubit<HomeScreenState> {
  final FakeUserRepository _userRepository;
  final MovieRepository _movieRepository;
  final MessageRepository _messageRepository;
  final ConnectionRepo _connectionRepository;
  final ListRepository _listRepository;

  var _notificationStream;

  HomeScreenCubit(this._movieRepository, this._userRepository,
      this._connectionRepository, this._messageRepository, this._listRepository)
      : super(Loading());

  Future<void> messageFirebase() async {
    if (_notificationStream != null) {
      await _notificationStream.cancel();
    }
    User user = await _userRepository.currentUser;
    List<TrendMovie> tms = await getTrendNC();

    _notificationStream =
        _messageRepository.streamNotification().listen((event) {
      emit(NotificationFirebase(user, tms, event));
    });
  }

  Future<void> checkConnectivity() async {
    bool connectivity = await _connectionRepository.isConnectedToInternet();
    if (connectivity) {
      emit(Connected());
    } else {
      User user = await _userRepository.currentUser;
      List<TrendMovie> tms = await getTrendNC();
      emit(NoConnection(user, tms));
    }
  }

  Future<void> resetCounter() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt('numberStart', -1);
  }

  Future<void> getTrendMovies() async {
    User user = await _userRepository.currentUser;
    List<TrendMovie> listTM = await _movieRepository.getTrends();
    int num = await getIntSP();
    print(num);
    // Aqui se puede hacer microptimization
    //for (var i = 0; i < listTMOld.length; i++) {
    //var title = listTMOld[i].title;
    //String g = await _movieRepository.getMovieGenre(title);
    //Map<String, dynamic> temp = toMT(listTMOld[i]);
    //temp['genre'] = g;
    //var mtNew = fromMT(temp);
    //listTMNew.add(mtNew);
    //}
    if (num == 0) {
      await DatabaseHelper.instance.addTrendMovies(listTM);
      //print('Guardo en base de datos');
    }
    emit(Loaded(user, listTM));
  }

  Future<String> getGenre(String title) async {
    String genre = await _movieRepository.getMovieGenre(title);
    return genre;
  }

  Future<List<TrendMovie>> readTrendMovies(int amountMovies) async {
    List<TrendMovie> listTM =
        await DatabaseHelper.instance.getMovieTrends(amountMovies);
    return listTM;
  }

  // Gets trends when their is no conexion
  Future<List<TrendMovie>> getTrendNC() async {
    List<TrendMovie> tms = await DatabaseHelper.instance.getMovieTrends(19);
    return tms;
  }

  Future<int> getIntSP() async {
    final prefs = await SharedPreferences.getInstance();
    // Si el numero es 0 es la primera vez que entra a la app
    // Si el numero es 1 no es la primera vez que entra a la app
    // Si retirn -1 significa que no hay nada guardado
    final number = prefs.getInt('numberStart');
    if (number == null) {
      return -1;
    }
    return number;
  }

  Future<void> incrementNumber() async {
    final prefs = await SharedPreferences.getInstance();
    int lastnumber = await getIntSP();
    int actual = lastnumber + 1;
    await prefs.setInt('numberStart', actual);
  }

/*
  Future<void> updateConection() async {
    User user = await _userRepository.currentUser;
    bool connected = await _connectionRepository.isConnectedToInternet();

    emit(NoConnection(user, connected));
  }
  */
  /*
  Future<void> getUser(String email) async {
    try {
      emit(HomeScreenState.loadingUser());
      final user = await _userRepository.fetchUser(email);
      emit(HomeScreenState.loadedUser(user));
    } on NetworkException {
      emit(HomeScreenState.errorUser(
          "Couldn't fetch USER. Is the device online ?"));
    }
  }
  */
}
