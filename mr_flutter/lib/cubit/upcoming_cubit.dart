import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/user.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/connectionRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/messageRepo.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/movieRepo.dart';
import 'package:mr_flutter/Repository_Layer/models/news.dart';
import 'package:mr_flutter/Repository_Layer/models/trendMovie.dart';
import 'package:mr_flutter/database_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'upcoming_state.dart';

class UpcomingCubit extends Cubit<UpcomingState> {
  final MovieRepository _movieRepository;

  final ConnectionRepo _connectionRepository;

  UpcomingCubit(this._movieRepository, this._connectionRepository)
      : super(Loading());

  Future<void> checkConnectivity() async {
    bool connectivity = await _connectionRepository.isConnectedToInternet();
    if (connectivity) {
      emit(Connected());
    } else {
      //User user = await _userRepository.currentUser;
      //List<TrendMovie> tms = await getTrendNC();
      emit(NoConnection());
    }
  }

  Future<void> getUpcoming() async {
    var upc = await _movieRepository.getUpcoming();

    emit(Loaded(upc));
  }
}
