import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mr_flutter/Data_Layer/models/user.dart';
import 'package:mr_flutter/Repository_Layer/Repositories/fakeUserRepo.dart';

part 'app_event.dart';
part 'app_state.dart';
part 'app_bloc.freezed.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final FakeUserRepository _fakeUserRepository;
  late final StreamSubscription<Future<User>> _userSubscription;
  AppBloc(this._fakeUserRepository) : super(_Loading()) {
    _userSubscription = _fakeUserRepository.isLogged.listen((user) async {
      return add(AppEvent.appUserChanged(await user));
    });
    on<AppEvent>((event, emit) {
      // TODO: implement event handler
      event.when(appUserChanged: (user) => _appUserChanged(user, emit));
    });
  }

  _appUserChanged(User user, Emitter<AppState> emit) async {
    print('user.isNotEmpty');
    print(user);
    emit(user.email != ""
        ? AppState.authenticated(user)
        : const AppState.unauthenticated());
  }

  @override
  Future<void> close() {
    _userSubscription.cancel();
    return super.close();
  }

  Future initialState() async {
    var currentUser = await _fakeUserRepository.currentUser;
    AppState a = currentUser.email != ""
        ? _Authenticated(currentUser)
        : const _Unauthenticated();
    this.emit(a);
  }

  // static AppState _initialState() {

  //         return _fakeUserRepository.currentUser.isNotEmpty
  //             ? _Authenticated(_fakeUserRepository.currentUser)
  //             : const _Unauthenticated()
  // }
}
