import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'Repository_Layer/models/recoMovie.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory dd = await getApplicationDocumentsDirectory();
    String path = join(dd.path, 'movies.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
CREATE TABLE movie (
    id integer NOT NULL,
    ratingscore double precision NOT NULL,
    postercomplete character varying NOT NULL,
    overview character varying,
    streamingplataformURL character varying,
    genrename charater varying
)''');
    await db.execute(
        '''CREATE TABLE movietrend(title character varying,numLikes integer,posterComplete character varying,duration integer, ratingscore double, genre character varying,PRIMARY KEY (title));''');
  }

  Future<void> addRecoMovies(List<RecomendedMovie> rm) async {
    final Database db = await database;
    for (var i = 0; i < rm.length; i++) {
      await db.insert('movie', toM(rm[i]),
          conflictAlgorithm: ConflictAlgorithm.replace);
    }
  }

  Future<void> addTrendMovies(List<TrendMovie> tm) async {
    final Database db = await database;
    for (var i = 0; i < tm.length; i++) {
      await db.insert('movietrend', toMT(tm[i]),
          conflictAlgorithm: ConflictAlgorithm.replace);
    }
  }

  Future<List<RecomendedMovie>> getRecoMovies(int numMovies) async {
    final Database db = await database;
    List<RecomendedMovie> rm = [];
    final List<Map<String, dynamic>> maps = await db.query('movie');
    print('Este es maps');
    print(maps);
    RecomendedMovie? temp = null;
    for (var i = 0; i < numMovies; i++) {
      //print('Runtime type');
      //print(int.parse(maps[i]['id']).runtimeType);
      temp = fromM(json.encode(maps[i]));
      rm.add(temp);
    }
    return rm;
  }

  Future<List<TrendMovie>> getMovieTrends(int numMovies) async {
    final Database db = await database;
    List<TrendMovie> tms = [];
    final List<Map<String, dynamic>> maps = await db.query('movietrend');
    TrendMovie? temp = null;
    for (var i = 0; i < numMovies; i++) {
      //print('Runtime type');
      //print(int.parse(maps[i]['id']).runtimeType);
      var temp = fromMT(json.encode(maps[i]));
      if (temp.title != '') {
        tms.add(temp);
      }
    }
    return tms;
  }
}
