// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trendMovie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TrendMovie _$$_TrendMovieFromJson(Map<String, dynamic> json) =>
    _$_TrendMovie(
      id: json['id'] as int,
      numLikes: json['numLikes'] as int,
      title: json['title'] as String,
      posterComplete: json['posterComplete'] as String,
      duration: json['duration'] as int,
      ratingScore: (json['ratingScore'] as num).toDouble(),
      genres: (json['genres'] as List<dynamic>)
          .map((e) => Genre.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TrendMovieToJson(_$_TrendMovie instance) =>
    <String, dynamic>{
      'id': instance.id,
      'numLikes': instance.numLikes,
      'title': instance.title,
      'posterComplete': instance.posterComplete,
      'duration': instance.duration,
      'ratingScore': instance.ratingScore,
      'genres': instance.genres,
    };
