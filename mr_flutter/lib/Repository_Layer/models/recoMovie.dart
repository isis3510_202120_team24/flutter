import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
import 'package:mr_flutter/Data_Layer/models/models.dart';
part 'recoMovie.freezed.dart';
part 'recoMovie.g.dart';

RecomendedMovie recomendedMovieFromJson(String str) =>
    RecomendedMovie.fromJson(json.decode(str));

String recomendedMovieToJson(RecomendedMovie data) =>
    json.encode(data.toJson());

@freezed
abstract class RecomendedMovie with _$RecomendedMovie {
  const factory RecomendedMovie({
    required int id,
    required List<Genre> genres,
    required String posterComplete,
    required String overview,
    required List<StreamingPlataform> streamingPlataforms,
    required double ratingScore,
  }) = _RecomendedMovie;
  factory RecomendedMovie.fromJson(Map<String, dynamic> json) =>
      _$RecomendedMovieFromJson(json);
}

Map<String, dynamic> toM(RecomendedMovie rm) {
  return {
    'id': rm.id,
    'genrename': rm.genres[0].name,
    'postercomplete': rm.posterComplete,
    'overview': rm.overview,
    'streamingplataformURL': rm.streamingPlataforms[0].logoPath,
    'ratingscore': rm.ratingScore,
  };
}

RecomendedMovie fromM(jsonP) {
  var id = json.decode(jsonP)['id'];

  var posterComplete = json.decode(jsonP)['postercomplete'];
  var overview = json.decode(jsonP)['overview'];
  var ratingscore = json.decode(jsonP)['ratingscore'];

  return RecomendedMovie(
      id: id,
      genres: [Genre(name: json.decode(jsonP)['genrename'])],
      posterComplete: posterComplete,
      overview: overview,
      streamingPlataforms: [
        StreamingPlataform(
            logoPath: json.decode(jsonP)['streamingplataformURL'],
            id: -1,
            providerName: '')
      ],
      ratingScore: ratingscore);
}
