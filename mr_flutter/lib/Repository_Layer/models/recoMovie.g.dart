// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recoMovie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RecomendedMovie _$$_RecomendedMovieFromJson(Map<String, dynamic> json) =>
    _$_RecomendedMovie(
      id: json['id'] as int,
      genres: (json['genres'] as List<dynamic>)
          .map((e) => Genre.fromJson(e as Map<String, dynamic>))
          .toList(),
      posterComplete: json['posterComplete'] as String,
      overview: json['overview'] as String,
      streamingPlataforms: (json['streamingPlataforms'] as List<dynamic>)
          .map((e) => StreamingPlataform.fromJson(e as Map<String, dynamic>))
          .toList(),
      ratingScore: (json['ratingScore'] as num).toDouble(),
    );

Map<String, dynamic> _$$_RecomendedMovieToJson(_$_RecomendedMovie instance) =>
    <String, dynamic>{
      'id': instance.id,
      'genres': instance.genres,
      'posterComplete': instance.posterComplete,
      'overview': instance.overview,
      'streamingPlataforms': instance.streamingPlataforms,
      'ratingScore': instance.ratingScore,
    };
