// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'trendMovie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TrendMovie _$TrendMovieFromJson(Map<String, dynamic> json) {
  return _TrendMovie.fromJson(json);
}

/// @nodoc
class _$TrendMovieTearOff {
  const _$TrendMovieTearOff();

  _TrendMovie call(
      {required int id,
      required int numLikes,
      required String title,
      required String posterComplete,
      required int duration,
      required double ratingScore,
      required List<Genre> genres}) {
    return _TrendMovie(
      id: id,
      numLikes: numLikes,
      title: title,
      posterComplete: posterComplete,
      duration: duration,
      ratingScore: ratingScore,
      genres: genres,
    );
  }

  TrendMovie fromJson(Map<String, Object> json) {
    return TrendMovie.fromJson(json);
  }
}

/// @nodoc
const $TrendMovie = _$TrendMovieTearOff();

/// @nodoc
mixin _$TrendMovie {
  int get id => throw _privateConstructorUsedError;
  int get numLikes => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get posterComplete => throw _privateConstructorUsedError;
  int get duration => throw _privateConstructorUsedError;
  double get ratingScore => throw _privateConstructorUsedError;
  List<Genre> get genres => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TrendMovieCopyWith<TrendMovie> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TrendMovieCopyWith<$Res> {
  factory $TrendMovieCopyWith(
          TrendMovie value, $Res Function(TrendMovie) then) =
      _$TrendMovieCopyWithImpl<$Res>;
  $Res call(
      {int id,
      int numLikes,
      String title,
      String posterComplete,
      int duration,
      double ratingScore,
      List<Genre> genres});
}

/// @nodoc
class _$TrendMovieCopyWithImpl<$Res> implements $TrendMovieCopyWith<$Res> {
  _$TrendMovieCopyWithImpl(this._value, this._then);

  final TrendMovie _value;
  // ignore: unused_field
  final $Res Function(TrendMovie) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? numLikes = freezed,
    Object? title = freezed,
    Object? posterComplete = freezed,
    Object? duration = freezed,
    Object? ratingScore = freezed,
    Object? genres = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      numLikes: numLikes == freezed
          ? _value.numLikes
          : numLikes // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      posterComplete: posterComplete == freezed
          ? _value.posterComplete
          : posterComplete // ignore: cast_nullable_to_non_nullable
              as String,
      duration: duration == freezed
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as int,
      ratingScore: ratingScore == freezed
          ? _value.ratingScore
          : ratingScore // ignore: cast_nullable_to_non_nullable
              as double,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
    ));
  }
}

/// @nodoc
abstract class _$TrendMovieCopyWith<$Res> implements $TrendMovieCopyWith<$Res> {
  factory _$TrendMovieCopyWith(
          _TrendMovie value, $Res Function(_TrendMovie) then) =
      __$TrendMovieCopyWithImpl<$Res>;
  @override
  $Res call(
      {int id,
      int numLikes,
      String title,
      String posterComplete,
      int duration,
      double ratingScore,
      List<Genre> genres});
}

/// @nodoc
class __$TrendMovieCopyWithImpl<$Res> extends _$TrendMovieCopyWithImpl<$Res>
    implements _$TrendMovieCopyWith<$Res> {
  __$TrendMovieCopyWithImpl(
      _TrendMovie _value, $Res Function(_TrendMovie) _then)
      : super(_value, (v) => _then(v as _TrendMovie));

  @override
  _TrendMovie get _value => super._value as _TrendMovie;

  @override
  $Res call({
    Object? id = freezed,
    Object? numLikes = freezed,
    Object? title = freezed,
    Object? posterComplete = freezed,
    Object? duration = freezed,
    Object? ratingScore = freezed,
    Object? genres = freezed,
  }) {
    return _then(_TrendMovie(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      numLikes: numLikes == freezed
          ? _value.numLikes
          : numLikes // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      posterComplete: posterComplete == freezed
          ? _value.posterComplete
          : posterComplete // ignore: cast_nullable_to_non_nullable
              as String,
      duration: duration == freezed
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as int,
      ratingScore: ratingScore == freezed
          ? _value.ratingScore
          : ratingScore // ignore: cast_nullable_to_non_nullable
              as double,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TrendMovie implements _TrendMovie {
  const _$_TrendMovie(
      {required this.id,
      required this.numLikes,
      required this.title,
      required this.posterComplete,
      required this.duration,
      required this.ratingScore,
      required this.genres});

  factory _$_TrendMovie.fromJson(Map<String, dynamic> json) =>
      _$$_TrendMovieFromJson(json);

  @override
  final int id;
  @override
  final int numLikes;
  @override
  final String title;
  @override
  final String posterComplete;
  @override
  final int duration;
  @override
  final double ratingScore;
  @override
  final List<Genre> genres;

  @override
  String toString() {
    return 'TrendMovie(id: $id, numLikes: $numLikes, title: $title, posterComplete: $posterComplete, duration: $duration, ratingScore: $ratingScore, genres: $genres)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TrendMovie &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.numLikes, numLikes) ||
                const DeepCollectionEquality()
                    .equals(other.numLikes, numLikes)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.posterComplete, posterComplete) ||
                const DeepCollectionEquality()
                    .equals(other.posterComplete, posterComplete)) &&
            (identical(other.duration, duration) ||
                const DeepCollectionEquality()
                    .equals(other.duration, duration)) &&
            (identical(other.ratingScore, ratingScore) ||
                const DeepCollectionEquality()
                    .equals(other.ratingScore, ratingScore)) &&
            (identical(other.genres, genres) ||
                const DeepCollectionEquality().equals(other.genres, genres)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(numLikes) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(posterComplete) ^
      const DeepCollectionEquality().hash(duration) ^
      const DeepCollectionEquality().hash(ratingScore) ^
      const DeepCollectionEquality().hash(genres);

  @JsonKey(ignore: true)
  @override
  _$TrendMovieCopyWith<_TrendMovie> get copyWith =>
      __$TrendMovieCopyWithImpl<_TrendMovie>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TrendMovieToJson(this);
  }
}

abstract class _TrendMovie implements TrendMovie {
  const factory _TrendMovie(
      {required int id,
      required int numLikes,
      required String title,
      required String posterComplete,
      required int duration,
      required double ratingScore,
      required List<Genre> genres}) = _$_TrendMovie;

  factory _TrendMovie.fromJson(Map<String, dynamic> json) =
      _$_TrendMovie.fromJson;

  @override
  int get id => throw _privateConstructorUsedError;
  @override
  int get numLikes => throw _privateConstructorUsedError;
  @override
  String get title => throw _privateConstructorUsedError;
  @override
  String get posterComplete => throw _privateConstructorUsedError;
  @override
  int get duration => throw _privateConstructorUsedError;
  @override
  double get ratingScore => throw _privateConstructorUsedError;
  @override
  List<Genre> get genres => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$TrendMovieCopyWith<_TrendMovie> get copyWith =>
      throw _privateConstructorUsedError;
}
