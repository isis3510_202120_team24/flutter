// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'recoMovie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecomendedMovie _$RecomendedMovieFromJson(Map<String, dynamic> json) {
  return _RecomendedMovie.fromJson(json);
}

/// @nodoc
class _$RecomendedMovieTearOff {
  const _$RecomendedMovieTearOff();

  _RecomendedMovie call(
      {required int id,
      required List<Genre> genres,
      required String posterComplete,
      required String overview,
      required List<StreamingPlataform> streamingPlataforms,
      required double ratingScore}) {
    return _RecomendedMovie(
      id: id,
      genres: genres,
      posterComplete: posterComplete,
      overview: overview,
      streamingPlataforms: streamingPlataforms,
      ratingScore: ratingScore,
    );
  }

  RecomendedMovie fromJson(Map<String, Object> json) {
    return RecomendedMovie.fromJson(json);
  }
}

/// @nodoc
const $RecomendedMovie = _$RecomendedMovieTearOff();

/// @nodoc
mixin _$RecomendedMovie {
  int get id => throw _privateConstructorUsedError;
  List<Genre> get genres => throw _privateConstructorUsedError;
  String get posterComplete => throw _privateConstructorUsedError;
  String get overview => throw _privateConstructorUsedError;
  List<StreamingPlataform> get streamingPlataforms =>
      throw _privateConstructorUsedError;
  double get ratingScore => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecomendedMovieCopyWith<RecomendedMovie> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecomendedMovieCopyWith<$Res> {
  factory $RecomendedMovieCopyWith(
          RecomendedMovie value, $Res Function(RecomendedMovie) then) =
      _$RecomendedMovieCopyWithImpl<$Res>;
  $Res call(
      {int id,
      List<Genre> genres,
      String posterComplete,
      String overview,
      List<StreamingPlataform> streamingPlataforms,
      double ratingScore});
}

/// @nodoc
class _$RecomendedMovieCopyWithImpl<$Res>
    implements $RecomendedMovieCopyWith<$Res> {
  _$RecomendedMovieCopyWithImpl(this._value, this._then);

  final RecomendedMovie _value;
  // ignore: unused_field
  final $Res Function(RecomendedMovie) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? genres = freezed,
    Object? posterComplete = freezed,
    Object? overview = freezed,
    Object? streamingPlataforms = freezed,
    Object? ratingScore = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
      posterComplete: posterComplete == freezed
          ? _value.posterComplete
          : posterComplete // ignore: cast_nullable_to_non_nullable
              as String,
      overview: overview == freezed
          ? _value.overview
          : overview // ignore: cast_nullable_to_non_nullable
              as String,
      streamingPlataforms: streamingPlataforms == freezed
          ? _value.streamingPlataforms
          : streamingPlataforms // ignore: cast_nullable_to_non_nullable
              as List<StreamingPlataform>,
      ratingScore: ratingScore == freezed
          ? _value.ratingScore
          : ratingScore // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
abstract class _$RecomendedMovieCopyWith<$Res>
    implements $RecomendedMovieCopyWith<$Res> {
  factory _$RecomendedMovieCopyWith(
          _RecomendedMovie value, $Res Function(_RecomendedMovie) then) =
      __$RecomendedMovieCopyWithImpl<$Res>;
  @override
  $Res call(
      {int id,
      List<Genre> genres,
      String posterComplete,
      String overview,
      List<StreamingPlataform> streamingPlataforms,
      double ratingScore});
}

/// @nodoc
class __$RecomendedMovieCopyWithImpl<$Res>
    extends _$RecomendedMovieCopyWithImpl<$Res>
    implements _$RecomendedMovieCopyWith<$Res> {
  __$RecomendedMovieCopyWithImpl(
      _RecomendedMovie _value, $Res Function(_RecomendedMovie) _then)
      : super(_value, (v) => _then(v as _RecomendedMovie));

  @override
  _RecomendedMovie get _value => super._value as _RecomendedMovie;

  @override
  $Res call({
    Object? id = freezed,
    Object? genres = freezed,
    Object? posterComplete = freezed,
    Object? overview = freezed,
    Object? streamingPlataforms = freezed,
    Object? ratingScore = freezed,
  }) {
    return _then(_RecomendedMovie(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      genres: genres == freezed
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>,
      posterComplete: posterComplete == freezed
          ? _value.posterComplete
          : posterComplete // ignore: cast_nullable_to_non_nullable
              as String,
      overview: overview == freezed
          ? _value.overview
          : overview // ignore: cast_nullable_to_non_nullable
              as String,
      streamingPlataforms: streamingPlataforms == freezed
          ? _value.streamingPlataforms
          : streamingPlataforms // ignore: cast_nullable_to_non_nullable
              as List<StreamingPlataform>,
      ratingScore: ratingScore == freezed
          ? _value.ratingScore
          : ratingScore // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RecomendedMovie implements _RecomendedMovie {
  const _$_RecomendedMovie(
      {required this.id,
      required this.genres,
      required this.posterComplete,
      required this.overview,
      required this.streamingPlataforms,
      required this.ratingScore});

  factory _$_RecomendedMovie.fromJson(Map<String, dynamic> json) =>
      _$$_RecomendedMovieFromJson(json);

  @override
  final int id;
  @override
  final List<Genre> genres;
  @override
  final String posterComplete;
  @override
  final String overview;
  @override
  final List<StreamingPlataform> streamingPlataforms;
  @override
  final double ratingScore;

  @override
  String toString() {
    return 'RecomendedMovie(id: $id, genres: $genres, posterComplete: $posterComplete, overview: $overview, streamingPlataforms: $streamingPlataforms, ratingScore: $ratingScore)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RecomendedMovie &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.genres, genres) ||
                const DeepCollectionEquality().equals(other.genres, genres)) &&
            (identical(other.posterComplete, posterComplete) ||
                const DeepCollectionEquality()
                    .equals(other.posterComplete, posterComplete)) &&
            (identical(other.overview, overview) ||
                const DeepCollectionEquality()
                    .equals(other.overview, overview)) &&
            (identical(other.streamingPlataforms, streamingPlataforms) ||
                const DeepCollectionEquality()
                    .equals(other.streamingPlataforms, streamingPlataforms)) &&
            (identical(other.ratingScore, ratingScore) ||
                const DeepCollectionEquality()
                    .equals(other.ratingScore, ratingScore)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(genres) ^
      const DeepCollectionEquality().hash(posterComplete) ^
      const DeepCollectionEquality().hash(overview) ^
      const DeepCollectionEquality().hash(streamingPlataforms) ^
      const DeepCollectionEquality().hash(ratingScore);

  @JsonKey(ignore: true)
  @override
  _$RecomendedMovieCopyWith<_RecomendedMovie> get copyWith =>
      __$RecomendedMovieCopyWithImpl<_RecomendedMovie>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RecomendedMovieToJson(this);
  }
}

abstract class _RecomendedMovie implements RecomendedMovie {
  const factory _RecomendedMovie(
      {required int id,
      required List<Genre> genres,
      required String posterComplete,
      required String overview,
      required List<StreamingPlataform> streamingPlataforms,
      required double ratingScore}) = _$_RecomendedMovie;

  factory _RecomendedMovie.fromJson(Map<String, dynamic> json) =
      _$_RecomendedMovie.fromJson;

  @override
  int get id => throw _privateConstructorUsedError;
  @override
  List<Genre> get genres => throw _privateConstructorUsedError;
  @override
  String get posterComplete => throw _privateConstructorUsedError;
  @override
  String get overview => throw _privateConstructorUsedError;
  @override
  List<StreamingPlataform> get streamingPlataforms =>
      throw _privateConstructorUsedError;
  @override
  double get ratingScore => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RecomendedMovieCopyWith<_RecomendedMovie> get copyWith =>
      throw _privateConstructorUsedError;
}
