import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';
import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
import 'models.dart';
part 'trendMovie.freezed.dart';
part 'trendMovie.g.dart';

TrendMovie trendMovieFromJson(String str) =>
    TrendMovie.fromJson(json.decode(str));

String trendMovieToJson(TrendMovie data) => json.encode(data.toJson());

@freezed
abstract class TrendMovie with _$TrendMovie {
  const factory TrendMovie(
      {required int id,
      required int numLikes,
      required String title,
      required String posterComplete,
      required int duration,
      required double ratingScore,
      required List<Genre> genres}) = _TrendMovie;

  factory TrendMovie.fromJson(Map<String, dynamic> json) =>
      _$TrendMovieFromJson(json);
}

Map<String, dynamic> toMT(TrendMovie tm) {
  return {
    'id': tm.id,
    'title': tm.title,
    'numLikes': tm.numLikes,
    'posterComplete': tm.posterComplete,
    'duration': tm.duration,
    'ratingscore': tm.ratingScore,
    'genre': tm.genres[0].name,
  };
}

TrendMovie fromMT(jsonP) {
  var id = json.decode(jsonP)['id'];
  var title = json.decode(jsonP)['title'];
  var duration = json.decode(jsonP)['duration'];
  var posterComplete = json.decode(jsonP)['posterComplete'];
  var numLikes = json.decode(jsonP)['numLikes'];
  var ratingscore = json.decode(jsonP)['ratingscore'];

  return TrendMovie(
      id: id,
      title: title,
      genres: [Genre(name: json.decode(jsonP)['genre'])],
      posterComplete: posterComplete,
      duration: duration,
      numLikes: numLikes,
      ratingScore: ratingscore);
}
