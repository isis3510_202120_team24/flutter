// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_News _$$_NewsFromJson(Map<String, dynamic> json) => _$_News(
      posterComplete: json['posterComplete'] as String,
      overview: json['overview'] as String,
      title: json['title'] as String,
      releasedate: json['releasedate'] as String,
    );

Map<String, dynamic> _$$_NewsToJson(_$_News instance) => <String, dynamic>{
      'posterComplete': instance.posterComplete,
      'overview': instance.overview,
      'title': instance.title,
      'releasedate': instance.releasedate,
    };
