import 'package:meta/meta.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'dart:convert';
part 'news.freezed.dart';
part 'news.g.dart';

News newsFromJson(String str) => News.fromJson(json.decode(str));
String newsToJson(News data) => json.encode(data.toJson());

@freezed
abstract class News with _$News {
  const factory News({
    required String posterComplete,
    required String overview,
    required String title,
    required String releasedate,
  }) = _News;

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);
}
