import 'dart:math';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:mr_flutter/Data_Layer/Service_Adapters/userAdapter.dart';

import '../../Data_Layer/models/user.dart';
import 'package:cache_manager/cache_manager.dart';

String keyUser = 'user';
String keyPie = 'pie';

abstract class UserRepository {
  /// Throws [NetworkException].
  Future signWithEmailAndPassword(String email, String password);
  Future<User> fetchUser(String email);
  Future signOut();
}

class FakeUserRepository implements UserRepository {
  FakeUserRepository(
      {firebase_auth.FirebaseAuth? firebaseAuth,
      required UserAdapter userAdapter})
      : _firebaseAuth = firebaseAuth ?? firebase_auth.FirebaseAuth.instance,
        _userAdapter = userAdapter;

  final UserAdapter _userAdapter;
  final firebase_auth.FirebaseAuth _firebaseAuth;
  final ran = Random().nextInt(10);
  User tCurrentUser = User.empty;
  static Map<String, double> noStats = {"Like a movie to see stats": 0.0};
  Map<String, double> tpieData = noStats;

  Stream<Future<User>> get isLogged {
    return _firebaseAuth.authStateChanges().map((firebaseUser) async {
      print('tCurrett fireabse');
      print(tCurrentUser);

      final user = firebaseUser == null ? User.empty : await currentUser;
      return user;
    });
  }

  Future<Map<String, double>> get pieData async {
    try {
      dynamic pieCache = await ReadCache.getJson(key: keyPie);
      if (pieCache != null) {
        User user = await currentUser;
        Map<String, double> pie = await makePie(user);
        makePie(user).then((map) => WriteCache.setJson(key: keyPie, value: map),
            onError: (a) {
          // TODO: Manejar el default
          print("NO ES UN ERROR c:");
          print(a);
        });
        if (pieCache.isEmpty ?? true) {
          return noStats;
        } else {
          return pie;
        }
      }

      return tpieData;
    } catch (a) {
      print(e);
      return tpieData;
    }
  }

  Future<User> get currentUser async {
    try {
      if (tCurrentUser == User.empty) {
        dynamic cacheUser = await ReadCache.getJson(key: keyUser);
        User userFromCache = User.fromJson(cacheUser);
        if (userFromCache != User.empty) {
          tCurrentUser = userFromCache;
        }
      }
      return tCurrentUser;
    } catch (a) {
      print(e);
      return User.empty;
    }
  }

  Future<void> fetchAndUpdateUser() async {
    // Si falla, es porque en algun momento el cache no tiene User.empty
    User user = tCurrentUser;
    if (user != User.empty) {
      tCurrentUser = await fetchUser(user.email);
      WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());
    } else {
      dynamic cacheUser = await ReadCache.getJson(key: keyUser);
      User userFromCache = User.fromJson(cacheUser);
      if (userFromCache != User.empty) {
        tCurrentUser = await fetchUser(userFromCache.email);
        WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());
      }
    }
  }

  Future<void> updateCurrentUser(User user) async {
    try {
      await WriteCache.setJson(key: keyUser, value: user.toJson());
      tCurrentUser = user;
    } catch (a) {
      throw a;
    }
  }

  @override
  Future<User> fetchUser(String email) async {
    List<User> userL = [];
    // Simulate network delay
    // Simulate some network exception

    // Return "fetched" weather
    //when called with Firebase needs to userjson.data
    Future<User> res = _userAdapter.getUser(email);
    User temp = await res;
    userL.add(temp);
    return userL[0];
  }

  @override
  Future<User> signWithEmailAndPassword(String email, String password) async {
    try {
      // login
      final user = await _userAdapter.getUser(email);
      tCurrentUser = user;
      WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());

      if (user != null) {
        firebase_auth.UserCredential result = await _firebaseAuth
            .signInWithEmailAndPassword(email: email, password: password);
        firebase_auth.User? userFirebase = result.user;
        print(userFirebase);

        if (userFirebase != null) {
          print("tCurrentUser");
          print(tCurrentUser);
          WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());
          CachedNetworkImageProvider(tCurrentUser.image);
        } else {
          tCurrentUser = User.empty;
          WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());

          throw Exception(
              'Something went wrong with login process. Try again later!');
        }
      }
      return tCurrentUser;
    } catch (e) {
      throw e;
    }
  }

  Future registerWithEmailAndPassword(
      String email, String password, String name, String birthday) async {
    try {
      //Register in Auth
      // TODO: Tocara primero en db luego firebase
      await updateCurrentUser(User.RegisterUser);

      firebase_auth.UserCredential result = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      firebase_auth.User? user = result.user;
      if (user != null) {
        tCurrentUser = await _userAdapter.createUser(email, name, birthday);
        WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());
      } else {
        throw Exception(
            'Something went wrong with register process. Try again later!');
      }

      return User.empty;
    } catch (e) {
      throw e;
    }
  }

  Future<Map<String, double>> pieMap() async {
    dynamic cacheUser = await ReadCache.getJson(key: keyUser);
    User user = User.fromJson(cacheUser);
    List<Stat> statList = user.stats;
    print(statList);
    Map<String, double> map = Map.fromIterable(statList,
        key: (item) => item.genre.name,
        value: (item) => double.parse(item.amount.toString()));
    print(map);
    tpieData = map;
    return map;
  }

  Future<Map<String, double>> makePie(User user) async {
    List<Stat> statList = user.stats;
    print(statList);
    Map<String, double> map = Map.fromIterable(statList,
        key: (item) => item.genre.name,
        value: (item) => double.parse(item.amount.toString()));
    print(map);
    tpieData = map;
    WriteCache.setJson(key: keyPie, value: map);
    if (map.isEmpty) {
      return noStats;
    }
    return map;
  }

  Future<Map<String, double>> updatePie() async {
    dynamic cacheUser = await ReadCache.getJson(key: keyUser);
    User user = User.fromJson(cacheUser);
    User cUser = await _userAdapter.getUser(user.email);
    print(cUser);
    List<Stat> statList = cUser.stats;
    print(statList);
    Map<String, double> map = Map.fromIterable(statList,
        key: (item) => item.genre.name,
        value: (item) => double.parse(item.amount.toString()));
    print(map);
    WriteCache.setJson(key: keyPie, value: map);
    tpieData = map;
    return map;
  }

  @override
  Future signOut() async {
    try {
      tCurrentUser = User.empty;
      tpieData = noStats;
      WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());
      WriteCache.setJson(key: keyPie, value: noStats);
      return await _firebaseAuth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future deleteUser() async {
    try {
      User userdelete = User(
          email: tCurrentUser.email,
          numRecomendedMovies: 0,
          numLikedRecomendedMovies: 0,
          timeInApp: 0,
          avgPassBeforeLike: 0,
          numLogin: 0,
          image: '',
          name: '',
          birthday: '',
          lists: [],
          stats: []);
      tpieData = noStats;
      WriteCache.setJson(key: keyUser, value: tCurrentUser.toJson());
      return await _userAdapter.updateUser(userdelete.toJson());
      ;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}

class NetworkException implements Exception {}
