import 'package:mr_flutter/Data_Layer/Service_Adapters/moviesAdapter.dart';
import 'package:mr_flutter/Data_Layer/models/models.dart';
import 'package:mr_flutter/Data_Layer/models/sp_country.dart';
import 'package:mr_flutter/Data_Layer/models/sp_country.dart';
import 'package:mr_flutter/Repository_Layer/models/models.dart';

class MovieRepository {
  MovieRepository(this._movieAdapter);

  final MovieServiceAdapter _movieAdapter;

  Future<String> getMovieGenre(String title) async {
    String g = await _movieAdapter.getMovieGenre(title);
    return g;
  }

  Future<dynamic> getUpcoming() async {
    return await _movieAdapter.getUpcoming();
  }

  Future<List<TrendMovie>> getTrends() async {
    List<Movie> allMovies = await _movieAdapter.getTrends();
    List<TrendMovie> tm = [];
    for (var movie in allMovies) {
      var id = movie.id;
      var posterComplete = movie.posterComplete;
      var runTime = movie.runTime;
      var ratingScore = movie.ratingScore;
      var genres = movie.genres;
      var title = movie.title;
      var numLikes = movie.numLikes;
      TrendMovie temp = TrendMovie(
          id: id,
          numLikes: numLikes,
          title: title,
          posterComplete: posterComplete,
          duration: runTime,
          ratingScore: ratingScore,
          genres: genres);
      tm.add(temp);
    }
    return tm;
  }

  Future<List<RecomendedMovie>> getRecommended(
      String email, List<int> movieIds, String countryCode) async {
    List<Movie> allMovies =
        await _movieAdapter.getRecommendedMock(email, movieIds, countryCode);
    List<RecomendedMovie> rm = [];
    for (var movie in allMovies) {
      var id = movie.id;
      var posterComplete = movie.posterComplete;
      var ratingScore = movie.ratingScore;
      var overview = movie.overview;
      var genres = movie.genres;
      var plataforms = movie.plataformsByCountry;
      List<StreamingPlataform> sps = [];
      for (var spCountry in plataforms) {
        var sp = spCountry.streamingPlataform;
        sps.add(sp);
      }

      RecomendedMovie temp = RecomendedMovie(
          id: id,
          genres: genres,
          posterComplete: posterComplete,
          overview: overview,
          streamingPlataforms: sps,
          ratingScore: ratingScore);
      rm.add(temp);
    }
    return rm;
  }

  Future<List<News>> getNews() async {
    List<Movie> allMovies = await _movieAdapter.getMovies();
    List<News> news = [];
    for (var movie in allMovies) {
      var posterComplete = movie.posterComplete;
      var overview = movie.overview;
      var title = movie.title;
      var releaseDate = movie.releaseDate;
      News temp = News(
          posterComplete: posterComplete,
          overview: overview,
          title: title,
          releasedate: releaseDate);
      news.add(temp);
    }
    return news;
  }

  Future<void> likeRecommendations(
      String currentEmail, int movieId, int actual) async {
    await _movieAdapter.likeRecommendations(currentEmail, movieId, actual);
  }
}
