import 'package:firebase_messaging/firebase_messaging.dart';

class MessageRepository {
  MessageRepository({required FirebaseMessaging messaging})
      : _messaging = messaging;

  final FirebaseMessaging _messaging;
  static const String USER_RETENTION = 'USER_RETENTION';

  Stream<Map<String, dynamic>?> streamNotification() {
    // For handling the received notifications
    return FirebaseMessaging.onMessage.map((RemoteMessage message) {
      // Parse the message received
      return messageInterpreter(message);
    });
  }

  void registerNotification() async {
    // 1. Instantiate Firebase Messaging

    // 2. On iOS, this helps to take the user permissions
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
      _messaging.getToken().then((token) {
        print(token);
      });
    } else {
      print('User declined or has not accepted permission');
    }
  }

  Future<Map<String, dynamic>?> checkForInitialMessage(message) async {
    RemoteMessage? initialMessage = await _messaging.getInitialMessage();

    if (initialMessage != null) {
      // Parse the message received
      Map<String, dynamic>? notification = messageInterpreter(initialMessage);
      print(notification);
      return notification;
    }
  }

  static Map<String, dynamic>? messageInterpreter(message) {
    print(message.data['type_notification'] == USER_RETENTION);
    if (message.data['type_notification'] == USER_RETENTION) {
      return {
        "title": message.notification?.title ?? "",
        "body": message.notification?.body ?? "",
        "movie": int.parse(message.data['movie'])
      };
    }
    return null;
  }
}
