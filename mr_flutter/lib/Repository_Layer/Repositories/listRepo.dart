import 'package:cache_manager/cache_manager.dart';
import 'package:mr_flutter/Data_Layer/Service_Adapters/listAdapter.dart';
import 'package:mr_flutter/Data_Layer/models/listU.dart';

const keyListU = 'listU';

class ListRepository {
  ListRepository(this._listAdapter);
  final ListAdapter _listAdapter;
  ListU cListU = ListU();

  Future<ListU> getListFromInternet(
      {required String userEmail, name: 'Likes'}) async {
    ListU allMovies =
        await _listAdapter.getList(name: name, userEmail: userEmail);
    if (allMovies.name == '') {
      await _listAdapter.createList(name: name, userEmail: userEmail);
    }
    saveListU(allMovies);
    return allMovies;
  }

  Future<ListU> getListCached(
      {required String userEmail, name: 'Likes'}) async {
    if (cListU.name != '') {
      return cListU;
    }
    dynamic cacheListU = await ReadCache.getJson(key: keyListU);
    ListU listUFromCache = ListU.fromJson(cacheListU);
    return listUFromCache;
  }

  Future<void> addMovieToList(
      {required String userEmail,
      String name: 'Likes',
      required int movieId}) async {
    await _listAdapter.addMovieToList(
        name: name, userEmail: userEmail, movieId: movieId);
  }

  void saveListU(ListU listU) {
    cListU = listU;
    WriteCache.setJson(key: keyListU, value: listU.toJson());
  }

  ListU deleteMovieFromList(
      {required String userEmail, name: 'Likes', required int posDelete}) {
    cListU.movies.removeAt(posDelete);
    WriteCache.setJson(key: keyListU, value: cListU.toJson());
    _listAdapter.updateList(userEmail: userEmail, name: name, listU: cListU);
    return cListU;
  }
}
