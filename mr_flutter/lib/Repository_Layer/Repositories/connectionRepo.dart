import 'package:internet_connection_checker/internet_connection_checker.dart';

class ConnectionRepo {
  Future<bool> isConnectedToInternet() async {
    return await InternetConnectionChecker().hasConnection;
  }

  Stream<bool> streamConnectedToInternet() {
    return InternetConnectionChecker()
        .onStatusChange
        .map((InternetConnectionStatus connectionStatus) {
      if (connectionStatus == InternetConnectionStatus.connected) {
        // I am connected to a network.
        return true;
      } else {
        // I am not connected to a network. c:
        return false;
      }
    });
  }
}
